import time

import pymysql
from global_object import DATABASE_URL, DATABASE_PORT, DATABASE_DBNAME, DATABASE_USER, DATABASE_PASSWORD






# db = pymysql.connect(host=DATABASE_URL, port=int(DATABASE_PORT),
#                      user=DATABASE_USER, password=DATABASE_PASSWORD,
#                      db=DATABASE_DBNAME, connect_timeout=2, read_timeout=60, write_timeout=60, charset='utf8')
# cursor = db.cursor(pymysql.cursors.DictCursor)


#########################################################################
class Database():
    def __init__(self):
        self.db = pymysql.connect(host=DATABASE_URL, port=int(DATABASE_PORT), user=DATABASE_USER, password=DATABASE_PASSWORD,
                                  db=DATABASE_DBNAME, connect_timeout=2, read_timeout=60, write_timeout=60, charset='utf8')
        self.cursor = self.db.cursor(pymysql.cursors.DictCursor)

    def execute(self, query, args={}):
        try:
            self.cursor.execute(query, args)
            return True
        except Exception as e:
            self.db.rollback()
            return False

    def executeOne(self, query, args={}):
        self.cursor.execute(query, args)
        row = self.cursor.fetchone()
        return row

    def executeAll(self, query, args={}):
        self.cursor.execute(query, args)
        row = self.cursor.fetchall()
        return row

    def commit(self):
        self.db.commit()

    def rollback(self):
        self.db.rollback()


###########################

    def is_device_on(self, device_id):

        try:
            sql = "SELECT dev_id " \
                  "FROM(SELECT dev_id " \
                  "     FROM ir_image_realtime WHERE  modify_date >  SUBDATE(NOW(), INTERVAL 10 second))A " \
                  "WHERE dev_id = %s"
            params = (device_id)
            res_data = self.executeOne(sql, params)
            if res_data:
                return True
            else:
                return False
        except Exception as e:
            return False


    def get_image_data(self,device_list):

        try:
            param_list = str(device_list).replace("[", "(").replace("]", ")")
            sql = "SELECT * " \
                  "FROM(SELECT * FROM ir_image_realtime " \
                  "WHERE  modify_date >  SUBDATE(NOW(), INTERVAL 10 second))A  " \
                  "WHERE dev_id in " + param_list
            res_data = self.executeAll(sql)
            if res_data:
                return res_data
            else:
                return []
        except Exception as e:
            return []


    def get_mouse_data(self, device_list):

        try:
            param_list = str(device_list).replace("[", "(").replace("]", ")")
            sql = "SELECT dev_id,mouse_temp, spot1_temp, spot1_x, spot1_y, spot2_temp, spot2_x, spot2_y, spot3_temp, spot3_x, spot3_y FROM ir_response_realtime WHERE modify_date >  SUBDATE(NOW(), INTERVAL 10 second) " \
                  "AND dev_id in "+ param_list
            res_data = self.executeAll(sql)
            if res_data:
                return res_data
            else:
                return []
        except Exception as e:
            return []

    def get_mouse_data2(self, device_list):

        try:
            param_list = str(device_list).replace("[", "(").replace("]", ")")
            sql = "SELECT dev_id,mouse_temp, spot1_temp, spot1_x, spot1_y, spot2_temp, spot2_x, spot2_y, spot3_temp, spot3_x, spot3_y, " \
                  "area1_onoff,area1_top,area1_x_start,area1_y_start, abs(area1_x_end-area1_x_start) as width1, abs(area1_y_end-area1_y_start) as height1, "\
                  "area2_onoff,area2_top,area2_x_start,area2_y_start, abs(area2_x_end-area2_x_start) as width2, abs(area2_y_end-area2_y_start) as height2, "\
                  "area3_onoff,area3_top,area3_x_start,area3_y_start, abs(area3_x_end-area3_x_start) as width3, abs(area3_y_end-area3_y_start) as height3, "\
                  "area4_onoff,area4_top,area4_x_start,area4_y_start, abs(area4_x_end-area4_x_start) as width4, abs(area4_y_end-area4_y_start) as height4, "\
                  "area5_onoff,area5_top,area5_x_start,area5_y_start, abs(area5_x_end-area5_x_start) as width5, abs(area5_y_end-area5_y_start) as height5, "\
                  "area6_onoff,area6_top,area6_x_start,area6_y_start, abs(area6_x_end-area6_x_start) as width6, abs(area6_y_end-area6_y_start) as height6 " \
                  "FROM ir_response_realtime WHERE modify_date >  SUBDATE(NOW(), INTERVAL 10 second) " \
                  "AND dev_id in "+ param_list
            res_data = self.executeAll(sql)
            if res_data:
                return res_data
            else:
                return []
        except Exception as e:
            return []


    def get_response_data(self, device_list):

        try:
            param_list = str(device_list).replace("[", "(").replace("]", ")")
            sql = """
                  SELECT *,DATE_FORMAT(modify_date,'%%H:%%i:%%s') as time  FROM ir_response_realtime
                   WHERE modify_date >  SUBDATE(NOW(), INTERVAL 10 second)
                   AND dev_id in $param
                  """.replace("$param",param_list)
            res_data = self.executeAll(sql)
            if res_data:
                return res_data
            else:
                return []
        except Exception as e:
            return []

    def merge_mouse_loc(self, dev_id, x, y):

        try:
            sql = "INSERT INTO ir_request_realtime (dev_id, mouse_x, mouse_y ) VALUES (%s, %s, %s )" \
                  "on duplicate key update mouse_x=%s, mouse_y=%s "
            params = (dev_id, x, y, x, y)

            res_data = self.execute(sql, params)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False


    def get_tree_data(self, node_list):
        try:

            sql = "SELECT t.id,t.parent_id,t.level,t.sibling_sequence,if(level !=4,t.name,d.dev_nm)name,if(level =4,d.dev_id,null)device_id,if(level!=4,null,d.ip_addr)ip "\
                  "FROM (SELECT * FROM cmn_tree_info WHERE id in $id_list ) t "\
                  "LEFT JOIN cmn_device_info d ON t.name = d.dev_id "\
                  "ORDER BY level,sibling_sequence "
            sql = sql.replace("$id_list", str(node_list).replace("[", "(").replace("]", ")"))

            res_data = self.executeAll(sql)
            if res_data:
                return res_data
            else:
                return False
        except Exception as e:
            return False


    def get_no_parent_devices(self):
        try:
            sql = "SELECT * FROM cmn_device_info "\
                  "WHERE dev_id not in(SELECT name FROM cmn_tree_info WHERE level = 4 )"

            res_data = self.executeAll(sql)
            if res_data:
                return res_data
            else:
                return False
        except Exception as e:
            return False

    def insert_device_parent(self,parent_id, dev_id):
        try:
            sql = "insert into cmn_tree_info(parent_id, level, name, sibling_sequence) values (%s, %s, %s, (SELECT if(num is null,0,num+1) FROM(SELECT max(sibling_sequence)num FROM cmn_tree_info WHERE parent_id = %s )A))"
            params = (parent_id, '4' ,dev_id, parent_id)

            res_data = self.execute(sql, params)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False



    def update_device_parent(self, parent_id, id):
        try:
            sql = "UPDATE cmn_tree_info " \
                  "SET parent_id = %s, " \
                  "sibling_sequence = (SELECT if(num is null,0,num+1) FROM(SELECT max(sibling_sequence)num FROM cmn_tree_info WHERE parent_id = %s )A) " \
                  "WHERE cmn_tree_info.id = %s "
            params = (parent_id,parent_id, id)
            res_data = self.execute(sql, params)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False

    def get_node_data(self, id):
        try:
            sql = "SELECT t.id,t.parent_id,t.level,t.sibling_sequence,if(level !=4,t.name,d.dev_nm)name,if(level!=4,null,d.ip_addr)ip "\
                  "FROM cmn_tree_info t "\
                  "LEFT JOIN cmn_device_info d ON t.name = d.dev_id "\
                  "WHERE t.id = %s "
            params = (id)
            res_data = self.executeOne(sql,params)
            if res_data:
                return res_data
            else:
                return False
        except Exception as e:
            return False


    def get_node_children_data(self, id):
        try:
            sql = "SELECT t.id,t.parent_id,t.level,t.sibling_sequence,if(level !=4,t.name,d.dev_nm)name,if(level!=4,null,d.ip_addr)ip "\
                  "FROM cmn_tree_info t "\
                  "LEFT JOIN cmn_device_info d ON t.name = d.dev_id "\
                  "WHERE t.parent_id = %s ORDER BY sibling_sequence"
            params = (id)
            res_data = self.executeAll(sql,params)
            if res_data:
                return res_data
            else:
                return False
        except Exception as e:
            return False

    def insert_child_node(self,parent_id, level, name):
        try:
            sql = "INSERT INTO cmn_tree_info (parent_id, level, name, sibling_sequence ) VALUES (%s, %s, %s, " \
                  "(SELECT num FROM(SELECT ifnull(max(sibling_sequence),0) + 1 as num  FROM cmn_tree_info WHERE parent_id = %s)A))"

            params = (parent_id, level, name, parent_id)

            res_data = self.execute(sql, params)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False

    def update_node_name(self, id, name):
        try:
            sql = "UPDATE cmn_tree_info SET name = %s WHERE id = %s "
            params = (name, id)
            res_data = self.execute(sql, params)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False

    def update_device_name(self,id, dev_name):
        try:
            sql = "UPDATE cmn_device_info SET dev_nm = %s WHERE dev_id = (SELECT name FROM cmn_tree_info WHERE level = '4' AND id = %s) "
            params = (dev_name, id)
            res_data = self.execute(sql, params)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False


    def select_famaily_data(self,id, level):
        try:
            sql = """
                  SELECT l1.id as '1',l2.id as '2',l3.id as '3',l4.id as '4'
                  FROM(SELECT * FROM cmn_tree_info WHERE level = 1)l1
                  LEFT JOIN(SELECT * FROM cmn_tree_info WHERE level = 2)l2 ON l1.id = l2.parent_id
                  LEFT JOIN(SELECT * FROM cmn_tree_info WHERE level = 3)l3 ON l2.id = l3.parent_id
                  LEFT JOIN(SELECT * FROM cmn_tree_info WHERE level = 4)l4 ON l3.id = l4.parent_id
                  WHERE l{0}.id = {1}
                  """.format(level,id)
            res_data = self.executeAll(sql)
            if res_data:
                return res_data
            else:
                return []
        except Exception as e:
            return []

    def delete_nodes(self,node_list):
        try:
            sql = "DELETE FROM cmn_tree_info WHERE id in " + str(node_list).replace("[", "(").replace("]", ")")
            res_data = self.execute(sql)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False

    def update_node_sequence(self,src_id, src_sequence, dst_id, dst_sequence):
        try:
            sql = """
                  UPDATE cmn_tree_info t1
                    JOIN (
                        SELECT %s as id, %s as new_sequence
                        UNION
                        SELECT %s, %s
                    ) t2 ON t1.id = t2.id
                  SET sibling_sequence = new_sequence
                  """
            params = (src_id, dst_sequence, dst_id, src_sequence)
            res_data = self.execute(sql, params)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False


    def get_users_data(self):
        try:
            sql = """
                  SELECT id,user_email as user_id,name,user_password as password, role,date_format(crt_date,'%%Y-%%m-%%d')c_date
                  FROM usr_main_info
                  WHERE ( role = 'admin' or role = 'user')
                  ORDER BY crt_date DESC
                  """
            res_data = self.executeAll(sql)
            if res_data:
                return res_data
            else:
                return []
        except Exception as e:
            return []

    def get_all_tree_data(self):
        try:
            sql = "SELECT t.id,t.parent_id,t.level,t.sibling_sequence,if(level !=4,t.name,d.dev_nm)name,if(level =4,d.dev_id,null)device_id,if(level!=4,null,d.ip_addr)ip "\
                  "FROM cmn_tree_info t "\
                  "LEFT JOIN cmn_device_info d ON t.name = d.dev_id "\
                  "ORDER BY level,sibling_sequence "
            res_data = self.executeAll(sql)
            if res_data:
                return res_data
            else:
                return False
        except Exception as e:
            return False

    def get_user_menu_auth_list(self,user_id, role = None):
        try:
            if role != None and 'admin' in role :
                sql = "SELECT 's' as auth_list UNION ALL SELECT 'u' as auth_list UNION ALL SELECT 'c' as auth_list " \
                      " UNION ALL SELECT 'h' as auth_list UNION ALL SELECT 'r' as auth_list"
                params =()
            else:
                sql = "SELECT menu_id as auth_list FROM usr_permission_info WHERE user_id = %s AND menu_type = 'M' "
                params =(user_id)
            res_data = self.executeAll(sql,params)
            if res_data:
                return [value for row in res_data for key,value in row.items() ]
            else:
                return []
        except Exception as e:
            return []

    def get_user_device_list(self,user_id, role = None):
        try:

            if role != None and  'admin' in role :
                sql = """
                        SELECT A.*, T2.name as parent_name
                        FROM(SELECT D.id, dev_id, dev_nm , T.parent_id FROM cmn_device_info D
                        JOIN cmn_tree_info T ON D.dev_id = T.name)A
                        JOIN cmn_tree_info T2
                        ON A.parent_id = T2.id
                      """
                params =()
            else:
                sql = """
                        SELECT A.*, T2.name as parent_name
                        FROM(SELECT D.id, dev_id, dev_nm, T.parent_id
                        FROM cmn_device_info D
                        JOIN cmn_tree_info T
                        ON D.dev_id = T.name
                        WHERE dev_id in(SELECT name FROM cmn_tree_info WHERE level = 4 AND parent_id in 
                        ( SELECT menu_id as auth_list FROM usr_permission_info WHERE user_id = %s AND menu_type = 'T' )))A
                        JOIN cmn_tree_info T2
                        ON A.parent_id = T2.id        
                       """
                params =(user_id)
            res_data = self.executeAll(sql, params)
            if res_data:
                return {row['dev_id']: row for row in res_data}
            else:
                return {}
        except Exception as e:
            return {}



    def get_user_tree_auth_list(self,user_id, role = None):
        try:

            if role != None and 'admin' in role :
                sql = "SELECT id as auth_list FROM cmn_tree_info "
                params =()
            else:
                sql = "SELECT menu_id as auth_list FROM usr_permission_info WHERE user_id = %s AND menu_type = 'T' " \
                      "UNION ALL " \
                      "SELECT id as auth_list FROM cmn_tree_info WHERE level = 4 AND parent_id in ( SELECT menu_id as auth_list FROM usr_permission_info WHERE user_id = %s AND menu_type = 'T' )"
                params =(user_id, user_id)
            res_data = self.executeAll(sql,params)
            if res_data:
                return list(set(map(int,[value for row in res_data for key,value in row.items() ])))
            else:
                return []
        except Exception as e:
            return []


    def delete_user_tree_data(self,idx,node_list):
        try:
            sql = "DELETE FROM usr_permission_info WHERE menu_type = 'T' AND user_id = %s AND menu_id in " + str(node_list).replace("[", "(").replace("]", ")")
            params = (str(idx))
            res_data = self.execute(sql,params)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False

    def insert_user_tree_data(self,idx,node_list):
        try:

            sql = "INSERT INTO usr_permission_info (user_id, menu_type, menu_id) VALUES "
            values = ",".join(["({},{},{})".format(str(idx),"'T'",str(node_id)) for node_id in node_list])
            sql += values
            res_data = self.execute(sql)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False

    def update_user_info(self,new_name, new_pw, idx):
        try:
            sql = """
                  UPDATE usr_main_info
                  SET name = %s, user_password = %s
                  WHERE id = %s
                  """
            params = (new_name, new_pw, idx)
            res_data = self.execute(sql, params)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False


    def delete_user_auth_data(self, idx, menu):
        try:
            sql = "DELETE FROM usr_permission_info WHERE menu_type = 'M' AND menu_id = %s AND user_id = %s"
            params = (menu,str(idx))
            res_data = self.execute(sql,params)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False



    def insert_user_auth_data(self, idx, menu):
        try:
            sql = "INSERT INTO usr_permission_info (user_id, menu_type, menu_id) VALUES (%s, 'M', %s)"
            params = (str(idx),menu)
            res_data = self.execute(sql, params)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False

    def get_family_names(self,ids):
        try:
            ids += [1]
            sql = "SELECT id,name  FROM cmn_tree_info WHERE id in " + str(ids).replace("[","(").replace("]",")")
            sql +=" ORDER BY level"
            res_data = self.executeAll(sql)
            if res_data:
                return [value for row in res_data for key,value in row.items() if key=='name']
            else:
                return []
        except Exception as e:
            return []

    def get_cmd_data(self,dev_id):
        try:
            sql = "SELECT c.*  FROM ir_screencfg_cmd c " \
                  "JOIN (SELECT dev_id FROM ir_image_realtime WHERE  modify_date >  SUBDATE(NOW(), INTERVAL 10 second) AND dev_id = %s )m " \
                  "ON c.dev_id = m.dev_id " \
                  "WHERE c.dev_id = %s "

            params = (dev_id, dev_id)
            res_data = self.executeOne(sql, params)
            if res_data:
                return res_data
            else:
                return False
        except Exception as e:
            return False

    def update_cmd(self, set_str, dev_id):
        try:
            sql = """
                  UPDATE ir_screencfg_cmd
                  SET $set_str 
                  WHERE dev_id = '$dev_id' 
                  """.replace("$set_str",set_str).replace("$dev_id",dev_id)
            res_data = self.execute(sql)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False


    def get_dev_network_data(self, dev_id):
        try:
            sql = "SELECT * FROM cmn_device_info WHERE dev_id = %s"
            params = (dev_id)
            res_data = self.executeOne(sql, params)
            if res_data:
                return res_data
            else:
                return False
        except Exception as e:
            return False

    def update_color(self, dev_id, color):
        try:
            sql = """
                  UPDATE ir_screencfg_cmd
                  SET colortable = %s 
                  WHERE dev_id = %s 
                  """
            params = (color, dev_id)
            res_data = self.execute(sql, params)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False


    def update_flip(self,dev_id , flip):
        try:
            sql = """
                  UPDATE ir_screencfg_cmd
                  SET flip = %s 
                  WHERE dev_id = %s 
                  """
            params = (flip, dev_id)
            res_data = self.execute(sql, params)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False

    def update_gain(self, dev_id, gain):
        try:
            sql = """
                  UPDATE ir_screencfg_cmd
                  SET gainmode = %s 
                  WHERE dev_id = %s 
                  """
            params = (gain, dev_id)
            res_data = self.execute(sql, params)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False



    def update_range_mod(self, dev_id, range_mod):
        try:
            if range_mod == '0':
                min_max_str = "absolute_min = 0, absolute_max = 0"
            else:
                min_max_str = "absolute_min = 0, absolute_max = 180"

            sql = """
                  UPDATE ir_screencfg_cmd
                  SET relativecolor = %s,$set_str 
                  WHERE dev_id = %s 
                  """.replace("$set_str", min_max_str)
            params = (range_mod, dev_id)
            res_data = self.execute(sql, params)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False


    def update_range_value(self, dev_id, min, max):
        try:
            sql = """
                  UPDATE ir_screencfg_cmd
                  SET absolute_min = %s, absolute_max = %s 
                  WHERE dev_id = %s 
                  """
            params = (min, max, dev_id)
            res_data = self.execute(sql, params)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False



    def get_admin_device_list(self):

        try:
            sql = "SELECT d.*,t.name FROM cmn_device_info d "\
                  "LEFT JOIN (SELECT name FROM cmn_tree_info WHERE level = 4) t on d.dev_id = t.name "
            res_data = self.executeAll(sql)
            if res_data:
                return res_data
            else:
                return []
        except Exception as e:
            return []

    def insert_device(self, device, d_id, d_name):
        try:
            sql = "INSERT INTO cmn_device_info (ip_addr, mac_addr, dev_id, dev_nm ,status) VALUES (%s, %s, %s, %s, %s)"
            params = (device['ip'], device['mac'], d_id, d_name, "ON")

            res_data = self.execute(sql, params)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False

    def update_id_and_name(self,id, dev_id, dev_nm):
        try:
            sql = "UPDATE cmn_device_info " \
                  "SET dev_id = %s, dev_nm = %s " \
                  "WHERE id = %s "
            params = (dev_id, dev_nm, id)

            res_data = self.execute(sql, params)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False



    def update_ip_and_mac(self, id, ip, mac):
        try:
            sql = "UPDATE cmn_device_info " \
                  "SET ip_addr = %s, mac_addr = %s " \
                  "WHERE id = %s "
            params = (ip, mac, id)
            res_data = self.execute(sql, params)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False

    def delete_device(self, id):
        try:
            sql = "DELETE FROM cmn_device_info " \
                  "WHERE id = %s "
            params = (id)
            res_data = self.execute(sql, params)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False

    def delete_node_device(self, dev_id):
        try:
            sql = "DELETE FROM cmn_tree_info " \
                  "WHERE level = '4' AND name = %s "
            params = (dev_id)
            res_data = self.execute(sql, params)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False

    def login(self, u_id, u_pw):
        try:
            sql = "SELECT * FROM usr_main_info "\
                  "WHERE user_email = %s AND user_password = %s "
            params = (u_id, u_pw)
            res_data = self.executeOne(sql, params)
            if res_data:
                return res_data
            else:
                return False
        except Exception as e:
            return False

    def delete_user(self, id):
        try:
            sql = "DELETE FROM usr_main_info " \
                  "WHERE id = %s "
            params = (id)
            res_data = self.execute(sql, params)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False

    def add_user(self, user_email,user_idx, user_name):
        try:
            sql = "insert into usr_main_info(user_email, user_idx, name, user_password) values (%s, %s, %s, %s)"
            params = (user_email, user_idx, user_name,'pms123')

            res_data = self.execute(sql, params)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False


    def select_video_list(self,dev_id, s_date, e_date):
        try:
            #WR1=Warning1,WR2=Warngin2, ER=Alarm FL
            sql = """
                  SELECT id, dev_id, ifnull(alarm_status,'OK')alarm_status, date_format(video_date,'%%Y-%%m-%%d %%H:%%i')video_time, video_path
                  FROM ir_video_his 
                  WHERE dev_id = %s
                  AND video_date BETWEEN %s AND %s
                  ORDER BY id DESC
                  """
            params = (dev_id, s_date, e_date)
            res_data = self.executeAll(sql, params)
            if res_data:
                return res_data
            else:
                return []
        except Exception as e:
            return []

    def select_report_list(self,dev_id, s_date, e_date, add_query):
        try:
            sql = """
                  SELECT ifnull(alarm_status,'OK')alarm_status, date_format(sens_date,'%%Y-%%m-%%d %%H:%%i')report_time,
                         spot1_temp, spot2_temp, spot3_temp,
                         if(area1_onoff=1,area1_top,0)area1, if(area2_onoff=1,area2_top,0)area2,
                         if(area3_onoff=1,area3_top,0)area3, if(area4_onoff=1,area4_top,0)area4,
                         if(area5_onoff=1,area5_top,0)area5, if(area6_onoff=1,area6_top,0)area6
                  FROM ir_report_summary 
                  WHERE dev_id = %s
                  AND sens_date BETWEEN %s AND %s
                  $alarm_status
                  ORDER BY id DESC
                  """.replace("$alarm_status", add_query)
            params = (dev_id, s_date, e_date)
            res_data = self.executeAll(sql, params)
            if res_data:
                return res_data
            else:
                return []
        except Exception as e:
            return []

    def select_number_of_children(self, parent_id):
        try:
            sql = """
                  SELECT COUNT(*) as count
                  FROM cmn_tree_info
                  WHERE parent_id = %s
                  """
            params = (parent_id)
            res_data = self.executeOne(sql, params)
            if res_data:
                return res_data
            else:
                return 13
        except Exception as e:
            return 13

    def update_whether_save_video(self, dev_id, status):
        try:
            sql = "UPDATE cmn_device_info " \
                  "SET save_image = %s " \
                  "WHERE dev_id = %s "
            params = (status, dev_id)
            res_data = self.execute(sql, params)
            if res_data:
                self.commit()
                return True
            else:
                return False
        except Exception as e:
            return False


#######################################################

