from PyQt5.QtMultimedia import QMediaPlayer, QMediaContent
from PyQt5.QtCore import pyqtSignal, QObject, QUrl
from global_object import VIDEO_FORDER_PATH
from PyQt5.QtMultimediaWidgets import * #꼭 필요

class Video(QObject):

    signal_state = pyqtSignal(str)
    signal_duration = pyqtSignal(int)
    signal_pos = pyqtSignal(int)

    def __init__(self, main, video_widget):
        super().__init__()
        self.parent = main
        self.video_path = None
        self.qmp = QMediaPlayer(main, flags=QMediaPlayer.VideoSurface)
        self.qmp.setVideoOutput(video_widget)

        self.qmp.stateChanged.connect(self.changed_state)
        self.qmp.durationChanged.connect(self.changed_duration)
        self.qmp.positionChanged.connect(self.changed_position)

        self.signal_state.connect(self.parent.update_state)
        self.signal_duration.connect(self.parent.update_prog_bar)
        self.signal_pos.connect(self.parent.update_pos)

    def set_video_path(self ,new):
        if self.video_path != new:
            self.video_path = new
            self.qmp.setMedia(QMediaContent(QUrl.fromLocalFile(VIDEO_FORDER_PATH+self.video_path)))


    def play(self):
        self.qmp.play()

    def pause(self):
        self.qmp.pause()

    def move(self, pos):
        self.qmp.setPosition(pos)


    def move_by_btn(self,value):
        self.qmp.setPosition(self.qmp.position()+value)


    def stop(self):
        self.qmp.stop()

    def changed_state(self, state):
        msg = ''
        if state==QMediaPlayer.StoppedState:
            msg = 'Stopped'
        elif state==QMediaPlayer.PlayingState:
            msg = 'Playing'
        else:
            msg = 'Paused'
        self.signal_state.emit(msg)

    def changed_duration(self, state):
        msg = ''
        if state == self.qmp.StoppedState:
            msg = 'Stopped'
        elif state == self.qmp.PlayingState:
            msg = 'Playing'
        else:
            msg = 'Paused'
        self.signal_state.emit(msg)


    def changed_duration(self, duration):
        self.signal_duration.emit(duration)

    def changed_position(self, pos):
        self.signal_pos.emit(pos)