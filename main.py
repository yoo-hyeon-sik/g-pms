import datetime
import math
import subprocess
import os
import ctypes
import timeit

import qtawesome as qta
import xlwt

from alert_window import *
from global_object import *
from PyQt5.QtCore import Qt, QEvent, pyqtSlot, QTimer, QModelIndex, QDate, QSize, QTime, QUrl
from PyQt5.QtGui import QCursor, QFont, QDropEvent, \
    QBrush, QKeyEvent, QPixmap, QPen, QStaticText, QFocusEvent, QDoubleValidator, QIntValidator, QMovie, QIcon, \
    QPainter, QMouseEvent, QImage, QPalette, QWindow
from PyQt5.QtWidgets import *

from Video import Video, QVideoWidget

from PyQt5 import uic, sip
from find_window import *
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.pyplot as plt
from matplotlib import dates as mdates
import mplcursors


# spec 파일을 만든 후 spec 파일의 설정을 기반으로 exe파일을 만듬
# pyi-makespec --noconsole -n g-pms main.py
# pyinstaller -w g-pms.spec


# <메인 윈도우>
main_form = uic.loadUiType(resource_path('main_window.ui'))[0]
class WindowClass(QMainWindow, main_form):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        #self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self.setWindowFlag(QtCore.Qt.FramelessWindowHint)
        #self.setAttribute(Qt.WA_TranslucentBackground, True)
        self.setWindowIcon(QIcon(resource_path('icon.png')))


        #로그인 유저 변수
        self.user_data = {}
        self.user_tree_data = []
        self.user_devices_data = {}
        self.re_login = False

        # 변수 영역
        self.menu_count = 3
        self.slot_count = 12
        self.slot_list = [QWidget() for i in range(self.slot_count)]
        self.screen_list = []
        self.dot_area_list = []
        self.info_area_list = []
        self.status_list = [QLabel() for i in range(self.slot_count)]
        self.name_list = [QLabel() for i in range(self.slot_count)]
        self.thermometer_list = [QLabel() for i in range(self.slot_count)]
        self.slot_areas_dict = {"dashboard":{"widget":self.area_slots_dashboard,"info":self.area_info_dashboard_lay}, "fullscreen":{"widget":self.area_slots_fullscreen, "info": self.area_info_fullscreen_lay}}
        self.now_highst_level = 0
        self.select_camera_index = self.slot_count + 1
        self.screen_width = 320
        self.screen_height = 240
        self.pm_list = [QPixmap(320, 240) for i in range(self.slot_count)]
        self.mouse_loc_dict = {}
        self.big_index = 0
        self.section_btn_dict = {}
        self.status_data_list = [] # 쓰레드를 통해 DB에서 갖고온 데이터를 갖고 있음
        self.t_index = 0
        self.t_command = 0 #0:stop #1:scroll #2:blink
        self.t_blink_msg = ""
        self.blink_dst_list = [ ]
        self.blink_colors = ["",""]
        self.blink_index = 0
        self.w1_dev_list = []
        self.w2_dev_list = []
        self.alrm_dev_list = []
        self.flame_dev_list = []
        self.conf_mode = 0
        self.areas_colors = ['#5A7CC0','#6C8ED2','#7EA0E4','#90B2F6','#A2C4FF','#B4D6FF']
        self.conf_px = QPixmap(560, 420)
        self.conf_px2 = QPixmap(560, 420)
        self.conf_px3 = QPixmap(560, 420)
        self.conf_tasking = False
        self.current_index = 0
        self.s_date = None
        self.e_date = None
        self.m_flag = False
        self.m_Position = 0
        self.x_time_list = []
        self.cursor_list = []

        #대시보드 알림 클릭시 팝업 윈도우
        self.alert_window = alert_window(self)
        self.alert_window.signal.connect(self.alert_window_receiver)


        # 쓰레드
        self.now_time_thread = thread_time(self)
        self.now_time_thread.signal.connect(self.now_time_Indicator)
        self.camera_thread = thread_camera(self) #대시보드에서 카메라 봄
        self.camera_thread.signal.connect(self.image_data_receive)
        self.dot_thread = thread_dot_temp(self)
        self.dot_thread.signal.connect(self.dot_data_receive)
        self.response_thread = thread_response(self)
        self.response_thread.signal.connect(self.response_data_receive)
        self.camera_thread_conf = thread_camera2(self) #configuration에서 카메라 봄
        self.camera_thread_conf.signal.connect(self.image_data_receive_conf)
        self.dot_thread_conf = thread_dot_temp_conf(self)
        self.dot_thread_conf.signal.connect(self.dot_data_receive_conf)
        self.camera_thread_admin = thread_camera3(self) #admin tool 에서 카메라 봄
        self.camera_thread_admin.signal.connect(self.image_data_receive_admin)
        self.history_search = thread_history(self)
        self.history_search.signal.connect(self.history_list_receive)
        self.delay_executor = thread_delay_execute(self, 3.0)
        self.delay_executor.signal.connect(self.when_program_start)
        self.report_search = thread_report(self)
        self.report_search.signal.connect(self.report_list_receive)
        self.firm_update_thread = thread_firm_update(self)
        self.firm_update_thread.signal.connect(self.firm_update_end_event)
        self.param_thread = thread_param(self)
        self.param_thread.signal.connect(self.param_end_event)


        # 버튼 이벤트 연결
        self.btn_win_close.clicked.connect(self.close_program)
        self.btn_win_min.clicked.connect(lambda: self.when_down())
        self.btn_menu.clicked.connect(self.menu_btn_clicked)
        self.btn_dashboard.clicked.connect(lambda: self.page_change(0))
        self.btn_conf.clicked.connect(lambda: self.page_change(3))
        self.btn_admin.clicked.connect(lambda: self.page_change(4))
        self.btn_history.clicked.connect(lambda: self.page_change(5))
        self.btn_report.clicked.connect(lambda: self.page_change(6))
        self.btn_node_name_change.clicked.connect(self.edit_node_name)
        self.btn_fullscreen.clicked.connect(self.to_full_screen)
        self.btn_user_info_change.clicked.connect(self.update_user_info)
        self.btn_area_add.clicked.connect(self.area_add)
        self.btn_area_init.clicked.connect(self.area_delete_all)
        self.btn_camera_stop.clicked.connect(self.stop_all_camera)
        self.btn_device_delete.clicked.connect(self.delete_device)
        self.btn_firm_update.clicked.connect(self.firm_update)
        self.btn_user_delete.clicked.connect(self.delete_user)
        self.btn_add_user.clicked.connect(self.add_user)
        self.btn_his_search.clicked.connect(self.search_his)
        self.btn_his_play.clicked.connect(self.play_his)
        self.btn_his_pause.clicked.connect(self.pause_his)
        self.btn_prev.clicked.connect(lambda :self.move_pos_btn(-10000))
        self.btn_forward.clicked.connect(lambda :self.move_pos_btn(10000))
        self.btn_his_stop.clicked.connect(self.stop_his)
        self.btn_find.clicked.connect(self.find_device)
        self.btn_rep_search.clicked.connect(self.search_rep)
        self.btn_param_update.clicked.connect(self.param_update)
        self.ip_and_mac_btn.clicked.connect(self.ip_and_mac_chage)

        self.btn_fd_cali.clicked.connect(lambda :self.click_fd_btn("calibration", 1024))
        self.btn_fd_init.clicked.connect(lambda :self.click_fd_btn("initialization", 2048))
        self.btn_fd_sr.clicked.connect(lambda :self.click_fd_btn("soft_reset", 4096))
        self.btn_fd_hr.clicked.connect(lambda :self.click_fd_btn("hard_reset", 8192))

        self.btn_graph_save.clicked.connect(self.save_graph)
        self.btn_data_save.clicked.connect(self.save_exel_data)


        # 슬라이드 이벤트 연결
        self.prog_bar.sliderMoved.connect(self.move_pos)

        # 슬라이드 아이콘 변경
        self.prog_bar : QSlider
        self.prog_bar.setStyleSheet(style_dict['prog_bar_style'].replace("$url", resource_path("pcon.png").replace("\\","/")))


       # 체크 박스 이벤트 연결
        self.checkbox_section.clicked.connect(lambda: self.update_user_checkbox('s',self.checkbox_section))
        self.checkbox_user.clicked.connect(lambda: self.update_user_checkbox('u',self.checkbox_user))
        self.checkbox_conf.clicked.connect(lambda: self.update_user_checkbox('c',self.checkbox_conf))
        self.checkbox_his.clicked.connect(lambda: self.update_user_checkbox('h',self.checkbox_his))
        self.checkbox_report.clicked.connect(lambda: self.update_user_checkbox('r',self.checkbox_report))
        #self.checkbox_admin_tool.clicked.connect(lambda: self.update_user_checkbox('a',self.checkbox_admin_tool))

        self.cb_n_save.clicked.connect(lambda: self.normal_video_save_active(self.cb_n_save))

        # 세팅 영역
        self.label_title.setText("G-PMS(IR)")
        self.label_title.setIcon(QIcon(resource_path("icon.png")))


        #상단 로고 장착
        p1 = QPixmap()
        p1.load(MAIN_LOGO1)
        p1 = p1.scaledToHeight(30)
        self.main_img1.setPixmap(QtGui.QPixmap(p1))

        #self.menu_box.closeEvent()
        #self.menu_box.focusOutEvent= lambda e : self.test1()
        #self.menu_box.hideEvent = lambda e : self.test2()
        #self.menu_box.closeEvent = lambda e : self.test3()
        self.label_title.mousePressEvent = self.bar_click
        self.label_title.mouseMoveEvent = self.bar_move
        self.label_title.mouseReaseEvent = self.bar_release
        self.area_top.mousePressEvent = self.bar_click
        self.area_top.mouseMoveEvent = self.bar_move
        self.area_top.mouseReaseEvent = self.bar_release
        self.showEvent = lambda e : self.when_up()
        self.hideEvent = lambda e : self.when_down()
        self.tabWidget.tabBar().hide()
        self.tabWidget.setCurrentIndex(0)
        self.tree_dashboard.setColumnCount(1)
        # self.tabWidget.tabBar().hide()
        self.tree_section.setDragDropMode(QAbstractItemView.InternalMove)
        self.btn_node_name_change.setStyleSheet(style_dict["section_btn_normal"])
        self.btn_node_add.setStyleSheet(style_dict["section_btn_normal"])
        self.btn_node_up.setStyleSheet(style_dict["section_btn_normal"])
        self.btn_node_down.setStyleSheet(style_dict["section_btn_normal"])
        self.btn_node_delete.setStyleSheet(style_dict["section_btn_node_delete"])
        self.btn_user_info_change.setStyleSheet(style_dict["section_btn_normal"])
        self.area_description.hide()
        self.label_description2.hide()
        self.label_description3.hide()
        self.input_abs_min.setValidator(QIntValidator(0, 180))
        self.input_abs_max.setValidator(QIntValidator(0, 180))
        self.list_device.itemClicked.connect(self.device_click)
        self.list_device.keyPressEvent = lambda e: e.ignore()
        for number in range(1, 9):
            eval("self.input_ip{}.setValidator(QIntValidator(0,255,self))".format(number))

        #self.param_r.setValidator(QIntValidator(0, 4294967295, self))
        self.param_b.setValidator(QIntValidator(0, 65535, self))
        self.param_f.setValidator(QIntValidator(0, 65535, self))
        self.param_o.setValidator(QIntValidator(0, 65535, self))
        self.param_t.setValidator(QIntValidator(0, 65535, self))
        self.param_atm.setValidator(QIntValidator(0, 65535, self))
        self.param_o2.setValidator(QIntValidator(0, 65535, self))
        self.param_s.setValidator(QIntValidator(0, 65535, self))
        self.param_max.setValidator(QIntValidator(0, 65535, self))
        self.param_min.setValidator(QIntValidator(0, 65535, self))
        self.param_auto1.setValidator(QIntValidator(0, 65535, self))
        self.param_auto2.setValidator(QIntValidator(0, 65535, self))
        self.param_auto3.setValidator(QIntValidator(0, 65535, self))

        self.input_to.setValidator(QIntValidator(100, 9999, self))
        self.area_admin_description.hide()
        self.area_admin_description2.hide()
        self.area_admin_description3.hide()
        self.c_area.hide()
        self.his_gif = QMovie(resource_path('loading.gif'))
        self.his_gif.setScaledSize(QSize(70, 70))
        self.label_loading.setMovie(self.his_gif)
        self.rep_gif = QMovie(resource_path('loading.gif'))
        self.rep_gif.setScaledSize(QSize(70, 70))
        self.label_loading2.setMovie(self.rep_gif)
        self.video = Video(self, self.screen_his)
        self.btn_find.setText("Search")
        self.btn_find.setIcon(qta.icon("mdi.file-find-outline", color="black"))
        self.btn_prev.setIcon(qta.icon("ei.arrow-left", color="black"))
        self.btn_prev_b.setIcon(qta.icon("ei.arrow-left", color="gray"))
        self.btn_forward.setIcon(qta.icon("ei.arrow-right", color="black"))
        self.btn_forward_b.setIcon(qta.icon("ei.arrow-right", color="gray"))
        self.btn_his_pause.setIcon(qta.icon("fa.pause", color="black"))
        self.btn_his_pause_b.setIcon(qta.icon("fa.pause", color="gray"))
        self.btn_prev.setIconSize(QtCore.QSize(32, 32))
        self.btn_prev_b.setIconSize(QtCore.QSize(32, 32))
        self.btn_forward.setIconSize(QtCore.QSize(32, 32))
        self.btn_forward_b.setIconSize(QtCore.QSize(32, 32))
        self.btn_his_pause.setIconSize(QtCore.QSize(32, 32))
        self.btn_his_pause_b.setIconSize(QtCore.QSize(32, 32))
        self.admin_gif = QMovie(resource_path('loading.gif'))
        self.admin_gif.setScaledSize(QSize(50, 50))
        self.label_admin_loading.setMovie(self.admin_gif)
        self.admin_gif2 = QMovie(resource_path('loading.gif'))
        self.admin_gif2.setScaledSize(QSize(50, 50))
        self.label_admin_loading2.setMovie(self.admin_gif2)
        self.admin_gif3 = QMovie(resource_path('loading.gif'))
        self.admin_gif3.setScaledSize(QSize(40, 40))
        self.label_admin_loading3.setMovie(self.admin_gif3)
        self.tree_section.itemClicked.connect(self.tree_section_clicked)
        self.list_his.setColumnWidth(1, 193)
        self.list_rep.setColumnWidth(0, 198)
        self.list_rep.setColumnWidth(1, 200)

        self.btn_fd_cali.setIcon(qta.icon("fa.sliders", color="black"))
        self.btn_fd_init.setIcon(qta.icon("fa5b.creative-commons-sa", color="black"))
        self.btn_fd_sr.setIcon(qta.icon("mdi.restart", color="black"))
        self.btn_fd_hr.setIcon(qta.icon("mdi.camera-retake", color="black"))
        self.btn_fd_cali.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.btn_fd_init.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.btn_fd_sr.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.btn_fd_hr.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.btn_fd_cali.setIconSize(QtCore.QSize(30, 30))
        self.btn_fd_init.setIconSize(QtCore.QSize(30, 30))
        self.btn_fd_sr.setIconSize(QtCore.QSize(30, 30))
        self.btn_fd_hr.setIconSize(QtCore.QSize(30, 30))

        self.tree_dashboard.setIndentation(10)
        self.tree_section.setIndentation(15)
        self.tree_user_auth.setIndentation(30)
        self.tree_conf.setIndentation(15)
        self.tree_his.setIndentation(15)
        self.tree_report.setIndentation(15)


        self.btn_dashboard : QToolButton
        self.btn_menu.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.btn_dashboard.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.btn_conf.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.btn_history.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.btn_report.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.btn_admin.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        self.btn_menu.setIcon(qta.icon("mdi.microsoft-xbox-controller-menu", color="gray"))
        self.btn_dashboard.setIcon(qta.icon("mdi.monitor-dashboard", color="gray"))
        self.btn_conf.setIcon(qta.icon("ei.cog-alt", color="gray"))
        self.btn_history.setIcon(qta.icon("mdi.file-document-multiple", color="gray"))
        self.btn_report.setIcon(qta.icon("ei.list-alt", color="gray"))
        self.btn_admin.setIcon(qta.icon("fa5s.tools", color="gray"))

        self.btn_menu.setIconSize(QtCore.QSize(32, 32))
        self.btn_dashboard.setIconSize(QtCore.QSize(32, 32))
        self.btn_conf.setIconSize(QtCore.QSize(32, 32))
        self.btn_history.setIconSize(QtCore.QSize(32, 32))
        self.btn_report.setIconSize(QtCore.QSize(32, 32))
        self.btn_admin.setIconSize(QtCore.QSize(32, 32))

        # self.btn_menu.setIcon(QIcon(resource_path("Menu_Btn_Icon_Menu.png")))
        # self.btn_dashboard.setIcon(QIcon(resource_path("Menu_Btn_Icon_Dashboard.png")))
        # self.btn_conf.setIcon(QIcon(resource_path("Menu_Btn_Icon_Configure.png")))
        # self.btn_history.setIcon(QIcon(resource_path("Menu_Btn_Icon_History.png")))
        # self.btn_report.setIcon(QIcon(resource_path("Menu_Btn_Icon_Report.png")))
        # self.btn_admin.setIcon(QIcon(resource_path("Menu_Btn_Icon_Admin.png")))
        #
        # self.btn_menu.setIconSize(QtCore.QSize(42, 42))
        # self.btn_dashboard.setIconSize(QtCore.QSize(42, 42))
        # self.btn_conf.setIconSize(QtCore.QSize(42, 42))
        # self.btn_history.setIconSize(QtCore.QSize(42, 42))
        # self.btn_report.setIconSize(QtCore.QSize(42, 42))
        # self.btn_admin.setIconSize(QtCore.QSize(42, 42))

        self.btn_fullscreen.setIcon(QIcon(resource_path("icon_full.png")))
        self.btn_camera_stop.setIcon(QIcon(resource_path("icon_off.png")))


        #self.tree_dashboard.setStyleSheet(style_dict['new_tree_style'])


        # 이벤트 연결
        # self.menu_box.focusOutEvent.connect(self.menu_btn_changed)
        # QComboBox.focusOutEvent.connect
        # self.tree_section.dragMoveEvent = self.move_event
        self.tree_section.dropEvent = self.drop_event
        self.menu_box.activated.connect(self.menu_changed)
        self.area_drawing_board.mousePressEvent = self.conf_board_click_event
        self.area_drawing_board.mouseMoveEvent = self.conf_board_move_event
        self.cb_cm.activated.connect(self.change_color)
        self.cb_fm.activated.connect(self.change_filp)
        self.cb_gm.activated.connect(self.change_gain)
        self.cb_rm.activated.connect(self.change_range_mode)
        self.input_abs_min.focusOutEvent = lambda e, this = self.input_abs_min: self.change_range_value(e, this)
        self.input_abs_max.focusOutEvent = lambda e, this = self.input_abs_max: self.change_range_value(e, this)
        self.input_abs_min.keyPressEvent = lambda e, this = self.input_abs_min: self.range_value_key_event(e, this)
        self.input_abs_max.keyPressEvent = lambda e, this = self.input_abs_max: self.range_value_key_event(e, this)
        self.list_users.clicked.connect(self.click_user)
        self.list_his.clicked.connect(self.click_his)
        self.list_his.doubleClicked.connect(lambda: self.btn_his_play.click())
        self.list_his.keyPressEvent = lambda e: e.ignore()
        self.input_node_add.keyPressEvent = lambda e, this = self.input_node_add: self.input_node_add_key_event(e, this)
        self.input_dev_id.keyPressEvent = lambda e, this = self.input_dev_id:self.id_and_name_btn_toggle(e, this)
        self.input_dev_name.keyPressEvent = lambda e, this = self.input_dev_name: self.id_and_name_btn_toggle(e, this)
        self.id_and_name_btn.clicked.connect(self.id_and_name_chage)
        self.input_admin_ip.keyPressEvent = lambda e, this = self.input_admin_ip:self.ip_and_mac_btn_toggle(e, this)
        self.input_admin_mac.keyPressEvent = lambda e, this = self.input_admin_mac: self.ip_and_mac_btn_toggle(e, this)
        self.init_graph_area()



        # 스크롤 애니메니션을 위한 타이머객체
        self.scroll_animation = QTimer(self)
        self.scroll_animation.timeout.connect(self.scoll_msg_bar)
        self.t_count = 0

        # 블링크 애니메니션을 위한 타이머객체
        self.blink_animation = QTimer(self)
        self.blink_animation.timeout.connect(self.blink_msg_bar)

        # 일정 시간마다 msg를 갱신해주는 타이머객체
        self.scheduler = QTimer(self)
        self.scheduler.timeout.connect(self.change_msg_bar_msg)


        self.list_node_children.itemClicked.connect(self.click_node_item)
        self.list_node_children.keyPressEvent = self.click_node_item
        self.btn_node_add.clicked.connect(self.add_child_node)
        self.btn_node_delete.clicked.connect(self.delete_node)
        self.btn_node_up.clicked.connect(lambda: self.node_up_down(-1))
        self.btn_node_down.clicked.connect(lambda: self.node_up_down(1))



        # 시작하면서 트리데이터를 불러옴
        #self.load_tree_dashboard(get_login_user()['user_tree_auth'])
        # 메뉴바 아래메뉴 감춤
        self.menu_box.hide()
        delete_memory_all()
        # 대시보드 스크린영역을 세팅함
        self.create_slot()
        self.create_slot_area()
        self.move_slots("dashboard",3) #대시보드에 3열로 배치
        # 대시보드 인포영역을 세팅함
        self.create_info_area()
        self.move_info_area("dashboard")
        self.update_info_area_size()
        # 풀스크린 영역을 감춤
        self.area_fullscreen.hide()

        self.center()



    def test1(self):
        data = get_memory_all()
        print(data)

        data2 = self.user_devices_data
        print(data2)


    def test2(self):
        print("test2")


    def test3(self):
        try:
            print("test3")

            self.screen_conf : QGraphicsView
            self.area_areas: QLabel # 영역 1~6
            self.area_drawing_board : QLabel #영역지정
            infor_str = """
    self.screen_conf
    isEnabled : %s
    isHidden : %s
    isVisible : %s
    isActiveWindow : %s
    
    
    self.camera_thread_conf
    isRunning() : %s
    stop : %s
    
    
    self.area_areas
    isEnabled() : %s
    isHidden() : %s
    isVisible() : %s
    isActiveWindow() : %s
    pixmap() : %s
    
    self.area_drawing_board
    isEnabled() : %s
    isHidden() : %s
    isVisible() : %s
    isActiveWindow() : %s
    pixmap() : %s
    
            """% (self.screen_conf.isEnabled(),
                self.screen_conf.isHidden(),
                self.screen_conf.isVisible(),
                self.screen_conf.isActiveWindow(),
                self.camera_thread_conf.isRunning(),
                self.camera_thread_conf.stop,
                self.area_areas.isEnabled(),
                self.area_areas.isHidden(),
                self.area_areas.isVisible(),
                self.area_areas.isActiveWindow(),
                self.area_areas.pixmap(),
                self.area_drawing_board.isEnabled(),
                self.area_drawing_board.isHidden(),
                self.area_drawing_board.isVisible(),
                self.area_drawing_board.isActiveWindow(),
                self.area_drawing_board.pixmap()
                  )
            print(infor_str)
            QMessageBox.information(self,'debug',infor_str)
        except Exception as e:
            QMessageBox.information(self,'debug',"error \n%s" % (e))



    def bar_click(self ,event):
        if event.button() == Qt.LeftButton:
            self.m_flag = True
            self.m_Position = event.globalPos() - self.pos()
            event.accept()

    def bar_move(self, QMouseEvent):
        if Qt.LeftButton and self.m_flag:
            self.move(QMouseEvent.globalPos() - self.m_Position)
            QMouseEvent.accept()

    def bar_release(self, QMouseEvent):
        self.m_flag = False




    def when_down(self):
        self.setWindowState(self.windowState() | QWindow.Minimized)

    def when_up(self):
        self.setWindowState(self.windowState())
        self.showNormal()


    @pyqtSlot()
    def when_program_start(self):
        if self.re_login:
            self.setWindowState(self.windowState() | QWindow.Minimized)
            self.setWindowState(self.windowState() | QWindow.Maximized)


        self.msg_bar_starter()
        self.now_time_thread.start()



    @pyqtSlot()
    def now_time_Indicator(self):
        self.count_table.item(0, 0).setText("%s\n%s" % (QDate.currentDate().toString('yyyy-MM-dd'),QTime.currentTime().toString()))



    def msg_bar_starter(self):
        all_device_list.clear()
        self.status_data_list.clear()
        self.t_index = 0
        for key, value in self.user_devices_data.items():
            all_device_list.append(key)
            self.status_data_list.append((key, None, None, None, None, None, None))
        if all_device_list and not self.response_thread.isRunning():
            self.response_thread.stop = False
            self.response_thread.start()
            self.scheduler.start(3000)



    def page_change(self, page_num):
        # 0:대시보드 1:섹션관리 2:사용자관리 3:configuration 4:AdminTool 5:history 6:report
        if self.current_index == page_num: #페이지 번호가 안바뀌면 아무 행동 안함
            return
        delete_memory_all()

        if self.camera_thread.isRunning():
            self.camera_thread.stop = True
            self.stop_all_camera()
        if self.dot_thread.isRunning():
            self.dot_thread.stop = True
        if self.camera_thread_conf.isRunning():
            self.camera_thread_conf.stop = True
        if self.dot_thread_conf.isRunning():
            self.dot_thread_conf.stop = True
        if self.camera_thread_admin.isRunning():
            self.camera_thread_admin.stop = True


        if page_num == 0: #대시보드

            self.load_tree_dashboard()

        elif page_num == 1: #섹션

            self.area_sinfor.hide()
            self.load_tree_section()
            self.load_node_data()
            self.load_node_children_data()

        elif page_num == 2: #유저
            self.create_user_list_table()
            self.init_user_manage_page()
            self.load_user_list_data()

        elif page_num == 3: #컨피그
            self.conf_tasking = False
            self.init_conf_page()
            self.load_tree_conf()
        elif page_num == 4: #어드민툴
            # answer = QMessageBox.question(self, '물음', '어드민 툴 진입시 감시 기능이 중단 됩니다\n진입하시겠습니까?', QMessageBox.Yes | QMessageBox.No)
            # if answer != QMessageBox.Yes:
            #     return
            #os.system("C:\g-pms-ir\pms_server\ir_stop.bat")
            self.init_admin_page()
            self.screen_admin.setScene(QGraphicsScene())
            self.load_admin_device()

        elif page_num == 5: #히스토리
            self.new_video_path = None
            self.video.video_path = None
            self.video_name = None
            self.video.stop()
            self.btn_his_play.show()
            self.btn_his_play_b.show()
            self.btn_his_pause.hide()
            self.btn_his_pause_b.hide()
            self.load_tree_his()
            self.label_loading.hide()
            self.area_file_name.hide()
            self.screen_his_blinder.show()
            self.label_pt.setText("00:00:00")
            self.label_fpt.setText("00:00:00")
            self.label_cam_fam_info2.setText("")
            self.d1.setDate(QDate.currentDate())
            self.d1.setTime(QTime.currentTime().addSecs(-3600))
            self.d2.setDate(QDate.currentDate())
            self.d2.setTime(QTime.currentTime())
            self.list_his.clearContents()
            self.list_his.setRowCount(0)


        elif page_num == 6: #레포트
            self.label_loading2.hide()
            self.label_cam_fam_info3.setText("")
            self.load_tree_report()
            self.d3.setDate(QDate.currentDate())
            self.d3.setTime(QTime.currentTime().addSecs(-3600))
            self.d4.setDate(QDate.currentDate())
            self.d4.setTime(QTime.currentTime())
            self.cb_report : QComboBox
            self.cb_report.setCurrentIndex(0)
            self.list_rep.clearContents()
            self.list_rep.setRowCount(0)
            self.graph_canvas.parent().hide()
            self.btn_graph_save.hide()
            self.btn_data_save.hide()




        self.tabWidget.setCurrentIndex(page_num)
        self.current_index = page_num

    def keyPressEvent(self, e:QKeyEvent):


        if e.key() == Qt.Key_Escape or e.key() == Qt.Key_F11:
            if is_screen_big_mod():
                self.screen_zoom(self.big_index)
            else:
                if self.area_fullscreen.isVisible():
                    self.to_normal_screen()

        elif e.key() == Qt.Key_F1:
            self.test1()
        elif e.key() == Qt.Key_F2:
            self.test2()
        elif e.key() == Qt.Key_F3:
            self.test3()


    ###<대시보드>###
    def menu_btn_clicked(self):
        self.menu_box.show()
        self.menu_box.showPopup()

    def menu_changed(self, index):
        self.menu_box : QComboBox
        item_str = self.menu_box.itemText(index)

        if item_str == 'Section 관리':
            self.page_change(1)
        elif item_str == '사용자 관리':
            self.page_change(2)
        elif item_str == 'S/W 버전 정보':
            QMessageBox.information(self, '알림', 'S/W Vers : ' + sw_ver
                                    + "\n최종 수정일자 : " + sw_date)
        elif item_str == '로그아웃':
            self.log_out()
        elif item_str == '프로그램 종료':
            self.close_program()

    def log_out(self):
        answer = QMessageBox.question(self, '물음', '로그아웃 하시겠습니까?', QMessageBox.Yes | QMessageBox.No)
        if answer == QMessageBox.Yes:
            self.delay_executor.stop = True
            self.re_login = True
            self.response_thread.stop = True
            self.blink_animation.stop()
            self.scheduler.stop()
            if self.camera_thread.isRunning():
                self.camera_thread.stop = True
                self.stop_all_camera()
            if self.dot_thread.isRunning():
                self.dot_thread.stop = True
            if self.camera_thread_conf.isRunning():
                self.camera_thread_conf.stop = True
            if self.dot_thread_conf.isRunning():
                self.dot_thread_conf.stop = True
            if self.camera_thread_admin.isRunning():
                self.camera_thread_admin.stop = True

            self.count_table.item(1, 1).setText("")
            self.count_table.item(1, 2).setText("")
            self.btn_red.setText("")
            self.btn_orange.setText("")
            self.btn_yellow.setText("")
            self.global_msg_list[0].setStyleSheet("background-color:green;color:white;")
            self.global_msg_list[0].setText(" 재시작 중... ")
            self.global_msg_list[1].setStyleSheet("background-color:green;color:white;")
            self.global_msg_list[1].setText(" 재시작 중... ")
            self.global_msg_list[0].move(0, 0)
            delete_memory_all()

            self.hide()
            loginWindow.show()


    def close_program(self):
        answer = QMessageBox.question(self, '물음', '종료 하시겠습니까?', QMessageBox.Yes | QMessageBox.No)
        if answer == QMessageBox.Yes:
            sys.exit()

    def create_slot(self):
        for i in range(self.slot_count):
            self.slot_list[i]: QWidget

            v_layout = QVBoxLayout()
            self.slot_list[i].setLayout(v_layout)
            v_layout.setContentsMargins(0,0,0,0)
            v_layout.setSpacing(0)
            self.slot_list[i].setSizePolicy(QSizePolicy.Fixed ,QSizePolicy.Fixed)
            self.slot_list[i].setMinimumSize(330, 310)
            self.slot_list[i].setMaximumSize(330, 310)
            self.slot_list[i].resize(330,310)
            self.slot_list[i].setStyleSheet("border:none")

            screen_area = QWidget()
            screen_area.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
            screen_area.setMaximumHeight(250)
            screen_area.setStyleSheet("border:2 solid lightgray")
            screen =QGraphicsView(screen_area)
            screen.setGeometry(5, 5, 320, 240)
            screen.setStyleSheet("border:none;background-color:black;")
            self.screen_list.append(screen)
            dot = QLabel(screen_area)
            dot.setGeometry(5, 5, 320, 240)
            dot.setStyleSheet("border:none;background-color:transparent;")
            dot.raise_()
            dot.mousePressEvent = lambda e, index = i: self.screen_click(e, index)
            dot.mouseDoubleClickEvent = lambda e, index = i: self.screen_zoom(index)
            self.dot_area_list.append(dot)



            info_area = QWidget()
            h_layout = QHBoxLayout()
            info_area.setLayout(h_layout)
            info_area.setStyleSheet("border:2 solid white;")
            self.info_area_list.append(info_area)


            self.status_list[i].setText("")
            self.status_list[i].setStyleSheet("border:none;")
            self.name_list[i].setStyleSheet("border:none;")
            self.thermometer_list[i].setStyleSheet("border:none;")
            self.status_list[i].setFont(QtGui.QFont("돋움", int(320 / 20)))
            self.status_list[i].setLayoutDirection(Qt.RightToLeft)
            self.status_list[i].setAlignment(Qt.AlignLeft|Qt.AlignVCenter)
            self.name_list[i].setFont(QtGui.QFont("돋움", int(320 / 20)))
            self.name_list[i].setAlignment(Qt.AlignLeft|Qt.AlignVCenter)
            self.thermometer_list[i].setFont(QtGui.QFont("돋움", int(320 / 20)))
            self.thermometer_list[i].setAlignment(Qt.AlignRight|Qt.AlignVCenter)

            h_layout.addWidget(self.status_list[i])
            h_layout.addWidget(self.name_list[i])
            h_layout.addWidget(self.thermometer_list[i])

            v_layout.addWidget(screen_area)
            v_layout.addWidget(info_area)


    def create_slot_area(self):
        for key,value in self.slot_areas_dict.items():
            dst_widget = value["widget"]
            hb_layout = QHBoxLayout(dst_widget)
            q_scroll = QScrollArea(dst_widget)
            q_scroll.setWidgetResizable(True)
            q_scroll_area = QWidget()
            gg_layout = QGridLayout(q_scroll_area)
            self.slot_areas_dict[key]['layout'] = gg_layout
            q_scroll.setWidget(q_scroll_area)
            hb_layout.addWidget(q_scroll)

    def move_slots(self,dst_name,col):
        dst_data = self.slot_areas_dict[dst_name]
        layout = dst_data['layout']
        for i, widget in enumerate(self.slot_list):
            layout.addWidget(widget, int(i / col), int(i % col))



    def set_slot_area(self, dst,col_num):

        hb_layout = QHBoxLayout(dst)
        q_scroll = QScrollArea(dst)
        q_scroll.setWidgetResizable(True)
        q_scroll_area = QWidget()
        gg_layout = QGridLayout(q_scroll_area)
        q_scroll.setWidget(q_scroll_area)
        hb_layout.addWidget(q_scroll)
        for j, widget in enumerate(self.slot_list):
            gg_layout.addWidget(widget, int(j / col_num), int(j % col_num))

    def update_slot_size(self,width,height):

        for i in range(self.slot_count):
            self.slot_list[i].setMinimumSize(width, height)#슬롯 크기를 정해줌
            self.slot_list[i].setMaximumSize(width, height)#슬롯 크기를 정해줌
            self.slot_list[i].resize(width, height)#슬롯 크기를 정해줌
            self.slot_list[i].show()
            self.slot_list[i].children()[1].setMinimumHeight(int(height*0.807))
            self.slot_list[i].children()[1].setMaximumHeight(int(height*0.807))
            self.slot_list[i].children()[1].show()
            self.slot_list[i].children()[2].setMinimumHeight(int(height*0.193))
            self.slot_list[i].children()[2].setMaximumHeight(int(height*0.193))
            self.slot_list[i].children()[2].show()
            self.status_list[i].setFont(QtGui.QFont("돋움", int(width/20)))
            self.name_list[i].setFont(QtGui.QFont("돋움", int(width/20)))
            self.thermometer_list[i].setFont(QtGui.QFont("돋움", int(width/20)))


    def update_slot_children_size(self):

        parent0 = self.screen_list[0].parent()
        pw = parent0.width()
        ph = parent0.height()


        self.camera_thread.width = pw-10
        self.camera_thread.height = ph-10
        self.screen_width = pw-10
        self.screen_height = ph-10

        for i in range(self.slot_count):
            self.slot_list: QWidget
            self.screen_list[i].setGeometry(5,5,pw-10,ph-10)
            self.dot_area_list[i].setGeometry(5,5,pw-10,ph-10)

    def create_info_area(self):

        #큰 메세지 영역
        self.info_bar = QWidget()
        h_lay = QHBoxLayout()
        h_lay.setContentsMargins(0,0,0,0)
        h_lay.setSpacing(0)
        self.info_bar.setLayout(h_lay)

        self.msg_bar = QWidget()
        self.msg_bar.setStyleSheet("border:1 solid black;")

        msg_0 = QLabel(self.msg_bar)
        msg_0.setStyleSheet("background-color:green;color:white;")
        msg_0.setFont(QtGui.QFont("돋움", 30))
        msg_0.setText(" G - PMS ")

        msg_1 = QLabel(self.msg_bar)
        msg_1.setStyleSheet("background-color:blue;color:white;")
        msg_1.setFont(QtGui.QFont("돋움", 30))

        #수량 영역
        self.global_msg_list = []
        self.global_msg_list.append(msg_0)
        self.global_msg_list.append(msg_1)

        self.count_bar = QWidget()
        self.count_bar.setStyleSheet("border:none;")
        self.count_bar.setWhatsThis('count_what')
        g_lay2 = QGridLayout()
        self.count_bar.setLayout(g_lay2)
        g_lay2.setContentsMargins(0,0,0,0)
        g_lay2.setSpacing(0)


        self.count_table = QTableWidget(2,6)
        self.count_table.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.count_table.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.count_table.setStyleSheet("border:1 solid white;")
        self.count_table.setContentsMargins(0,0,0,0)
        self.count_table.horizontalHeader().setVisible(False)
        self.count_table.verticalHeader().setVisible(False)
        self.count_table.setEditTriggers(QAbstractItemView.NoEditTriggers)

        for i in range(12):
            row = int(i / 6)
            col = i % 6
            self.count_table.setItem(row, col,QTableWidgetItem(""))
            self.count_table.item(row, col).setTextAlignment(Qt.AlignCenter)
            self.count_table.item(row, col).setFlags(Qt.NoItemFlags)


        #self.count_table.item(0, 0).setText("")
        self.count_table.item(0, 1).setText("총수량")
        self.count_table.item(0, 2).setText("정상")
        self.count_table.item(0, 3).setText("주의")
        self.count_table.item(0, 4).setText("경고")
        self.count_table.item(0, 5).setText("알람")
        self.count_table.item(1, 0).setText("현황 알림")

        self.count_table.item(1, 0).setBackground(QBrush(Qt.black))
        self.count_table.item(1, 0).setForeground(QBrush(Qt.white))
        self.count_table.item(1, 1).setBackground(QBrush(Qt.white))
        self.count_table.item(1, 1).setForeground(QBrush(Qt.black))
        self.count_table.item(1, 2).setBackground(QBrush(QtGui.QColor(status_b_color[0])))#QBrush(QtGui.QColor("blue"))
        self.count_table.item(1, 2).setForeground(QBrush(QtGui.QColor(status_color[0])))


        self.btn_yellow = QPushButton()
        self.btn_yellow.setStyleSheet("border:none;background-color:{};color:{};".format(status_b_color[1], status_color[1]))
        self.count_table.setCellWidget(1, 3, self.btn_yellow)
        self.btn_orange = QPushButton()
        self.btn_orange.setStyleSheet("border:none;background-color:{};color:{};".format(status_b_color[2], status_color[2]))
        self.count_table.setCellWidget(1, 4, self.btn_orange)
        self.btn_red = QPushButton()
        self.btn_red.setStyleSheet("border:none;background-color:{};color:{};".format(status_b_color[3], status_color[3]))
        self.count_table.setCellWidget(1, 5, self.btn_red)

        self.btn_yellow.setCursor(QCursor(Qt.PointingHandCursor))
        self.btn_orange.setCursor(QCursor(Qt.PointingHandCursor))
        self.btn_red.setCursor(QCursor(Qt.PointingHandCursor))

        self.btn_yellow.clicked.connect(lambda state, this = self.btn_yellow :self.show_alert(this, [1]))
        self.btn_orange.clicked.connect(lambda state, this = self.btn_orange : self.show_alert(this, [2]))
        self.btn_red.clicked.connect(lambda state, this = self.btn_red : self.show_alert(this, [3, 4]))

        g_lay2.addWidget(self.count_table)
        h_lay.addWidget(self.msg_bar)
        h_lay.addWidget(self.count_bar)

    def move_info_area(self, dst_name):

        dst = self.slot_areas_dict[dst_name]['info']
        dst.addWidget(self.info_bar)



    def update_info_area_size(self):

        #큰 메세지 영역
        parent = self.info_bar.parent()
        p_w = parent.width()
        p_h = parent.height()
        self.msg_bar.setFixedWidth(int(p_w*0.65))
        self.msg_bar.setFixedHeight(p_h)


        self.count_bar.setFixedWidth(int(p_w*0.35))
        self.count_bar.setFixedHeight(p_h)

        msg_geo = self.msg_bar.geometry()
        msg_w = msg_geo.width()
        msg_h = msg_geo.height()


        self.scroll_animation.stop()
        self.t_count = 0

        self.global_msg_list[0].setGeometry(0,0,msg_w,msg_h)
        self.global_msg_list[1].setGeometry(0,msg_h,msg_w,msg_h)

        self.global_msg_list[0].setFont(QtGui.QFont("돋움", int(msg_w/45)))
        self.global_msg_list[1].setFont(QtGui.QFont("돋움", int(msg_w/45)))

        #수량 영역
        p_w2 = self.count_bar.width()
        p_h2 = self.count_bar.height()


        self.count_table.setFixedSize(p_w2-2,p_h2-2)
        self.count_table.setColumnWidth(0,int(p_w2/3.7))
        self.count_table.setColumnWidth(1,int(p_w2/6.5))
        self.count_table.setColumnWidth(2,int(p_w2/7))
        self.count_table.setColumnWidth(3,int(p_w2/7))
        self.count_table.setColumnWidth(4,int(p_w2/7))
        self.count_table.setColumnWidth(5,int(p_w2/7))
        self.count_table.setRowHeight(0, int(p_h2/2)-3)
        self.count_table.setRowHeight(1, int(p_h2/2)-3)

        count_table_font_size = int(p_w2 / 33)
        self.count_table.item(0, 0).setFont(QtGui.QFont("돋움", int(p_h2 / 7)))

        for i in range(1, 12):
            row = int(i / 6)
            col = i % 6
            self.count_table.item(row, col).setFont(QtGui.QFont("돋움", count_table_font_size))


        self.btn_red.setFont(QtGui.QFont("돋움", count_table_font_size))
        self.btn_orange.setFont(QtGui.QFont("돋움", count_table_font_size))
        self.btn_yellow.setFont(QtGui.QFont("돋움", count_table_font_size))


        self.count_table.raise_()



    def change_msg_bar_msg(self):

        if self.t_command == 1:
            try:
                if self.status_data_list[self.t_index]:
                    dev_id, dev_nm, parent_id, parent_name, level, temp, time  = self.status_data_list[self.t_index]
                    if level != None:
                        self.global_msg_list[1].setText(
                            "[%s]%s %s %s (%s°C)" % (time, status_str_list[level], parent_name, dev_nm, str(temp))
                        )
                        self.global_msg_list[1].setStyleSheet(style_dict["info_area_style2"].replace("$bcolor",status_b_color[level]).replace("$color",status_color[level]))
                    else:
                        self.global_msg_list[1].setText(
                            "[연결 끊김] %s %s" % (parent_name, dev_nm)
                        )
                        self.global_msg_list[1].setStyleSheet(style_dict["info_area_style2"].replace("$bcolor","gray").replace("$color","white"))


                    self.max_count = self.msg_bar.height()
                    self.scroll_animation.start(5)
                if self.t_index+1 >= len(self.status_data_list):
                    self.t_index = 0
                else:
                    self.t_index += 1
            except:
                self.t_index = 0

        elif self.t_command == 2:
            try:
                self.blink_animation.stop()
                self.global_msg_list[0].setText(self.t_blink_msg)
                self.t_index = 0
                self.blink_animation.start(200)
            except:
                pass


    def scoll_msg_bar(self):
        if self.max_count == self.t_count:
            self.scroll_animation.stop()
            self.global_msg_list[0].move(0, self.global_msg_list[0].parent().height())
            self.global_msg_list.reverse()
            self.t_count = 0

        self.global_msg_list[0].move(self.global_msg_list[0].x(), self.global_msg_list[0].y() - 1)
        self.global_msg_list[1].move(self.global_msg_list[1].x(), self.global_msg_list[1].y() - 1)
        self.t_count += 1

    def blink_msg_bar(self):
        self.blink_colors.reverse()
        self.global_msg_list[0].setStyleSheet(style_dict["info_area_style2"].replace("$bcolor", self.blink_colors[1]).replace("$color", self.blink_colors[0]))
        if self.now_highst_level == 4:
            self.tabWidget.setStyleSheet(style_dict["info_area_style3"].replace("$bcolor", self.blink_colors[1]).replace("$color", self.blink_colors[0]))
            self.area_slots_fullscreen.setStyleSheet(style_dict["info_area_style3"].replace("$bcolor", self.blink_colors[1]).replace("$color", self.blink_colors[0]))


    def screen_click(self, e, sc_num):

        index = int(sc_num)

        if len(play_device_list) <= 0 or index > len(play_device_list) - 1:
            #QMessageBox.warning(self, '경고', "재생중인곳을 눌러주세요.", QMessageBox.Yes)
            return

        screen0 = self.screen_list[0]

        screen_width = screen0.width()
        screen_height = screen0.height()

        ex = e.x()
        ey = e.y()

        rx = math.floor( ex * 160 /screen_width)
        ry = math.floor( ey * 120/screen_height)

        device_id = play_device_list[index]

        try:
            db = Database()
            result = db.merge_mouse_loc(device_id, rx, ry)
        except:
            QMessageBox.warning(self, '경고', "익셉션 발생.", QMessageBox.Yes)

        if result:
            pass
        else:
            QMessageBox.warning(self, '경고', "실패.", QMessageBox.Yes)

        #eval("self.status{}".format(str(index))).setText(str(math.floor(e.x() / 2)) + "," + str(math.floor(e.y() / 2)))

        self.mouse_loc_dict[device_id] = [ex, ey]

    def screen_zoom(self, index):


        if len(play_device_list) <= 0 or index > len(play_device_list) - 1:
            #QMessageBox.warning(self, '경고', "재생중인곳을 눌러주세요.", QMessageBox.Yes)
            return


        #줌 할떄 마우스 좌표 삭제
        if self.mouse_loc_dict.get(play_device_list[index]):
            del self.mouse_loc_dict[play_device_list[index]]
            self.dot_area_list[index].setPixmap(QtGui.QPixmap())

        if is_screen_big_mod():

            set_big_mod(False)

            # src_data = get_memory_all()


            for i in range(self.slot_count):
                self.slot_list[i].show()


            if self.isFullScreen():
                area_data = self.area_slots_fullscreen.geometry()
                a_width = area_data.width()
                a_height = area_data.height()
                slot_width = int(a_width / 4) - 20
                slot_height = int(a_height / 3) - 20
            else:
                slot_width = 330
                slot_height = 310

            self.update_slot_size(slot_width, slot_height)
            self.update_slot_children_size()


        else:
            set_big_mod(True)
            self.big_index = index
            src_data = self.slot_list[index].geometry()

            # set_memory("src_x", src_data.x())
            # set_memory("src_y", src_data.y())
            # set_memory("src_width", src_data.width())
            # set_memory("src_height", src_data.height())
            # set_memory("src_index", index)

            f_width = int(self.size().width()*0.65)
            f_height = int(self.size().height()*0.65)

            self.update_slot_size(f_width, f_height)
            self.update_slot_children_size()

            for i in range(self.slot_count):
                self.slot_list[i].hide()

            self.slot_list[index].show()

            return

            # 마우스 좌표 수정
            # device_id = play_device_list[index]
            # mouse_data = self.mouse_loc_dict.get(device_id)
            # if mouse_data:
            #     self.mouse_loc_dict[device_id] = [mouse_data[0] * ratio, mouse_data[1] * ratio]
            #
            # eval("self.screen{}".format(str(index))).setGeometry(0, 0, screen_width * ratio, screen_height * ratio)
            # eval("self.area_dot{}".format(str(index))).setGeometry(0, 0, screen_width * ratio, screen_height * ratio)
            # set_ratio(ratio)

    def to_full_screen(self):

        # if len(play_device_list) < 1:
        #     QMessageBox.warning(self, '경고', '재생중인 화면이 없습니다.')
        #     return
        if is_screen_big_mod():
            self.screen_zoom(self.big_index)


        before_s_width = self.screen_list[0].width()
        before_s_height = self.screen_list[0].height()

        self.showFullScreen()
        self.area_top.hide()
        self.menubar.hide()
        self.area_title.hide()
        self.line_1.hide()
        self.line_2.hide()
        self.line_3.hide()
        self.tabWidget.hide()
        self.area_fullscreen.show()


        full_screen_data = self.geometry()
        f_width = full_screen_data.width()
        f_height = full_screen_data.height()

        self.area_fullscreen.setGeometry(0,0,f_width,f_height)
        self.area_fullscreen.setStyleSheet(style_dict['default_outer'])

        self.area_slots_fullscreen : QWidget
        self.area_slots_fullscreen.resize(f_width,int((f_height*90)/100))
        self.area_slots_fullscreen.setMinimumSize(f_width,int((f_height*90)/100))
        self.area_slots_fullscreen.setMaximumSize(f_width,int((f_height*90)/100))

        self.area_info_fullscreen.resize(f_width, int((f_height * 10) / 100))
        self.area_info_fullscreen.setMinimumSize(f_width,int((f_height*10)/100))
        self.area_info_fullscreen.setMaximumSize(f_width,int((f_height*10)/100))


        area_data = self.area_slots_fullscreen.geometry()
        a_width = area_data.width()
        a_height = area_data.height()


        slot_width = int(a_width/4) - 20
        slot_height = int(a_height/3) - 20

        self.move_slots("fullscreen",4)

        self.update_slot_size(slot_width, slot_height)
        self.update_slot_children_size()

        self.move_info_area("fullscreen")
        self.update_info_area_size()

        after_s_width = self.screen_list[0].width()
        after_s_height = self.screen_list[0].height()
        self.update_mouse_loc(after_s_width / before_s_width, after_s_height / before_s_height)

        QMessageBox.information(self, '알림', '전체화면을 종료하려면 ESC 또는 F11을 누르세요.')


    def to_normal_screen(self):


        before_s_width = self.screen_list[0].width()
        before_s_height = self.screen_list[0].height()

        self.showNormal()
        self.resize(1440,930)
        self.area_top.show()
        self.menubar.show()
        self.area_title.show()
        self.line_1.show()
        self.line_2.show()
        self.line_3.show()
        self.tabWidget.show()
        self.area_fullscreen.hide()
        self.center()

        self.move_slots("dashboard",3)
        self.update_slot_size(330, 310)
        self.update_slot_children_size()

        self.move_info_area("dashboard")
        self.update_info_area_size()


        after_s_width = self.screen_list[0].width()
        after_s_height = self.screen_list[0].height()
        self.update_mouse_loc(after_s_width/before_s_width, after_s_height/before_s_height)


    def update_mouse_loc(self, w_ratio, h_ratio):
        for key, value in self.mouse_loc_dict.items():
            self.mouse_loc_dict[key][0] = round(value[0]*w_ratio)
            self.mouse_loc_dict[key][1] = round(value[1]*h_ratio)

    def center(self):
        # qr = self.frameGeometry()
        # cp = QDesktopWidget().availableGeometry().center()
        # qr.moveCenter(cp)
        # self.move(qr.topLeft())
        frameGm = self.frameGeometry()
        screen = QApplication.desktop().screenNumber(QApplication.desktop().cursor().pos())
        centerPoint = QApplication.desktop().screenGeometry(screen).center()
        frameGm.moveCenter(centerPoint)
        self.move(frameGm.topLeft())


    def stop_all_camera(self):

        if is_screen_big_mod():
            self.screen_zoom(self.big_index)
        self.camera_thread.stop = True
        self.dot_thread.stop = True
        scene = QGraphicsScene()
        self.mouse_loc_dict = {}
        self.select_camera_index = self.slot_count + 1

        for i in range(len(play_device_list)):
            self.screen_list[i].setScene(scene)
            self.pm_list[i].fill(Qt.transparent)
            self.dot_area_list[i].setPixmap(QtGui.QPixmap(self.pm_list[i]))
            self.status_list[i].setText("")
            self.name_list[i].setText("")
            self.thermometer_list[i].setText("")
            self.info_area_list[i].setStyleSheet("border:2 solid white;color:white;background-color:#25253A;")

        for btn in self.play_btn_list.values():
            btn.setStyleSheet(style_dict['play_btn'])
            #btn.setIcon(qta.icon("fa.play-circle", color="green"))

        for btn in self.camera_btn_list.values():
            btn.setStyleSheet(style_dict['cam_btn'])

        for i, btn in enumerate(self.camera_btn_list.values()):
            btn.setStyleSheet(style_dict['cam_btn'])

        for info_area in self.info_area_list:
            info_area.setStyleSheet(info_area.styleSheet() + "border:2 solid white;")

        #self.screen_list[i].parent().setStyleSheet("border:2 solid lightgray;")
        for screen in self.screen_list:
            screen.parent().setStyleSheet("border:2 solid lightgray;")

        play_device_list.clear()
        self.camera_thread.stop = True
        self.dot_thread.stop = True


    def select_camera(self, device_id):
        try:
            for i, btn in enumerate(self.camera_btn_list.values()):
                btn.setStyleSheet(style_dict['cam_btn'])

            for info_area in self.info_area_list:
                info_area.setStyleSheet(info_area.styleSheet() + "border:2 solid white;")

            # self.screen_list[i].parent().setStyleSheet("border:2 solid lightgray;")
            for screen in self.screen_list:
                screen.parent().setStyleSheet("border:2 solid lightgray;")


            if device_id in play_device_list:
                index = play_device_list.index(device_id)
                self.camera_btn_list[device_id].setStyleSheet("Text-align:left;border:none;color:blue;background-color:#47C83E;")
                self.screen_list[index].parent().setStyleSheet("border:3 solid #47C83E;")
                self.info_area_list[index].setStyleSheet(self.info_area_list[index].styleSheet() + "border:3 solid #47C83E;")
                self.select_camera_index = index
            else:
                self.select_camera_index = self.slot_count + 1
        except Exception as e:
            print(e)

    def show_camera(self, device_id, name):
        if device_id in play_device_list:
            QMessageBox.warning(self, '경고', "이미 선택 되었습니다.", QMessageBox.Yes)
            return

        length = len(play_device_list)
        if length >= self.slot_count:
            QMessageBox.warning(self, '경고', "화면이 최대치 입니다.", QMessageBox.Yes)
            return

        # if not Database().is_device_on(device_id):
        #     QMessageBox.warning(self, '경고', "데이터가 없습니다.", QMessageBox.Yes)
        #     return

        self.camera_thread.stop = True
        self.dot_thread.stop = True

        self.status_list[length].setText("[동작]")
        self.name_list[length].setText(name)

        play_device_list.append(device_id)

        self.camera_thread.stop = False
        self.dot_thread.stop = False

        self.camera_thread.start()
        self.dot_thread.start()


    def show_camera_multi(self, id):
        try:
            self.stop_all_camera()
            self.camera_thread.stop = True
            self.dot_thread.stop = True

            self.play_btn_list[str(id)].setStyleSheet("Text-align:left;border:none;color:green;background-color:#FFFFCC;")
            self.play_btn_list[str(id)].setIcon(qta.icon("fa.play-circle", color='green'))


            if self.family34_list[id] :
                for i, child in enumerate(self.family34_list[id]):
                    if i < self.slot_count:
                        self.status_list[i].setText("[동작]")
                        self.name_list[i].setText(child['name'])
                        play_device_list.append(child['dev_id'])
                        #self.camera_btn_list[child['dev_id']].setStyleSheet("Text-align:left;border:none;color:blue;background-color:#FFFFCC;")
                        # self.camera_btn_list[child['dev_id']].setIcon(qta.icon("fa5s.camera", color='white'))

                self.camera_thread.stop = False
                self.dot_thread.stop = False

                self.camera_thread.start()
                self.dot_thread.start()

        except Exception as e:
            print(e)

    @pyqtSlot(list)
    def image_data_receive(self, img_data_list):

        try:
            no_data_device_list = play_device_list[:]
            if img_data_list:
                for row in img_data_list:
                    no_data_device_list.remove(row['dev_id'])
                    index = play_device_list.index(row['dev_id'])
                    img = parser2(row['image_latest'])
                    h, w, c = img.shape
                    qImg = QtGui.QImage(img.data, w, h, w * c, QtGui.QImage.Format_RGB888)
                    scene = QGraphicsScene()
                    tempmap = QtGui.QPixmap.fromImage(qImg)
                    scene.addPixmap(tempmap.scaled(self.screen_width,self.screen_height, QtCore.Qt.IgnoreAspectRatio))
                    self.screen_list[index].setScene(scene)

            if no_data_device_list:
                scene = QGraphicsScene()
                for device_id in no_data_device_list:
                    i = play_device_list.index(device_id)
                    self.screen_list[i].setScene(scene)
        except:
            pass


    @pyqtSlot(list)
    def dot_data_receive(self, data_list):
        try:
            no_data_device_list = play_device_list[:]
            center_x = self.screen_width / 2
            center_y = self.screen_height / 2
            for data in data_list:
                no_data_device_list.remove(data['dev_id'])
                index = play_device_list.index(data['dev_id'])
                mouse_loc = self.mouse_loc_dict.get(data['dev_id'])
                self.pm_list[index] = self.pm_list[index].scaled(self.screen_width, self.screen_height,QtCore.Qt.IgnoreAspectRatio)
                self.pm_list[index].fill(Qt.transparent)
                f_size = self.screen_width
                painter = QtGui.QPainter(self.pm_list[index])
                painter.setFont(QFont("돋움", f_size / 30))
                painter.setPen(QPen(Qt.white, int(f_size / 60), Qt.SolidLine))

                n_width = self.screen_list[index].width()
                n_height = self.screen_list[index].height()


                for k in range(1, 7):

                    if data['area{}_onoff'.format(str(k))] == 1:

                        painter.setPen(QPen(QtGui.QColor(self.areas_colors[k-1]), int(f_size / 70), Qt.SolidLine))
                        painter.drawRect(int(data['area{}_x_start'.format(str(k))] * (n_width/160)),
                                         int(data['area{}_y_start'.format(str(k))] * (n_height/120)),
                                         int(data['width{}'.format(str(k))] * (n_width/160)),
                                         int(data['height{}'.format(str(k))] * (n_height/120)))


                        #st = QStaticText("AREA{}:{}°".format(str(k), str(data['area{}_top'.format(str(k))])))
                        st = QStaticText("AREA{}\n{}°C".format(str(k) ,str(data['area{}_top'.format(str(k))])))
                        st.setTextWidth(1.0)
                        painter.drawStaticText(int(data['area{}_x_start'.format(str(k))] * (n_width/160))+5, int(data['area{}_y_start'.format(str(k))] * (n_height/120))+5, st)


                for j in range(3, 0, -1):

                    painter.setPen(QPen(QtGui.QColor(spot_color[j-1]), int(f_size / 60), Qt.SolidLine))

                    rx = int(data['spot{}_x'.format(str(j))] * (n_width/160))
                    ry = int(data['spot{}_y'.format(str(j))] * (n_height/120))

                    rev_x_value = 0 if data['spot{}_x'.format(str(j))] < 80 else -int(n_width / 5)
                    rev_y_value = 0 if data['spot{}_y'.format(str(j))] < 60 else -int(n_height / 15)

                    st = QStaticText("s{}:{}°C".format(str(j), str(data['spot{}_temp'.format(str(j))])))
                    painter.drawPoint(rx, ry)
                    painter.drawStaticText(rx + rev_x_value, ry + rev_y_value, st)




                if mouse_loc:
                    painter.setPen(QPen(Qt.white, int(f_size / 60), Qt.SolidLine))
                    loc_str_x_value = int(f_size/160) if mouse_loc[0] < center_x else -int(f_size/20)
                    loc_str_y_value = int(f_size/160) if mouse_loc[1] < center_y else -int(f_size/20)
                    st = QStaticText(str(data['mouse_temp']) + "°C")
                    painter.drawPoint(mouse_loc[0], mouse_loc[1])
                    painter.drawStaticText(mouse_loc[0] + loc_str_x_value, mouse_loc[1] + loc_str_y_value, st)

                self.dot_area_list[index].setPixmap(QtGui.QPixmap(self.pm_list[index]))


            if no_data_device_list :
                for device_id in no_data_device_list:
                    i = play_device_list.index(device_id)
                    self.thermometer_list[i].setText("")
                    self.dot_area_list[i].setPixmap(QtGui.QPixmap())
        except Exception as e:
            print('익셉션')
            print(e)
            pass


    @pyqtSlot(list)
    def response_data_receive(self, data_list):
        try:
            highst_level = 0
            no_data_device_list = all_device_list[:]
            total_count = 0
            normal_count = 0
            self.w1_dev_list.clear()
            self.w2_dev_list.clear()
            self.alrm_dev_list.clear()
            self.flame_dev_list.clear()

            for data in data_list:
                no_data_device_list.remove(data['dev_id'])
                index = all_device_list.index(data['dev_id'])
                data['status_str'] = status_str_list[data['level']]
                device_info = self.user_devices_data.get(data['dev_id'])
                if device_info :
                    data['parent_name'] = device_info['parent_name']
                    data['parent_id'] = device_info['parent_id']
                    data['dev_nm'] = device_info['dev_nm']
                else:
                    data['parent_name'] = ""
                    data['parent_id'] = None
                    data['dev_nm'] = ""

                self.status_data_list[index] = (data['dev_id'], data['dev_nm'], data['parent_id'], data['parent_name'], data['level'], data['temp'],data['time'])

                if data['dev_id'] in play_device_list:
                    index2 = play_device_list.index(data['dev_id'])
                    self.status_list[index2].setText(data['status_str'])
                    self.thermometer_list[index2].setText("[%s°C]" % str(data['temp']))
                    if self.select_camera_index != index2:
                        self.info_area_list[index2].setStyleSheet(style_dict["info_area_style"].replace("$bcolor",status_b_color[data['level']]).replace("$color",status_color[data['level']]))
                    else:
                        self.info_area_list[index2].setStyleSheet(
                            style_dict["info_area_style4"].replace("$bcolor", status_b_color[data['level']]).replace("$color", status_color[data['level']]))
                total_count += 1
                if data['level'] == 0:
                    normal_count += 1
                elif data['level'] == 1:
                    self.w1_dev_list.append(data)
                elif data['level'] == 2:
                    self.w2_dev_list.append(data)
                elif data['level'] == 3:
                    self.alrm_dev_list.append(data)
                elif data['level'] == 4:
                    self.flame_dev_list.append(data)


            if no_data_device_list :
                for device_id in no_data_device_list:
                    total_count += 1
                    j = all_device_list.index(device_id)
                    self.status_data_list[j] = (device_id, self.user_devices_data[device_id]['dev_nm'], self.user_devices_data[device_id]['parent_id'], self.user_devices_data[device_id]['parent_name'], None, None, None)
                    if device_id in play_device_list:
                        i = play_device_list.index(device_id)
                        self.thermometer_list[i].setText("")#ei.ban-circle
                        self.status_list[i].setText("[연결 끊김]")
                        if self.select_camera_index != i:
                            self.info_area_list[i].setStyleSheet("border:2 solid white;color:red;background-color:gray;")
                        else:
                            self.info_area_list[i].setStyleSheet("border:3 solid #47C83E;color:red;background-color:gray;")

            w1_count = len(self.w1_dev_list)
            w2_count = len(self.w2_dev_list)
            alrm_count = len(self.alrm_dev_list)
            flame_count = len(self.flame_dev_list)


            self.count_table.item(1, 1).setText(str(total_count))
            self.count_table.item(1, 2).setText(str(normal_count))
            self.btn_yellow.setText(str(w1_count))
            self.btn_orange.setText(str(w2_count))
            self.btn_red.setText(str(alrm_count+flame_count))


            highst_level_list = []
            if flame_count != 0:
                highst_level = 4
                highst_level_list = self.flame_dev_list
            elif alrm_count != 0:
                highst_level = 3
                highst_level_list = self.alrm_dev_list
            elif w2_count != 0:
                highst_level = 2
                highst_level_list = self.w2_dev_list
            elif w1_count != 0:
                highst_level = 1
                highst_level_list = self.w1_dev_list


            if highst_level != 0:
                data = highst_level_list[self.blink_index]
                self.t_blink_msg = "[%s]%s %s %s (%s°C)" % (
                data['time'], data['status_str'], data['parent_name'], data['dev_nm'], str(data['temp']))
                self.blink_colors = [status_b_color[data['level']], status_color[data['level']]]
                if self.blink_index + 1 >= len(highst_level_list):
                    self.blink_index = 0
                else:
                    self.blink_index += 1

            if self.now_highst_level == 4 and highst_level != 4:
                self.tabWidget.setStyleSheet(style_dict['default_outer'])
                self.area_slots_fullscreen.setStyleSheet(style_dict['default_outer2'])

            self.now_highst_level = highst_level
            if highst_level != 0:
                self.t_command = 2
            else:
                self.t_command = 1
                self.blink_animation.stop()
        except Exception as e:
            self.blink_index = 0
            self.t_command = 1
            pass


    def load_tree_dashboard(self):

        self.tree_dashboard.clear()
        self.completed_dict = {}
        self.family34_list = {}
        self.play_btn_list = {}
        self.camera_btn_list = {}

        node_list = self.user_tree_data

        tree_data_list = Database().get_tree_data(node_list)

        if not tree_data_list:
            return

        level_0 = QTreeWidgetItem(self.tree_dashboard)
        level_0.setText(0, tree_data_list[0]['name'])
        level_0.setFont(0, QFont('돋움', 11))
        self.completed_dict[tree_data_list[0]['id']] = level_0

        for i, row in enumerate(tree_data_list[1:]):
            data = QTreeWidgetItem()
            data.setFont(0, QFont('돋움', 11))
            if row['level'] != 4:
                data.setText(0, row['name'])
            # else:
            #     data.setText(0, row['name'] + "(" + row['ip'] +")" )
            data.setForeground(0,QtGui.QBrush(QtGui.QColor(level_color[row['level']])))
            self.completed_dict[row['id']] = data
            for id in self.completed_dict.keys():
                if id == row['parent_id']:
                    self.completed_dict[id].addChild(data)
                    if row['level'] == 4:
                        self.family34_list[id].append({"dev_id": row['device_id'], "name": row['name']})

            if row['level'] == 3:
                self.family34_list[row['id']] = []
                btn = QPushButton(row['name'])
                btn.setFont(QFont('돋움', 11))
                btn.setStyleSheet(style_dict['section_tree_btn'].replace("$color", "green"))
                btn.setCursor(QCursor(Qt.PointingHandCursor))
                btn.clicked.connect(lambda state, id = row['id']: self.show_camera_multi(id))
                btn.setIcon(qta.icon("fa.play-circle", color="green"))
                btn.setWhatsThis(str(id))
                self.play_btn_list[str(id)] = btn
                self.tree_dashboard.setItemWidget(data, 0, btn)

            if row['level'] == 4:
                device_name_dict[row['device_id']] = row['name']
                btn = QPushButton("{0} ({1})".format(row['name'], row['ip']))
                btn.setIcon(qta.icon("fa5s.camera", color="blue"))
                btn.setFont(QFont('Times', 10))
                btn.setStyleSheet(style_dict['section_tree_btn'].replace("$color", level_color[row['level']]))
                btn.setCursor(QCursor(Qt.PointingHandCursor))
                btn.clicked.connect(lambda state, x=row['device_id']: self.select_camera(x))
                self.tree_dashboard.setItemWidget(data, 0, btn)
                self.camera_btn_list[row['device_id']] = btn
        self.tree_dashboard.expandAll()


    def show_alert(self,btn,level_list):
        btn_text = btn.text()
        result_list = []
        if btn_text != "" and btn_text != "0":
            for data in self.status_data_list:
                if data[4] in level_list:
                    result_list.append({"name":data[1], "parent_id": data[2],  "parent_name": data[3]})

        if result_list:
            self.alert_window.alert_list = result_list
            self.alert_window.spread_alert_list()
            self.alert_window.setWindowTitle(status_str_list[level_list[0]]+"현황")
            self.alert_window.show()
            self.alert_window.raise_()

    @pyqtSlot(int)
    def alert_window_receiver(self, parent_id):
        self.show_camera_multi(parent_id)

        #self.alert_window.show()

    ###</대시보드>###

    ###<섹션관리>
    def load_tree_section(self):

        self.tree_section.clear()

        db = Database()

        node_list = self.user_tree_data

        tree_data_list = db.get_tree_data(node_list)
        not_tree_device_list = db.get_no_parent_devices()

        if tree_data_list == False:
            QMessageBox.warning(self, '경고', "데이터를 불러오지 못했습니다.", QMessageBox.Yes)
            return
        if tree_data_list[0]['parent_id'] != None:
            QMessageBox.warning(self, '경고', "데이터가 올바르지 않습니다.", QMessageBox.Yes)
            return

        completed_dict = {}
        self.section_btn_dict = {}

        for i, row in enumerate(tree_data_list):

            if row['level'] != 0:
                data = QTreeWidgetItem()
            else:
                data = QTreeWidgetItem(self.tree_section)

            if row['level'] == 3: #(기기를 받을 수 있음(드롭) ItemIsDropEnabled)
                data.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled | Qt.ItemIsDropEnabled)
                data.setIcon(0, qta.icon("fa.play-circle", color="black"))
            elif row['level'] == 4:#(기기, 드래그 할수 있음)
                data.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled | Qt.ItemIsDragEnabled | Qt.ItemNeverHasChildren)
                data.setIcon(0, qta.icon("fa5s.camera", color="blue"))
                data.setWhatsThis(3, row['device_id'])
            else:
                data.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)

            data.setWhatsThis(0, str(row['id']))
            data.setWhatsThis(1, str(row['level']))
            data.setWhatsThis(2, "y") # 이미 트리에 등록 되어 있는 기기


            completed_dict[row['id']] = data
            for id in completed_dict.keys():
                if id == row['parent_id']:
                    completed_dict[id].addChild(data)
            if row['level'] == 4:

                data.setText(0, row['name'])
                data.setForeground(0, QtGui.QBrush(QtGui.QColor("blue")))

            else:
                btn = QPushButton(row['name'])
                btn.setWhatsThis(str(row['level']))
                btn.setFont(QFont('돋움', 11))
                btn.setStyleSheet(style_dict['section_tree_btn'].replace("$color", level_color[row['level']]))
                btn.setCursor(QCursor(Qt.PointingHandCursor))
                btn.clicked.connect(lambda state, id=row['id'], level=row['level']: self.load_node_data_all(id, level))
                self.tree_section.setItemWidget(data, 0, btn)
                self.section_btn_dict[row['id']] = btn

        if not_tree_device_list: # 배치 안 한 기기가 있으면 트리에 표시
            data01 = QTreeWidgetItem(self.tree_section)
            data01.setForeground(0,QBrush(Qt.white))
            data01.setBackground(0,QBrush(Qt.red))
            data01.setText(0,"미 배치 기기 목록")
            data01.setWhatsThis(0, None)
            for device in not_tree_device_list:
                data = QTreeWidgetItem()
                data.setWhatsThis(3, device['dev_id'])
                data.setText(0, device['dev_nm'])
                data.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled | Qt.ItemIsDragEnabled | Qt.ItemNeverHasChildren)
                data.setForeground(0, QtGui.QBrush(QtGui.QColor("blue")))
                data.setIcon(0, qta.icon("fa5s.camera", color="blue"))
                data.setWhatsThis(0, None)

                data01.addChild(data)

        if get_memory("edit_item_id"):  # 만약 편집중인 노드가 있으면 tree위젯에 표시
            level = int(get_memory("edit_item_level"))
            if level != 4:
                self.section_btn_dict[get_memory("edit_item_id")].setStyleSheet(
                    style_dict['section_tree_btn_selected'].replace("$color", level_color[level]))
        self.tree_section.expandAll()

    def tree_section_clicked(self,e):

        if e.type() == QEvent.KeyPress:
            return
        else:
            pass

        item = self.tree_section.currentItem()
        id = item.whatsThis(0)
        if not id:
            self.area_sinfor.hide()
            return

        level = item.whatsThis(1)


        set_memory("edit_item_id", id)
        set_memory("edit_item_level", int(level))

        self.btn_node_up.setEnabled(False)
        self.btn_node_down.setEnabled(False)
        self.display_tree_section_node(id)
        self.load_node_data()
        self.load_node_children_data()


    def display_tree_section_node(self, id):

        for key, value in self.section_btn_dict.items():
            row_level = int(value.whatsThis())
            if key == id:  # 선택한 node의 를 tree에 표시해줌
                value.setStyleSheet(style_dict['section_tree_btn_selected'].replace("$color", level_color[row_level]))
            else:
                value.setStyleSheet(style_dict['section_tree_btn'].replace("$color", level_color[row_level]))

    def load_node_data_all(self, id, level):

        set_memory("edit_item_id", id)
        set_memory("edit_item_level", level)
        self.btn_node_up.setEnabled(False)
        self.btn_node_down.setEnabled(False)
        self.display_tree_section_node(id)
        self.load_node_data()
        self.load_node_children_data()

    def load_node_data(self):
        edit_item_id = get_memory("edit_item_id")
        node_data = False

        if edit_item_id:
            db = Database()
            node_data = db.get_node_data(edit_item_id)

        if node_data:

            self.area_sinfor.show()
            self.input_node_name.setText(node_data['name'])
            if node_data['level'] != 4:
                self.area_sh2.setEnabled(True)
                self.area_sh3.setEnabled(True)
                self.input_node_name.setStyleSheet(
                    "color:" + "black" + ";background-color:white;border:1 solid gray")
                self.input_node_add.setStyleSheet(
                    "color:" + "black" + ";background-color:white;border:1 solid gray")
            else:
                self.input_node_name.setStyleSheet(
                    "color:" + "black" + ";background-color:white;border:1 solid gray")
                self.area_sh2.setEnabled(False)
                self.area_sh3.setEnabled(False)

            set_memory("edit_item_name", node_data['name'])

            if node_data['level'] == 0:
                self.btn_node_delete.hide()
            else:
                self.btn_node_delete.show()

            if node_data['level'] == 3:
                self.input_node_add.setEnabled(False)
                self.btn_node_add.setEnabled(False)
            else:
                self.input_node_add.setEnabled(True)
                self.btn_node_add.setEnabled(True)
            return

        self.area_sinfor.hide()

    def load_node_children_data(self):

        edit_item_id = get_memory("edit_item_id")
        node_children_data = False
        self.list_node_children.clear()
        if edit_item_id:
            db = Database()
            node_children_data = db.get_node_children_data(edit_item_id)

        if node_children_data:
            for row in node_children_data:
                data = QListWidgetItem()
                data.setText(row['name'])
                data.setWhatsThis(str(row))
                data.setForeground(QBrush(QtGui.QColor("black")))
                self.list_node_children: QListWidget
                self.list_node_children.addItem(data)

            return


    def node_up_down(self, num):

        self.list_node_children: QListWidget
        parent = self.list_node_children
        index = parent.currentRow()
        length = parent.count()

        src_dict = eval(parent.item(index).whatsThis())
        dst_dict = eval(parent.item(index + num).whatsThis())

        src_id = src_dict['id']
        src_sequence = src_dict['sibling_sequence']
        dst_id = dst_dict['id']
        dst_sequence = dst_dict['sibling_sequence']

        result = Database().update_node_sequence(src_id, src_sequence, dst_id, dst_sequence)

        if result:
            self.load_node_children_data()
            self.load_tree_section()
            if index + num == 0:
                self.btn_node_up.setEnabled(False)
            else:
                self.btn_node_up.setEnabled(True)
            if index + num + 1 == length:
                self.btn_node_down.setEnabled(False)
            else:
                self.btn_node_down.setEnabled(True)

            if num == 1:
                self.list_node_children.focusNextChild()
            else:
                self.list_node_children.focusPreviousChild()

            self.list_node_children.setCurrentRow(index + num)

        else:
            QMessageBox.warning(self, '경고', "순서 변경 실패", QMessageBox.Yes)

    def edit_node_name(self):

        m_data = get_memory_all()
        if not m_data.get("edit_item_id"):
            QMessageBox.warning(self, '경고', "error node name", QMessageBox.Yes)
            return

        edit_item_id = m_data['edit_item_id']
        orgin_name = m_data['edit_item_name']
        new_name = self.input_node_name.text()
        if orgin_name == new_name:
            QMessageBox.warning(self, '경고', "이름을 변경한 후 눌러주세요", QMessageBox.Yes)
            return
        if len(new_name) == 0:
            QMessageBox.warning(self, '경고', "이름을 입력해주세요", QMessageBox.Yes)
            return
        level = get_memory("edit_item_level")
        db = Database()
        if level != 4:

            result = db.update_node_name(edit_item_id, new_name)

            if result:
                self.display_tree_section_node(edit_item_id)
                self.load_tree_section()
                self.load_node_children_data()
                self.user_devices_data = db.get_user_device_list(self.user_data['id'], self.user_data['role'])
                set_memory("edit_item_name", new_name)
                QMessageBox.information(self, '알림', '수정 성공')
            else:
                QMessageBox.warning(self, '경고', "수정 실패", QMessageBox.Yes)
        else:

            result = db.update_device_name(edit_item_id, new_name)

            if result:
                self.display_tree_section_node(edit_item_id)
                self.load_tree_section()
                self.load_node_children_data()
                self.user_devices_data = db.get_user_device_list(self.user_data['id'], self.user_data['role'])
                set_memory("edit_item_name", new_name)
                QMessageBox.information(self, '알림', '수정 성공')
            else:
                QMessageBox.warning(self, '경고', "수정 실패", QMessageBox.Yes)

    def add_child_node(self):

        m_data = get_memory_all()
        parent_id = m_data.get('edit_item_id')
        parent_level = m_data.get('edit_item_level')
        self.input_node_add: QLineEdit
        name = self.input_node_add.text()
        if len(name) == 0:
            QMessageBox.warning(self, '경고', "이름을 입력해주세요", QMessageBox.Yes)
            return
        if parent_id == None or parent_level == None:
            QMessageBox.warning(self, '경고', "error()", QMessageBox.Yes)
            return

        db = Database()
        result = db.insert_child_node(parent_id, parent_level + 1, name)

        if result:

            user_data = self.user_data
            user_tree_auth = db.get_user_tree_auth_list(user_data['id'], user_data['role'])
            self.user_tree_data = user_tree_auth

            self.load_tree_section()
            self.load_node_children_data()
            QMessageBox.information(self, '알림', '추가 성공')

        else:
            QMessageBox.warning(self, '경고', "추가 실패", QMessageBox.Yes)

        self.input_node_add.setText("")
        self.input_node_add.setFocus()

    def delete_node(self):
        id = get_memory("edit_item_id")
        level = int(get_memory("edit_item_level"))

        if not id:
            QMessageBox.warning(self, '경고', "error id is none", QMessageBox.Yes)
            return

        if level != 4:
            c_text = "지금 삭제"
            text, ok = QInputDialog.getText(self, '경고', " 하위 항목과 같이 삭제 됩니다 \n 삭제 하시려면 '" + c_text + "' 를 입력해주세요")

            if text != c_text or not ok:
                return

        else:
            dev_id = self.tree_section.currentItem().whatsThis(3)
            if not dev_id:
                QMessageBox.warning(self, '경고', "기기를 다시 선택해주세요", QMessageBox.Yes)
                return
            result = QMessageBox.question(self, '물음', '미 배치 기기로 분류됩니다 \n삭제하시겠습니까?', QMessageBox.Yes | QMessageBox.No)
            if result != QMessageBox.Yes:
                return

        db = Database()
        famaily_data = db.select_famaily_data(id, level)
        node_list = []
        for row in famaily_data:
            for key, value in row.items():
                if int(level) <= int(key) and value != None:
                    node_list.append(value)
        node_list = list(set(node_list))
        result = False
        if node_list:
            result = db.delete_nodes(node_list)

        if result:
            delete_memory_all()
            self.load_tree_section()
            self.display_tree_section_node(-1)
            self.area_sinfor.hide()
            self.user_devices_data = db.get_user_device_list(self.user_data['id'], self.user_data['role'])

            self.msg_bar_starter()

            if not self.user_devices_data and self.response_thread.isRunning():
                self.count_table.item(1, 1).setText("0")
                self.count_table.item(1, 2).setText("0")
                self.btn_red.setText("0")
                self.btn_orange.setText("0")
                self.btn_yellow.setText("0")
                self.response_thread.stop = True
                self.blink_animation.stop()
                #self.scheduler.stop()
            QMessageBox.information(self, '알림', '삭제 성공')
        else:
            print("실패")
            QMessageBox.warning(self, '경고', "삭제 실패", QMessageBox.Yes)


    def click_node_item(self, x):

        if x.type() == QEvent.KeyPress:
            return
        else:
            pass

        self.btn_node_up.setEnabled(False)
        self.btn_node_down.setEnabled(False)

        item = self.list_node_children.currentItem()

        self.list_node_children: QListWidget
        length = self.list_node_children.count()
        index = self.list_node_children.indexFromItem(item).row()

        if index != 0:
            self.btn_node_up.setEnabled(True)
        if index != length - 1:
            self.btn_node_down.setEnabled(True)

    def click_btn_node_up(self):
        self.list_node_children: QListWidget
        self.list_node_children.setCurrentIndex(0)

    def drop_event(self, e: QDropEvent):

        self.area_sinfor.hide()
        self.display_tree_section_node(-1)
        delete_memory_all()

        dst_item = self.tree_section.itemAt(e.pos())
        current_item = self.tree_section.currentItem()
        current_id = current_item.whatsThis(0)
        current_have_parent = True if current_item.whatsThis(2) == 'y' else False
        parent_id = current_item.parent().whatsThis(0)

        if dst_item != None:
            if isinstance(dst_item, QTreeWidgetItem):
                dst_id = dst_item.whatsThis(0)
                dst_level = dst_item.whatsThis(1)
                if dst_level == '3':
                    if parent_id == dst_id:
                        return

                    db = Database()

                    number_of_children = db.select_number_of_children(dst_id)
                    if number_of_children['count'] >= 12:
                        QMessageBox.warning(self, '경고', "최대 12개까지 배치 가능합니다.", QMessageBox.Yes)
                        return

                    if current_have_parent:
                        result = db.update_device_parent(dst_id, current_id)
                    else:
                        result = db.insert_device_parent(dst_id, current_item.whatsThis(3))

                    if result:

                        user_tree_auth = db.get_user_tree_auth_list(self.user_data['id'], self.user_data['role'])
                        self.user_tree_data = user_tree_auth
                        user_devices = db.get_user_device_list(self.user_data['id'], self.user_data['role'])
                        self.user_devices_data = user_devices

                        self.msg_bar_starter()

                        self.load_tree_section()
                    else:
                        QMessageBox.warning(self, '경고', "수정 실패", QMessageBox.Yes)

    def input_node_add_key_event(self, e, line_edit):
        if e.key() == Qt.Key_Enter or e.key() == Qt.Key_Return:
            self.btn_node_add.click()
        else:
            QLineEdit.keyPressEvent(line_edit, e)
    ###</섹션관리>


    ###<사용자관리>
    def create_user_list_table(self):


        self.list_users : QTableWidget
        self.list_users.setColumnCount(4)
        self.list_users.setRowCount(22)

        self.list_users.setStyleSheet(style_dict['table_style1'])
        self.list_users.verticalHeader().setVisible(False)
        id = QTableWidgetItem('ID')
        id.setFont(QtGui.QFont("돋움", 15))
        name = QTableWidgetItem('사용자명')
        name.setFont(QtGui.QFont("돋움", 15))
        password = QTableWidgetItem('Password')
        password.setFont(QtGui.QFont("돋움", 15))
        date = QTableWidgetItem('등록일자')
        date.setFont(QtGui.QFont("돋움", 15))
        self.list_users.setHorizontalHeaderItem(0, id)
        self.list_users.setHorizontalHeaderItem(1, name)
        self.list_users.setHorizontalHeaderItem(2, password)
        self.list_users.setHorizontalHeaderItem(3, date)

        self.input_user_name.setStyleSheet("color:black;background-color:white;border:1 solid gray")
        self.input_user_pw.setStyleSheet("color:black;background-color:white;border:1 solid gray")

        # self.list_users : QTableWidget
        # asd = QTableWidgetItem()
        # asd.setBackground(QBrush(Qt.black))
        # asd.setForeground(QBrush(Qt.white))
        # asd.setText("IDXXX")
        # col_num = 4
        # ##CBCBCB
        # ##E7E7E7
        # for col in range(col_num):
        #     self.list_users.setItem(0, col,QTableWidgetItem(""))
        #     self.list_users.item(0, col).setTextAlignment(Qt.AlignCenter)
        #     self.list_users.item(0, col).setFlags(Qt.NoItemFlags)
        #     self.list_users.item(0, col).setBackground(QBrush(Qt.black))
        #     self.list_users.item(0, col).setForeground(QBrush(Qt.white))
        # self.list_users.item(0,0).setText("ID")
        # self.list_users.item(0,1).setText("사용자명")
        # self.list_users.item(0,2).setText("Password")
        # self.list_users.item(0,3).setText("등록일자")




    def load_user_list_data(self):

        users_data = Database().get_users_data()

        default_row_count = 22
        if len(users_data) > 22 :
            default_row_count = len(users_data)
            self.list_users.setRowCount(default_row_count)

        default_cell_count = default_row_count *4
        for i in range(default_cell_count):
            row = int(i / 4)
            col = i % 4
            self.list_users.setItem(row, col, QTableWidgetItem(""))
            self.list_users.item(row, col).setTextAlignment(Qt.AlignCenter)
            self.list_users.item(row, col).setFlags(Qt.ItemIsEnabled)
            self.list_users.item(row, col).setForeground(QBrush(Qt.black))
            if row % 2 == 1:
                self.list_users.item(row,col).setBackground(QtGui.QColor("#CBCBCB"))
            else:
                self.list_users.item(row, col).setBackground(QtGui.QColor("#E7E7E7"))

        for row, user in enumerate(users_data):
            self.list_users : QTableWidget
            self.list_users.item(row, 0).setText(user['user_id'])
            self.list_users.item(row, 0).setWhatsThis(str(user))
            self.list_users.item(row, 1).setText(user['name'])
            self.list_users.item(row, 2).setText(user['password'])
            self.list_users.item(row, 3).setText(user['c_date'])

    def click_user(self):
        row = self.list_users.currentIndex().row()

        this = self.list_users.item(row, 0).whatsThis()

        if not this:
            return False


        self.list_users : QTableWidget
        row_count = self.list_users.rowCount()
        for i in range(row_count):
            if i % 2 == 1:
                color = QtGui.QColor("#CBCBCB")
            else:
                color = QtGui.QColor("#E7E7E7")
            self.list_users.item(i, 0).setBackground(color)
            self.list_users.item(i, 1).setBackground(color)
            self.list_users.item(i, 2).setBackground(color)
            self.list_users.item(i, 3).setBackground(color)

        self.list_users.item(row, 0).setBackground(QBrush(QtGui.QColor("#FFFFCC")))
        self.list_users.item(row, 1).setBackground(QBrush(QtGui.QColor("#FFFFCC")))
        self.list_users.item(row, 2).setBackground(QBrush(QtGui.QColor("#FFFFCC")))
        self.list_users.item(row, 3).setBackground(QBrush(QtGui.QColor("#FFFFCC")))

        data = eval(this)
        set_memory("edit_idx", data['id'])
        set_memory("edit_id", data['user_id'])
        set_memory("edit_name", data['name'])
        set_memory("edit_password", data['password'])


        if data['role'] == 'admin':
            self.btn_user_delete.hide()
            self.area_m_auth.hide()
            self.tree_user_auth.hide()
            self.tree_user_auth_title.hide()
            self.area_m_auth_title.hide()
        else:
            self.btn_user_delete.show()
            self.area_m_auth.show()
            self.tree_user_auth.show()
            self.tree_user_auth_title.show()
            self.area_m_auth_title.show()


        self.input_user_id.setText(data['user_id'])
        self.input_user_name.setText(data['name'])
        self.input_user_pw.setText(data['password'])

        self.load_user_auth_tree(data['id'])
        self.load_user_auth_check_box(data['id'])

    def load_user_auth_check_box(self, id):
        data = Database().get_user_menu_auth_list(id)
        if 's' in data:
            self.checkbox_section.setChecked(True)
        else:
            self.checkbox_section.setChecked(False)
        if 'u' in data:
            self.checkbox_user.setChecked(True)
        else:
            self.checkbox_user.setChecked(False)

        if 'c' in data:
            self.checkbox_conf.setChecked(True)
        else:
            self.checkbox_conf.setChecked(False)

        if 'h' in data:
            self.checkbox_his.setChecked(True)
        else:
            self.checkbox_his.setChecked(False)

        if 'r' in data:
            self.checkbox_report.setChecked(True)
        else:
            self.checkbox_report.setChecked(False)

        # if 'a' in data:
        #     self.checkbox_admin_tool.setChecked(True)
        # else:
        #     self.checkbox_admin_tool.setChecked(False)


    def init_user_manage_page(self):
        self.input_user_id.setText("")
        self.input_user_name.setText("")
        self.input_user_pw.setText("")

        self.checkbox_section.setChecked(False)
        self.checkbox_user.setChecked(False)
        self.checkbox_conf.setChecked(False)
        self.checkbox_his.setChecked(False)
        self.checkbox_report.setChecked(False)
        #self.checkbox_admin_tool.setChecked(False)

        self.tree_user_auth.clear()




    def load_user_auth_tree(self,id):

        self.tree_user_auth.clear()
        completed_dict = {}

        db = Database()

        tree_data_all = db.get_all_tree_data()
        user_tree_data = db.get_user_tree_auth_list(id)

        level_0 = QTreeWidgetItem(self.tree_user_auth)
        level_0.setText(0, tree_data_all[0]['name'])
        level_0.setFlags(level_0.flags() | Qt.ItemIsUserCheckable)
        level_0.setWhatsThis(0, '0')
        level_0.setWhatsThis(1, '0')
        completed_dict[tree_data_all[0]['id']] = level_0

        for i, row in enumerate(tree_data_all[1:]):
            data = QTreeWidgetItem()
            data.setForeground(0,QtGui.QBrush(QtGui.QColor(level_color[row['level']])))
            data.setWhatsThis(0,str(row['id']))
            data.setWhatsThis(1,str(row['level']))

            completed_dict[row['id']] = data
            for id in completed_dict.keys():
                if id == row['parent_id']:
                    completed_dict[id].addChild(data)

            if row['level'] != 4:
                cb = QCheckBox()
                cb.mousePressEvent = lambda e, id = row['id'],level = row['level'], this = cb: self.click_user_tree(id, level,this)
                cb.setText(row['name'])
                if row['id'] in user_tree_data:
                    cb.setChecked(True)
                self.tree_user_auth.setItemWidget(data, 0, cb)
            else:
                data.setText(0, row['name'])
                data.setIcon(0,qta.icon("fa5s.camera", color="blue"))

        self.tree_user_auth.expandAll()


    def click_user_tree(self, id, level, cbox):

        db = Database()
        src_id = id
        src_level = level
        user_idx = get_memory("edit_idx")
        if src_level >= 1 and src_level <= 3:
            famaily_data = db.select_famaily_data(src_id, src_level)
            node_list = []
            for row in famaily_data:
                for key, value in row.items():
                    if src_level <= int(key) and value != None:
                        node_list.append(value)

            node_list = list(set(node_list))

            result = False
            if cbox.isChecked():  # 삭제
                result = db.delete_user_tree_data(user_idx, node_list)
            else:
                user_tree_data = db.get_user_tree_auth_list(user_idx)
                auth_list = [x for x in node_list if x not in user_tree_data]
                result = db.insert_user_tree_data(user_idx, auth_list)

            if result:
                self.load_user_auth_tree(user_idx)
            else:
                QMessageBox.warning(self, '경고', "실패", QMessageBox.Yes)


    def update_user_info(self):
        data = get_memory_all()
        idx = data.get('edit_idx')
        if not idx :
            QMessageBox.warning(self, '경고', "유저를 선택해 주세요", QMessageBox.Yes)
            return

        new_name = self.input_user_name.text()
        new_pw = self.input_user_pw.text()

        if new_name == data['edit_name'] and new_pw == data['edit_password']:
            QMessageBox.warning(self, '경고', "변경 사항이 없습니다", QMessageBox.Yes)
            return

        result = Database().update_user_info(new_name, new_pw, data['edit_idx'])

        if result:
            set_memory("edit_name",new_name)
            set_memory("edit_password",new_pw)
            self.load_user_list_data()
        else:
            QMessageBox.warning(self, '경고', "오류 발생", QMessageBox.Yes)

    def update_user_checkbox(self,src_menu,src):


        idx = get_memory("edit_idx")
        if not idx :
            QMessageBox.warning(self, '경고', "유저를 선택해 주세요", QMessageBox.Yes)
            return

        db = Database()
        result = False

        if src.checkState(): #메뉴 권한인서트
            result = db.insert_user_auth_data(idx, src_menu)
        else: #메뉴 권한딜리트
            result = db.delete_user_auth_data(idx, src_menu)

        if result:
            pass
        else:
            QMessageBox.warning(self, '경고', "오류 발생", QMessageBox.Yes)

    def delete_user(self):
        idx = get_memory("edit_idx")
        if not idx :
            QMessageBox.warning(self, '경고', "유저를 선택해 주세요", QMessageBox.Yes)
            return

        c_text = "지금 삭제"
        text, ok = QInputDialog.getText(self, '경고', "유저가 삭제 됩니다 \n삭제 하시려면 '" + c_text + "' 를 입력해주세요")

        if text != c_text or not ok:
            return

        result = Database().delete_user(idx)

        if result:
            set_memory("edit_idx",None)
            self.create_user_list_table()
            self.init_user_manage_page()
            self.load_user_list_data()
            QMessageBox.information(self, '알림', '유저가 삭제 되었습니다')
        else:
            QMessageBox.warning(self, '경고', "삭제 실패", QMessageBox.Yes)

    def add_user(self):

        input = QInputDialog()
        user_email, ok = input.getText(self, '입력', "사용할 유저의 아이디를 입력해주세요")

        if len(user_email) == 0 or not ok:
            return

        user_idx = "user"+''.join(random.choice("0123456789-abcdefghijklmnopqrstuvwxyz") for i in range(10))
        user_name = "사용자명"+"".join(random.choice("0123456789") for i in range(5))
        result = Database().add_user(user_email, user_idx, user_name)

        if result:
            set_memory("edit_idx", None)
            self.create_user_list_table()
            self.init_user_manage_page()
            self.load_user_list_data()
            QMessageBox.information(self, '알림', '유저가 추가 되었습니다')
        else:
            QMessageBox.warning(self, '경고', "실패", QMessageBox.Yes)


    ###</사용자관리>
    ###<configuration>
    def init_conf_page(self):
        self.list_areas : QTableWidget
        self.list_areas.clearContents()
        self.list_areas.setColumnCount(9)
        self.list_areas.setRowCount(6)
        self.list_areas.setAlternatingRowColors(True)
        self.list_areas.setStyleSheet(style_dict["table_style2"])
        self.list_areas.verticalHeader().setVisible(False)

        self.table_all_info.clearContents()
        self.table_all_info.setColumnCount(7)
        self.table_all_info.setRowCount(1)
        self.table_all_info.setStyleSheet(style_dict["table_style3"])
        self.table_all_info.verticalHeader().setVisible(False)


        str_list = ['No','주의 사용','주의 온도','경고 사용','경고 온도','알람 사용','알람 온도','거리(m)','방사율(%)']

        for i,str in enumerate(str_list):
            item = QTableWidgetItem(str)
            item.setFont(QtGui.QFont("돋움", 9))
            self.list_areas.setHorizontalHeaderItem(i, item)


        for i, str in enumerate(str_list[1:]):
            item = QTableWidgetItem(str)
            item.setFont(QtGui.QFont("돋움", 9))
            self.table_all_info.setHorizontalHeaderItem(i, item)



        self.list_areas.setColumnWidth(0,45)
        self.list_areas.setColumnWidth(7, 60)
        self.list_areas.setColumnWidth(8, 60)
        #self.list_areas.setColumnWidth(9, 35)

        col_num = len(str_list)
        cell_num = col_num*6


        self.conf_px.fill(Qt.transparent)
        self.conf_px2.fill(Qt.transparent)
        self.conf_px3.fill(Qt.transparent)
        self.area_areas.setPixmap(QtGui.QPixmap(self.conf_px))
        self.area_drawing_board.setPixmap(QtGui.QPixmap(self.conf_px2))
        self.area_conf_dot.setPixmap(QtGui.QPixmap(self.conf_px3))
        scene = QGraphicsScene()
        self.screen_conf.setScene(scene)

        self.label_cam_fam_info.setText("")

        # for i in range(cell_num):
        #     row = int(i/col_num)
        #     col = i%col_num
        #     self.list_areas.setItem(row, col, QTableWidgetItem(''))
        #     if row % 2 == 1:
        #         self.list_areas.item(row, col).setBackground(QtGui.QColor("#CBCBCB"))
        #     else:
        #         self.list_areas.item(row, col).setBackground(QtGui.QColor("#E7E7E7"))



    def load_tree_conf(self):
        self.tree_conf.clear()
        completed_dict = {}

        node_list = self.user_tree_data

        tree_data_list = Database().get_tree_data(node_list)

        if not tree_data_list:
            return

        level_0 = QTreeWidgetItem(self.tree_conf)
        level_0.setText(0, tree_data_list[0]['name'])
        completed_dict[tree_data_list[0]['id']] = level_0

        self.btn_list = []

        for i, row in enumerate(tree_data_list[1:]):
            data = QTreeWidgetItem()
            data.setText(0, row['name'])
            #data.setForeground(0,QtGui.QBrush(QtGui.QColor(level_color[row['level']])))
            data.setForeground(0, QtGui.QBrush(QtGui.QColor(Qt.black)))
            completed_dict[row['id']] = data
            for id in completed_dict.keys():
                if id == row['parent_id']:
                    completed_dict[id].addChild(data)
            if row['level'] == 4:
                device_name_dict[row['device_id']] = row['name']
                btn = QPushButton("{0}".format(row['name']))
                btn.setFont(QFont('돋움', 11))
                btn.setIcon(qta.icon("fa5s.camera", color="blue"))
                btn.setStyleSheet(style_dict['section_tree_btn'].replace("$color", "blue"))
                btn.setCursor(QCursor(Qt.PointingHandCursor))
                btn.clicked.connect(lambda state, x=row['device_id'], y=row['id'], z= btn, name = row['name']: self.select_camera_conf(x, y, z, name))
                self.btn_list.append(btn)
                self.tree_conf.setItemWidget(data, 0, btn)

        self.tree_conf.expandAll()


    def load_camera_conf_data(self, device_id):

        self.list_areas.clearContents()

        db = Database()
        # screen cfg cmd를 갖고옴
        cmd_empty_flag = False
        cmd_data = db.get_cmd_data(device_id)
        self.wwa_box_list = []  # 경고1경고2알람 체크박스
        self.wwa_temp_list = []  # 경고1경고2알람 온도
        if not cmd_data:
            cmd_data = default_cmd
            cmd_empty_flag = True
            cmd_data['dev_id'] = device_id
        set_memory("edit_cmd", cmd_data)



        # 컬러 셀렉트 태그
        self.cb_cm.setCurrentIndex(cmd_data['colortable'])
        self.cb_fm.setCurrentIndex(cmd_data['flip'])
        self.cb_gm.setCurrentIndex(cmd_data['gainmode'])
        set_memory("edit_flip",cmd_data['flip'])
        self.cb_rm.setCurrentIndex(cmd_data['relativecolor'])


        if cmd_data['relativecolor'] == 0:
            self.label_abs_min.hide()
            self.label_abs_max.hide()
            self.input_abs_min.hide()
            self.input_abs_max.hide()
            self.label_pts1.hide()
            self.label_pts2.hide()
            self.label_pts3.hide()
            self.label_pts4.hide()
        else:
            self.label_abs_min.show()
            self.label_abs_max.show()
            self.input_abs_min.show()
            self.input_abs_max.show()
            self.label_pts1.show()
            self.label_pts2.show()
            self.label_pts3.show()
            self.label_pts4.show()
            self.input_abs_min.setText(str(cmd_data['absolute_min']))
            self.input_abs_max.setText(str(cmd_data['absolute_max']))


        a_list = ['warning1_onoff', 'warning2_onoff', 'alarm_onoff']
        b_list = ['warning1_temp', 'warning2_temp', 'alarm_temp']

        for i in range(3): # ALL AREA wwa 체크박스 3개
            cbox = QCheckBox()
            if cmd_data[a_list[i]] == 1:
                cbox.setChecked(True)
            cbox.setFixedWidth(14)
            cbox.mousePressEvent = lambda e, dev_id=cmd_data['dev_id'], this=cbox,index= i: self.cell_check_box_click2(e, dev_id, this,index)
            cellWidget = QWidget()
            layoutCB = QHBoxLayout(cellWidget)
            layoutCB.addWidget(cbox)
            layoutCB.setAlignment(QtCore.Qt.AlignCenter)
            layoutCB.setContentsMargins(0, 0, 0, 0)
            cellWidget.setLayout(layoutCB)
            cellWidget.setStyleSheet("border:none;background-color:#585858;color:white;")
            self.table_all_info.setCellWidget(0,2*i,cellWidget)

            cline = QLineEdit()
            if cmd_data[a_list[i]] == 1:
                cline.setText(str(cmd_data[b_list[i]]))
            else:
                cline.setText("")
                cline.setEnabled(False)
            cline.focusInEvent = lambda e, this=cline: self.cell_focus_in2(e, this)
            cline.focusOutEvent = lambda e, dev_id=cmd_data['dev_id'], this=cline, index = i: self.cell_focus_out(e,dev_id,this, index)
            cline.keyPressEvent = lambda e, dev_id=cmd_data['dev_id'], this=cline, index = i: self.cell_key_press2(e,dev_id,this, index)
            cline.setAlignment(Qt.AlignCenter)
            cline.setStyleSheet("border:none;background-color:#585858;color:white;")
            self.table_all_info.setCellWidget(0, 2*i+1, cline)


        qline = QLineEdit()
        qline.setMaxLength(4)
        qline.setValidator(QDoubleValidator(1.0, 25.5, 2))
        qline.focusInEvent = lambda e, this=qline: self.cell_focus_in3(e, this, 'distance', 'area_all')
        qline.focusOutEvent = lambda e, dev_id=cmd_data['dev_id'], this=qline, i=0: self.cell_focus_out(e, dev_id,this, i)
        qline.keyPressEvent = lambda e, dev_id=cmd_data['dev_id'], this=qline, i=0,what = "distance", where = 'area_all': self.cell_key_press3(e, dev_id,this, i,what, where)
        qline.setAlignment(Qt.AlignCenter)
        qline.setStyleSheet("border:none;background-color:#585858;color:white;")
        qline.setText(str(cmd_data['tot_distance']))
        self.table_all_info.setCellWidget(0, 6, qline)

        set_memory("edid_area_max", 0)
        self.area_count = 0

        self.conf_px2.fill(Qt.transparent)
        self.area_areas.setPixmap(QtGui.QPixmap(self.conf_px2))
        painter = QtGui.QPainter(self.conf_px2)


        for i in range(6):

            is_valid = False

            if cmd_data['area{}_onoff'.format(str(i + 1))] == 1:
                self.area_count += 1
                is_valid = True

            # area1~6 영상 영역에 사격형을 그림
            if is_valid:

                x1 = int(cmd_data['area{}_x_start'.format(str(i + 1))] * 3.5)
                x2 = int((cmd_data['area{}_x_end'.format(str(i + 1))]) * 3.5)
                y1 = int(cmd_data['area{}_y_start'.format(str(i + 1))] * 3.5)
                y2 = int((cmd_data['area{}_y_end'.format(str(i + 1))]) * 3.5)

                painter.setPen(QPen(QtGui.QColor(self.areas_colors[i]), 6, Qt.SolidLine))
                st = QStaticText("AREA{}".format((str(i + 1))))
                painter.drawStaticText(x1 + 10, y1 + 10, st)
                # painter.drawRect(x1, y1, x2-x1, y2-y1)
                painter.drawLine(x1, y1, x2, y1)
                painter.drawLine(x2, y1, x2, y2)
                painter.drawLine(x2, y2, x1, y2)
                painter.drawLine(x1, y2, x1, y1)
                self.area_areas.setPixmap(QtGui.QPixmap(self.conf_px2))

            # area1~area6 테이블에 세팅
            if i % 2 == 1:
                b_color = "#CBCBCB"
            else:
                b_color = "#E7E7E7"

            for j in range(9):
                if is_valid:
                    if j == 0:
                        numero = QTableWidgetItem(str(i + 1))
                        numero.setTextAlignment(Qt.AlignCenter)
                        self.list_areas.setItem(i, j, numero)
                    elif j == 1 or j == 3 or j == 5:
                        cbox = QCheckBox()
                        cbox.setFixedWidth(14)
                        self.wwa_box_list.append(cbox)
                        cbox.mousePressEvent = lambda e, dev_id=cmd_data['dev_id'], this=cbox,index=len(self.wwa_box_list) - 1: self.cell_check_box_click(e,dev_id,this,index)
                        cellWidget = QWidget()
                        if j == 1 and cmd_data['area{}_warning1_onoff'.format(str(i + 1))] == 1:
                            cbox.setChecked(True)
                        elif j == 3 and cmd_data['area{}_warning2_onoff'.format(str(i + 1))] == 1:
                            cbox.setChecked(True)
                        elif j == 5 and cmd_data['area{}_alarm_onoff'.format(str(i + 1))] == 1:
                            cbox.setChecked(True)
                        layoutCB = QHBoxLayout(cellWidget)
                        layoutCB.addWidget(cbox)
                        layoutCB.setAlignment(QtCore.Qt.AlignCenter)
                        layoutCB.setContentsMargins(0, 0, 0, 0)
                        cellWidget.setLayout(layoutCB)
                        cellWidget.setStyleSheet("border:none;background-color:$bcolor".replace("$bcolor", b_color))
                        self.list_areas.setCellWidget(i, j, cellWidget)
                    elif j == 2 or j == 4 or j == 6:
                        cline = QLineEdit()
                        if j == 2:
                            self.wwa_temp_list.append(cline)
                            cline.setMaxLength(6)
                            cline.setValidator(QDoubleValidator(0.0, 200.0, 2))
                            if cmd_data['area{}_warning1_onoff'.format(str(i + 1))] == 1:
                                cline.setText(str(cmd_data['area{}_warning1_temp'.format(str(i + 1))]))
                            else:
                                cline.setEnabled(False)
                        elif j == 4:
                            self.wwa_temp_list.append(cline)
                            cline.setMaxLength(6)
                            cline.setValidator(QDoubleValidator(0.0, 200.0, 2))
                            if cmd_data['area{}_warning2_onoff'.format(str(i + 1))] == 1:
                                cline.setText(str(cmd_data['area{}_warning2_temp'.format(str(i + 1))]))
                            else:
                                cline.setEnabled(False)
                        elif j == 6:
                            self.wwa_temp_list.append(cline)
                            cline.setMaxLength(6)
                            cline.setValidator(QDoubleValidator(0.0, 200.0, 2))
                            if cmd_data['area{}_alarm_onoff'.format(str(i + 1))] == 1:
                                cline.setText(str(cmd_data['area{}_alarm_temp'.format(str(i + 1))]))
                            else:
                                cline.setEnabled(False)
                        cline.setAlignment(Qt.AlignCenter)
                        cline.setStyleSheet(
                            "border:none;background-color:$bcolor;color:black".replace("$bcolor", b_color))
                        index = len(self.wwa_temp_list) - 1
                        cline.focusInEvent = lambda e, this=cline: self.cell_focus_in(e, this)
                        cline.focusOutEvent = lambda e, dev_id=cmd_data['dev_id'], this=cline, i=index: self.cell_focus_out(e, dev_id, this, i)
                        cline.keyPressEvent = lambda e, dev_id=cmd_data['dev_id'], this=cline, i=index: self.cell_key_press(e, dev_id, this, i)
                        # cellWidget.setStyleSheet("border:none;background-color:$bcolor;color:black;".replace("$bcolor", b_color))
                        self.list_areas.setCellWidget(i, j, cline)
                        # self.list_areas.cellWidget(i, j).setFlags(Qt.ItemIsEnabled)
                        # self.list_areas.item(i, j).setFlags(Qt.ItemIsEnabled)
                    elif j == 7 or j == 8:
                        cline = QLineEdit()
                        cline.setAlignment(Qt.AlignCenter)
                        cline.setStyleSheet("border:none;background-color:$bcolor;color:black".replace("$bcolor", b_color))
                        if j == 7:
                            cline.setMaxLength(4)
                            cline.setValidator(QDoubleValidator(1.0, 25.5, 2))
                            cline.setText(str(cmd_data['area{}_distance'.format(str(i + 1))]))
                            what = 'distance'
                        elif j == 8:
                            cline.setMaxLength(4)
                            cline.setValidator(QDoubleValidator(0.01, 1.00, 2))
                            cline.setText(str(cmd_data['area{}_emission_rate'.format(str(i + 1))]))

                            what = 'emission_rate'

                        cline.focusInEvent = lambda e, this=cline: self.cell_focus_in3(e, this,what,'area16')
                        cline.focusOutEvent = lambda e, dev_id=cmd_data['dev_id'], this=cline, i=index: self.cell_focus_out(e, dev_id, this, i)
                        cline.keyPressEvent = lambda e, dev_id=cmd_data['dev_id'], this=cline, i=index,w= what: self.cell_key_press3(e, dev_id, this, i, w, 'area16')
                        self.list_areas.setCellWidget(i, j, cline)
                else:
                    if j == 0:
                        self.list_areas.setItem(i, j, QTableWidgetItem("미사용"))
                        self.list_areas.item(i, j).setTextAlignment(Qt.AlignCenter)
                    else:
                        self.list_areas.setItem(i, j, QTableWidgetItem(""))

                # if not j1359_flag:
                #     self.list_areas.item(i, j).setTextAlignment(Qt.AlignCenter)
                #     self.list_areas.item(i, j).setFlags(Qt.ItemIsEnabled)


        if cmd_empty_flag:
            self.outer_general_setting.setEnabled(False)
            self.btn_area_add.setEnabled(False)
            self.btn_area_init.setEnabled(False)
            self.area_all_info.setEnabled(False)
            self.cb_n_save.setEnabled(False)
            QMessageBox.warning(self, '경고', "카메라가 작동 중인지 확인해 주시기 바랍니다.", QMessageBox.Yes)

        else:
            self.outer_general_setting.setEnabled(True)
            self.btn_area_add.setEnabled(True)
            self.btn_area_init.setEnabled(True)
            self.area_all_info.setEnabled(True)
            self.cb_n_save.setEnabled(True)


    def select_camera_conf(self, dev_id, id, c_btn, name):

        if self.conf_tasking :
            return

        set_memory("edit_did", dev_id)

        if self.camera_thread_conf.isRunning():
            self.camera_thread_conf.stop = True
        if self.dot_thread_conf.isRunning():
            self.dot_thread_conf.stop = True

        # 트리에서 클릭 한 버튼을 배경색을 바꿔줌
        for btn in self.btn_list:
            btn.setStyleSheet(style_dict['section_tree_btn'].replace("$color", "blue"))
        c_btn.setStyleSheet("Text-align:left;border:none;background-color:#FFFFCC;color:blue;")

        # 트리 정보를 띄움
        db = Database()
        f_data = db.select_famaily_data(id, 4)[0]
        f_ids = [value for key, value in f_data.items()]
        f_names = db.get_family_names(f_ids)
        if len(f_names) >= 5:  # 족보가 5보다 작으면 미완성 트리
            f_text = f_names_to_str(f_names, name)
            self.label_cam_fam_info.setText(f_text)

        #AREA 에 대한 정보를 불러움
        self.load_camera_conf_data(dev_id)
        #네트워크 정보를 불러옴
        self.load_network_conf_data(dev_id)


        # 영상 쓰레드 스타트
        set_conf_play_id(dev_id)
        self.camera_thread_conf.stop = False
        self.camera_thread_conf.start()
        self.dot_thread_conf.stop = False
        self.dot_thread_conf.start()


    def load_network_conf_data(self, dev_id):

        net_data = Database().get_dev_network_data(dev_id)
        self.label_conf_ip.setText(net_data['ip_addr'])
        self.label_conf_sm.setText(net_data['subnet'])
        self.label_conf_gw.setText(net_data['Gateway'])
        self.label_conf_pt.setText(net_data['port_no'])

        if net_data['save_image'] == 1:
            self.cb_n_save.setChecked(True)
        else:
            self.cb_n_save.setChecked(False)



    #area1~6온도 체크 박스 클릭시
    def cell_check_box_click(self,e,dev_id ,checkbox, index):
        if checkbox.isChecked(): # on  -> off
            area_index = int((index + 3) / 3)
            wwa_list = ['warning1', 'warning2', 'alarm']
            wwa = wwa_list[(index + 3) % 3]
            set_str ="area{}_{}_onoff=0,area{}_{}_temp=0".format(str(area_index),wwa,str(area_index),wwa)
            result = Database().update_cmd(set_str,dev_id)
            if result:
                self.wwa_temp_list[index].setEnabled(False)
                self.wwa_temp_list[index].setText("")
                self.wwa_temp_list[index].clearFocus()
                self.label_description2.hide()
                QCheckBox.mousePressEvent(checkbox, e)

            else:
                QMessageBox.warning(self, '에러', "에러 발생", QMessageBox.Yes)
                self.init_conf_page()
            self.conf_tasking = False
        else: # off -> on
            self.wwa_temp_list[index].setEnabled(True)
            self.wwa_temp_list[index].setFocus()
            self.label_description2.show()
            QCheckBox.mousePressEvent(checkbox, e)

    # area1~6온도 라인에딧에 포커스 인
    def cell_focus_in(self, e, line_edit):
        self.label_description2.setText("값을 입력 후 Enter키를 입력해주세요")
        self.label_description2.show()
        QLineEdit.focusInEvent(line_edit, e)

    # area1~6온도 라인에딧에 포커스 아웃
    def cell_focus_out(self, e, dev_id, line_edit, index):
        if self.conf_tasking:
            return
        self.conf_tasking = True
        self.label_description2.hide()
        self.label_description3.hide()
        QLineEdit.focusOutEvent(line_edit, e)
        self.load_camera_conf_data(dev_id)
        self.conf_tasking = False


    # area1~6온도 라인에딧에 키입력(엔터 눌렀을 경우 값을 입력함)
    def cell_key_press(self, e, dev_id,line_edit, index):
        try:
            if e.key()  == Qt.Key_Enter or e.key() == Qt.Key_Return:
                value = line_edit.text()
                if len(value) == 0:
                    QMessageBox.warning(self, '에러', "값을 입력해주세요", QMessageBox.Yes)
                    self.label_description2.hide()
                    return
                if float(value) > 140.0:
                    QMessageBox.warning(self, '에러', "140 보다 작은 값을 입력해주세요", QMessageBox.Yes)
                    self.label_description3.hide()
                    return

                row = int(index / 3)  # row num
                area_index = int((index + 3) / 3)  # area1~area~6
                wwa_list = ['warning1', 'warning2', 'alarm']
                wwa_index = (index + 3) % 3
                wwa = wwa_list[wwa_index]  # 'warning1'or 'warning2' or 'alarm'
                temp_list = []
                for i in range(3):
                    if self.wwa_box_list[row * 3 + i].isChecked():
                        temp_list.append(float(self.wwa_temp_list[row * 3 + i].text()))

                if len(temp_list) >= 2:
                    for i in range(len(temp_list) - 1):
                        if temp_list[i] >= temp_list[i + 1]:
                            QMessageBox.warning(self, '경고', "주의 온도°C < 경고 온도°C < 알람 온도°C 조건을 만족해야 합니다.", QMessageBox.Yes)
                            return

                set_str = "area{}_{}_onoff = 1,area{}_{}_temp ={}".format(str(area_index), wwa, str(area_index), wwa, value)
                result = Database().update_cmd(set_str, dev_id)
                self.label_description2.hide()
                if result:
                    QMessageBox.information(self, '알림', '온도 설정 완료!')
                else:
                    self.init_conf_page()
                    QMessageBox.warning(self, '에러', "에러 발생", QMessageBox.Yes)
            else:
                QLineEdit.keyPressEvent(line_edit, e)
        except:
            QMessageBox.warning(self, '에러', "에러 발생", QMessageBox.Yes)
            self.init_conf_page()


    #ALL_AREA온도 체크 박스 클릭 시
    def cell_check_box_click2(self,e,dev_id ,checkbox, index):
        if checkbox.isChecked(): # on  -> off
            wwa_list = ['warning1', 'warning2', 'alarm']
            wwa = wwa_list[index]
            set_str ="{}_onoff=0,{}_temp=0".format(wwa, wwa)
            result = Database().update_cmd(set_str,dev_id)
            if result:
                self.table_all_info.cellWidget(0,2*index+1).setEnabled(False)
                self.table_all_info.cellWidget(0,2*index+1).setText("")
                self.table_all_info.cellWidget(0,2*index+1).clearFocus()
                self.label_description3.hide()
                QCheckBox.mousePressEvent(checkbox, e)

            else:
                QMessageBox.warning(self, '에러', "에러 발생", QMessageBox.Yes)
                self.init_conf_page()
            self.conf_tasking = False
        else: # off -> on
            self.table_all_info.cellWidget(0,2*index+1).setEnabled(True)
            self.table_all_info.cellWidget(0,2*index+1).setFocus()
            self.label_description3.show()
            QCheckBox.mousePressEvent(checkbox, e)

    # ALL_AREA온도 포커스 인
    def cell_focus_in2(self, e, line_edit):
        self.label_description3.setText("값을 입력 후 Enter키를 입력해주세요")
        self.label_description3.show()
        QLineEdit.focusInEvent(line_edit, e)


    # ALL_AREA온도 키 입력
    def cell_key_press2(self, e, dev_id,line_edit, index):
        if e.key()  == Qt.Key_Enter or e.key() == Qt.Key_Return:
            value = line_edit.text()
            if len(value) == 0:
                QMessageBox.warning(self, '에러', "값을 입력해주세요", QMessageBox.Yes)
                self.label_description3.hide()
                return

            if len(value) == 0:
                QMessageBox.warning(self, '에러', "값을 입력해주세요", QMessageBox.Yes)
                self.label_description3.hide()
                return

            if float(value) > 140.0:
                QMessageBox.warning(self, '에러', "140 보다 작은 값을 입력해주세요", QMessageBox.Yes)
                self.label_description3.hide()
                return

            temp_list = []
            for i in range(3):
                if self.table_all_info.cellWidget(0,2*i).findChild(QCheckBox).isChecked():
                    temp_list.append(float(self.table_all_info.cellWidget(0,2*i+1).text()))

            if len(temp_list) >= 2:
                for i in range(len(temp_list) - 1):
                    if temp_list[i] >= temp_list[i + 1]:
                        QMessageBox.warning(self, '경고', "경고1온도°C <= 경고2온도°C <= 알람온도°C 조건을 만족해야 합니다.", QMessageBox.Yes)
                        self.label_description3.hide()
                        return

            wwa_list = ['warning1', 'warning2', 'alarm']
            wwa = wwa_list[index]  # 'warning1'or 'warning2' or 'alarm'

            set_str = "{}_onoff = 1,{}_temp ={}".format(wwa, wwa, value)
            result = Database().update_cmd(set_str, dev_id)
            self.label_description3.hide()
            if result:
                QMessageBox.information(self, '알림', '온도 설정 완료!')
            else:
                self.init_conf_page()
                QMessageBox.warning(self, '에러', "에러 발생", QMessageBox.Yes)
        else:
            QLineEdit.keyPressEvent(line_edit, e)


    # area1~6,ALL_AREA 거리 또는 방사율
    def cell_focus_in3(self, e, line_edit, what, where):

        if where == 'area16':
            self.label_description2.show()
        elif where == 'area_all':
            self.label_description3.show()

        QLineEdit.focusInEvent(line_edit, e)


    # area1~6,ALL_AREA 거리 또는 방사율
    def cell_key_press3(self, e, dev_id,line_edit, index, what, where):
        if e.key()  == Qt.Key_Enter or e.key() == Qt.Key_Return:
            value = line_edit.text()
            if len(value) == 0:
                QMessageBox.warning(self, '경고', "값을 입력해주세요", QMessageBox.Yes)
                self.label_description2.hide()
                self.label_description3.hide()
                return

            value_float = float(value)
            if what == 'distance':
                if not (value_float >= 1.0 and value_float <= 25.5):
                    QMessageBox.warning(self, '경고', "거리 범위는 1.0 ~ 25.5 입니다", QMessageBox.Yes)
                    return
            elif what == 'emission_rate':
                if not (value_float >= 0.01 and value_float <= 1.00):
                    QMessageBox.warning(self, '경고', "방사율 범위는 0.01 ~ 1.00 입니다", QMessageBox.Yes)
                    return
            else:
                QMessageBox.warning(self, '경고', "에러 발생", QMessageBox.Yes)
                self.label_description2.hide()
                self.label_description3.hide()
                return



            result = False

            if where == 'area16':
                area_name = "area"+str(int(index/3) +1)

            elif where =='area_all':
                area_name = "tot"

            set_str = "{}_{} = {}".format(str(area_name), what, value)

            result = Database().update_cmd(set_str,dev_id)

            line_edit.clearFocus()
            self.label_description2.hide()
            self.label_description3.hide()

            if result:
                QMessageBox.information(self, '알림', '설정 완료!')
            else:
                self.load_camera_conf_data(dev_id)
        else:
            QLineEdit.keyPressEvent(line_edit, e)



    def edit_area_info(self, row_num,data):
        self.list_areas : QTableWidget
        a0 = self.list_areas.item(0,0)
        a1 = self.list_areas.cellWidget(0,1)
        a2 = self.list_areas.item(0,2)
        a3 = self.list_areas.cellWidget(0,3)
        a4 = self.list_areas.item(0,4)
        a5 = self.list_areas.cellWidget(0,5)


    @pyqtSlot(list)
    def image_data_receive_conf(self, img_data_list):
        if img_data_list:
            img = parser2(img_data_list[0]['image_latest'])
            h, w, c = img.shape
            qImg = QtGui.QImage(img.data, w, h, w * c, QtGui.QImage.Format_RGB888)
            scene = QGraphicsScene()
            tempmap = QtGui.QPixmap.fromImage(qImg)
            scene.addPixmap(tempmap.scaled(560, 420, QtCore.Qt.IgnoreAspectRatio))
        else:
            scene = QGraphicsScene()
        self.screen_conf.setScene(scene)

    @pyqtSlot(list)
    def dot_data_receive_conf(self, dot_data_list):

        self.conf_px3.fill(Qt.transparent)
        if dot_data_list:
            painter = QtGui.QPainter(self.conf_px3)

            data = dot_data_list[0]

            for k in range(1, 7):

                if data['area{}_onoff'.format(str(k))] == 1:
                    painter.setPen(QPen(QtGui.QColor(self.areas_colors[k - 1]), 3, Qt.SolidLine))
                    # st = QStaticText("AREA{}:{}°".format(str(k), str(data['area{}_top'.format(str(k))])))
                    st = QStaticText("{}°C".format(str(data['area{}_top'.format(str(k))])))
                    painter.drawStaticText(int(data['area{}_x_start'.format(str(k))] * 3.5) + 10,
                                           int(data['area{}_y_start'.format(str(k))] * 3.5) + 25, st)

            for j in range(3, 0, -1):
                painter.setPen(QPen(QtGui.QColor(spot_color[j - 1]), 10, Qt.SolidLine))

                n_width = 560
                n_height = 420


                rx = int(data['spot{}_x'.format(str(j))] * (n_width / 160))
                ry = int(data['spot{}_y'.format(str(j))] * (n_height / 120))

                rev_x_value = 5 if data['spot{}_x'.format(str(j))] < 80 else -int(n_width / 15)
                rev_y_value = 5 if data['spot{}_y'.format(str(j))] < 60 else -int(n_height / 20)

                st = QStaticText("s{}:{}°C".format(str(j), str(data['spot{}_temp'.format(str(j))])))
                painter.drawPoint(rx, ry)
                painter.drawStaticText(rx + rev_x_value, ry + rev_y_value, st)


        self.area_conf_dot.setPixmap(QtGui.QPixmap(self.conf_px3))

    def area_add(self):
        if self.conf_tasking :
            return

        d_id = get_memory("edit_did")

        if not d_id:
            QMessageBox.warning(self, '경고', "먼저 기기를 선택해주세요", QMessageBox.Yes)
            return

        if self.area_count >= 6:
            QMessageBox.warning(self, '경고', "AREA 설정은 최대 6개 까지 가능 합니다.", QMessageBox.Yes)
            return

        self.conf_mode = 1
        self.conf_description()
        self.conf_px.fill(Qt.transparent)


    def conf_board_click_event(self,e):


        if self.conf_mode == 1:
            x1,y1 = e.x(), e.y()
            self.conf_mouse_x = [x1,0]
            self.conf_mouse_y = [y1,0]
            painter = QtGui.QPainter(self.conf_px)
            painter.setPen(QPen(Qt.white, 1, Qt.SolidLine))
            painter.drawPoint(x1, y1)
            self.area_drawing_board.setPixmap(QtGui.QPixmap(self.conf_px))
            self.area_drawing_board.setMouseTracking(True)
            self.conf_mode = 2
            self.conf_description()

        elif self.conf_mode == 2:
            self.area_drawing_board.setMouseTracking(False)
            self.conf_mouse_x[1] = e.x()
            self.conf_mouse_y[1] = e.y()

            if self.conf_mouse_x[0] > self.conf_mouse_x[1]:
                self.conf_mouse_x.reverse()
            if self.conf_mouse_y[0] > self.conf_mouse_y[1]:
                self.conf_mouse_y.reverse()

            s_x = round(self.conf_mouse_x[0]/3.5)
            s_y = round(self.conf_mouse_y[0]/3.5)
            e_x = round(self.conf_mouse_x[1]/3.5)
            e_y = round(self.conf_mouse_y[1]/3.5)

            if s_x > 159 : s_x = 159
            if s_y > 119 : s_y = 119
            if e_x > 159 : e_x = 159
            if e_y > 119 : e_y = 119


            self.label_description.setText("이대로 지정하시겠습니까?")
            result = QMessageBox.question(self, '물음', '이대로 지정하시겠습니까?', QMessageBox.Yes | QMessageBox.No)

            if result == QMessageBox.Yes:
                dev_id = get_memory("edit_did")
                area_index = self.area_count + 1
                set_str = "area{}_onoff = 1,".format(str(area_index))
                set_str += "area{}_x_start = {},".format(str(area_index), str(s_x))
                set_str += "area{}_x_end = {},".format(str(area_index), str(e_x))
                set_str += "area{}_y_start = {},".format(str(area_index), str(s_y))
                set_str += "area{}_y_end = {},".format(str(area_index), str(e_y))
                set_str += "area{}_distance = {},".format(str(area_index), str(1.0))
                set_str += "area{}_emission_rate = {}".format(str(area_index), str(1.0))

                result = Database().update_cmd(set_str, dev_id)
                if result:
                    self.load_camera_conf_data(dev_id)
                    QMessageBox.information(self, '알림', '영역을 설정했습니다')
                else:
                    QMessageBox.warning(self, '경고', "영역 설정 실패.", QMessageBox.Yes)

            self.tree_conf.setEnabled(True)
            self.outer_general_setting.setEnabled(True)
            self.btn_area_init.setEnabled(True)
            self.conf_px.fill(Qt.transparent)
            self.area_drawing_board.setPixmap(QtGui.QPixmap(self.conf_px))
            self.conf_mode = 0
            self.conf_description()

    def conf_board_move_event(self, e):
        if self.conf_mode == 2:
            x1 = self.conf_mouse_x[0]
            y1 = self.conf_mouse_y[0]
            x2 = e.x()
            y2 = e.y()
            self.conf_px.fill(Qt.transparent)
            painter = QtGui.QPainter(self.conf_px)
            painter.setPen(QPen(Qt.white, 1, Qt.SolidLine))
            painter.drawLine(x1, y1, x2, y1)
            painter.drawLine(x2, y1, x2, y2)
            painter.drawLine(x2, y2, x1, y2)
            painter.drawLine(x1, y2, x1, y1)
            self.area_drawing_board.setPixmap(QtGui.QPixmap(self.conf_px))


    def conf_description(self):
        desc_list =[
            "",
            "시작 지점을 클릭해주세요",
            "끝 지점을 클릭해주세요"
        ]

        if self.conf_mode != 0:
            self.tree_conf.setEnabled(False)
            self.outer_general_setting.setEnabled(False)
            self.btn_area_init.setEnabled(False)
            self.area_area_list.hide()
            self.area_all_info.hide()
            self.area_description.show()
        else:
            self.area_area_list.show()
            self.area_all_info.show()
            self.area_description.hide()

        self.label_description.setText(desc_list[self.conf_mode])

    def area_delete_all(self):
        if self.conf_tasking :
            return

        dev_id = get_memory("edit_did")

        if not dev_id:
            QMessageBox.warning(self, '경고', "먼저 기기를 선택해주세요", QMessageBox.Yes)
            return

        answer = QMessageBox.question(self, '물음', '초기화 하시겠습니까?', QMessageBox.Yes | QMessageBox.No)

        if answer != QMessageBox.Yes:
            return

        a_str = """
            area$index$_onoff = 0,area$index$_x_start = 0,area$index$_x_end = 0,area$index$_y_start = 0,
                area$index$_y_end = 0,area$index$_warning1_onoff=0,area$index$_warning1_temp=0,area$index$_warning2_onoff=0,area$index$_warning2_temp=0,
                area$index$_alarm_onoff = 0,area$index$_alarm_temp = 0,area$index$_distance=1.0,area$index$_emission_rate=1.0
                """
        set_str =  ",".join(a_str.replace("$index$", str(i+1)) for i in range(6))

        result = Database().update_cmd(set_str,dev_id)

        if result:
            self.load_camera_conf_data(dev_id)
            QMessageBox.information(self, '알림', '초기화 하였습니다')
        else:
            QMessageBox.warning(self, '경고', "초기화 실패.", QMessageBox.Yes)


    def change_color(self,color_num):

        d_id = get_memory("edit_did")
        if not d_id:
            QMessageBox.warning(self, '경고', "먼저 기기를 선택해주세요", QMessageBox.Yes)
            return

        result = Database().update_color(d_id,str(color_num))

        if not result:
            QMessageBox.warning(self, '경고', "색상 변경 실패.", QMessageBox.Yes)

    def change_filp(self, flip_num):


        d_did = get_memory("edit_did")
        d_flip = get_memory("edit_flip")


        if not d_did:
            QMessageBox.warning(self, '경고', "먼저 기기를 선택해주세요", QMessageBox.Yes)
            return

        if d_flip == flip_num:
            return

        sum = d_flip + flip_num

        if sum == 3:#모두 반전
            x_m = 159
            y_m = 119
        elif sum % 2 == 1:#상하반전
            x_m = 0
            y_m = 119
        elif sum % 2 == 0:#좌우반전
            x_m = 159
            y_m = 0
        db = Database()
        cmd_info = db.get_cmd_data(d_did)
        set_str = ""
        for i in range(1, 7):
            if cmd_info['area{}_onoff'.format(str(i))] == 1:
                x1_n = abs(x_m - cmd_info['area{}_x_start'.format(str(i))])
                x2_n = abs(x_m - cmd_info['area{}_x_end'.format(str(i))])
                y1_n = abs(y_m - cmd_info['area{}_y_start'.format(str(i))])
                y2_n = abs(y_m - cmd_info['area{}_y_end'.format(str(i))])
                if x1_n > x2_n:
                    x1_n , x2_n = x2_n, x1_n
                if y1_n > y2_n:
                    y1_n , y2_n = y2_n, y1_n
                set_str += "area{}_x_start={},area{}_x_end={},area{}_y_start={},area{}_y_end={},"\
                           .format(str(i), x1_n, str(i), x2_n,str(i), y1_n, str(i), y2_n)

        set_str += "flip = {}".format(str(flip_num))
        result = db.update_cmd(set_str,d_did)
        if result:
            self.load_camera_conf_data(d_did)
            set_memory("edit_flip", flip_num)
        else:
            QMessageBox.warning(self, '경고', "플립 변경 실패.", QMessageBox.Yes)


    def change_gain(self, gain_num):

        d_id = get_memory("edit_did")
        if not d_id:
            QMessageBox.warning(self, '경고', "먼저 기기를 선택해주세요", QMessageBox.Yes)
            return

        result = Database().update_gain(d_id,str(gain_num))

        if not result:
            QMessageBox.warning(self, '경고', "색상 변경 실패.", QMessageBox.Yes)



    def change_range_mode(self,range_mod):

        d_id = get_memory("edit_did")
        if not d_id:
            QMessageBox.warning(self, '경고', "먼저 기기를 선택해주세요", QMessageBox.Yes)
            return

        result = Database().update_range_mod(d_id,str(range_mod))

        if result:
            if range_mod == 0:
                self.label_abs_min.hide()
                self.label_abs_max.hide()
                self.input_abs_min.hide()
                self.input_abs_max.hide()
                self.label_pts1.hide()
                self.label_pts2.hide()
                self.label_pts3.hide()
                self.label_pts4.hide()

            else:
                self.label_abs_min.show()
                self.label_abs_max.show()
                self.input_abs_min.show()
                self.input_abs_max.show()
                self.label_pts1.show()
                self.label_pts2.show()
                self.label_pts3.show()
                self.label_pts4.show()
                self.input_abs_min.setText("0")
                self.input_abs_max.setText("180")

        else:
            QMessageBox.warning(self, '경고', "범위 변경 실패.", QMessageBox.Yes)


    def range_value_key_event(self, e, line_edit):
        if e.key()  == Qt.Key_Enter or e.key() == Qt.Key_Return:
            line_edit.clearFocus()
        else:
            QLineEdit.keyPressEvent(line_edit, e)



    def change_range_value(self, e, line_edit):

        d_id = get_memory("edit_did")
        if not d_id:
            QMessageBox.warning(self, '경고', "먼저 기기를 선택해주세요", QMessageBox.Yes)
            return

        min_value = int(self.input_abs_min.text())
        max_value = int(self.input_abs_max.text())

        if min_value >= max_value:
            QMessageBox.warning(self, '경고', "최소값은 최대값 보다 클 수 없습니다", QMessageBox.Yes)
            return

        result = Database().update_range_value(d_id, min_value, max_value)

        if result:
            QMessageBox.information(self, '알림', '값 변경 성공')
        else:
            QMessageBox.warning(self, '경고', "값 변경 실패.", QMessageBox.Yes)

        QLineEdit.focusOutEvent(line_edit, e)


    def normal_video_save_active(self, src):


        d_id = get_memory("edit_did")

        if not d_id:
            QMessageBox.warning(self, '경고', "먼저 기기를 선택해주세요", QMessageBox.Yes)
            return

        db = Database()

        if src.checkState():  # 메뉴 권한인서트
            result = db.update_whether_save_video(d_id, 1)
        else:  # 메뉴 권한딜리트
            result = db.update_whether_save_video(d_id, 0)

        if result:
            if src.checkState():
                QMessageBox.information(self, '알림', '전체 영상을 저장합니다.')
            else:
                QMessageBox.information(self, '알림', '경고/알람상태의 영상만 저장합니다.')
        else:
            QMessageBox.warning(self, '경고', "오류 발생", QMessageBox.Yes)


    ###</configuration>


    ###<admin tool>
    def init_admin_page(self):

        texts = []
        texts += self.area_admin_net.findChildren(QLineEdit)
        texts += self.area_admin_info.findChildren(QLineEdit)
        texts += self.area_admin_device.findChildren(QLineEdit)
        texts += self.area_admin_param_setting.findChildren(QLineEdit)
        for text in texts:
            text.setText("")


    def load_admin_device(self):

        self.list_device.clear()

        device_list = Database().get_admin_device_list()

        for device in device_list:
            item = QListWidgetItem()
            item_str = str(device['ip_addr'])+" / "+ str(device['mac_addr'])
            if device['name'] == None:
                item_str += '(미 배치)'
                item.setForeground(QBrush(Qt.red))
            item.setText(item_str)
            item.setWhatsThis(str(device))
            self.list_device.addItem(item)


    def device_click(self,item):

        self.camera_thread_admin.stop = True

        global attempt_count
        attempt_count = 0

        while working():
            time.sleep(0.2)
            attempt_count += 1
            if attempt_count > 10:
                QMessageBox.warning(self, '에러', '다른 작업 중 입니다.', QMessageBox.Yes)
                return

        device_info = eval(item.whatsThis())

        set_memory("edit_id",device_info['id'])
        set_memory("edit_dev_id",device_info['dev_id'])
        set_memory("edit_dev_nm", device_info['dev_nm'] if device_info['dev_nm'] else "")
        set_memory("edit_dev_ip", device_info['ip_addr'])
        set_memory("edit_dev_mac", device_info['mac_addr'])
        set_memory("edit_have_parent", True if device_info['name'] else False)

        self.camera_thread_admin.Targethost = device_info['ip_addr']
        self.input_admin_ip.setText(device_info['ip_addr'])
        self.input_admin_mac.setText(device_info['mac_addr'])
        self.label_admin_sm.setText(device_info['subnet'])
        self.label_admin_gw.setText(device_info['Gateway'])
        self.label_admin_pt.setText(device_info['port_no'])
        self.input_dev_id.setText(device_info['dev_id'])
        self.input_dev_name.setText(device_info['dev_nm'])

        data = camera_data(device_info['ip_addr'])
        if data:
            self.area_admin_net.setEnabled(True)
            self.area_admin_info.setEnabled(True)
            self.area_admin_device.setEnabled(True)
            self.area_admin_param_setting.setEnabled(True)

            self.input_firm_ver.setText(str(data[-17:-13].decode()))
            self.input_prod_date.setText(str(data[-27:-19].decode()))

            #print(":6")
            #print(str(int.from_bytes( data[2],"little",signed=False)))
            print("%s " %(data.hex()))
            #print(bytes(data))




            if True:
                self.area_admin_param_fd.setEnabled(True)
            else:
                self.area_admin_param_fd.setEnabled(False)

            param_dict = {}
            R = str(int.from_bytes( data[3:7],"little",signed=False))
            B = str(int.from_bytes( data[7:9],"little",signed=False))
            F = str(int.from_bytes( data[9:11],"little",signed=False))
            O = str(int.from_bytes( data[11:13],"little",signed=False))
            T = str(int.from_bytes( data[13:15],"little",signed=False))
            atm = str(int.from_bytes( data[15:17],"little",signed=False))
            O2 = str(int.from_bytes( data[17:19],"little",signed=False))
            S = str(int.from_bytes( data[19:21],"little",signed=False))
            max_value = str(int.from_bytes(data[21:23], "little", signed=False))
            min_value = str(int.from_bytes(data[23:25], "little", signed=False))
            auto1 = str(int.from_bytes(data[25:27], "little", signed=False))
            auto2 = str(int.from_bytes(data[27:29], "little", signed=False))
            auto3 = str(int.from_bytes(data[29:31], "little", signed=False))
            tbkg = str(int.from_bytes(data[33:37], "little", signed=False))
            param_dict['R'] =R
            param_dict['B'] =B
            param_dict['F'] =F
            param_dict['O'] =O
            param_dict['T'] =T
            param_dict['atm'] =atm
            param_dict['O2'] =O2
            param_dict['S'] =S
            param_dict['max_value'] =max_value
            param_dict['min_value'] =min_value
            param_dict['auto1'] = auto1
            param_dict['auto2'] = auto2
            param_dict['auto3'] = auto3
            param_dict['tbkg'] = tbkg

            self.param_r.setText(R)
            self.param_b.setText(B)
            self.param_f.setText(F)
            self.param_o.setText(O)

            self.param_t.setText(T)
            self.param_atm.setText(atm)
            self.param_o2.setText(O2)
            self.param_s.setText(S)

            self.param_max.setText(max_value)
            self.param_min.setText(min_value)
            self.param_auto1.setText(auto1)
            self.param_auto2.setText(auto2)
            self.param_auto3.setText(auto3)
            self.param_tbkg.setText(tbkg)

            set_memory("edit_param_dict", param_dict)







            self.camera_thread_admin.stop = False
            self.camera_thread_admin.start()
        else:
            self.area_admin_net.setEnabled(False)
            self.area_admin_info.setEnabled(False)
            self.area_admin_device.setEnabled(False)
            self.area_admin_param_setting.setEnabled(False)
            self.area_admin_param_fd.setEnabled(False)
            self.input_firm_ver.setText("")
            self.input_prod_date.setText("")
            texts = []
            texts += self.area_admin_param_setting.findChildren(QLineEdit)
            for text in texts:
                text.setText("")



            QMessageBox.warning(self, '에러', '연결 상태를 확인해주세요', QMessageBox.Yes)
            scene = QGraphicsScene()
            self.screen_admin.setScene(scene)

    @pyqtSlot(bytearray)
    def image_data_receive_admin(self,img_data):
        if img_data:
            img = parser(img_data)
            h, w, c = img.shape
            qImg = QtGui.QImage(img.data, w, h, w * c, QtGui.QImage.Format_RGB888)
            scene = QGraphicsScene()
            tempmap = QtGui.QPixmap.fromImage(qImg)
            scene.addPixmap(tempmap.scaled(560, 420, QtCore.Qt.IgnoreAspectRatio))
        else:
            scene = QGraphicsScene()
        self.screen_admin.setScene(scene)

    def find_device(self):
        try:
            ip_s1 = int(self.input_ip1.text())
            ip_s2 = int(self.input_ip2.text())
            ip_s3 = int(self.input_ip3.text())
            ip_s4 = int(self.input_ip4.text())
            ip_e1 = int(self.input_ip5.text())
            ip_e2 = int(self.input_ip6.text())
            ip_e3 = int(self.input_ip7.text())
            ip_e4 = int(self.input_ip8.text())
            time_out = int(self.input_to.text())
        except Exception as e:
            QMessageBox.question(self, '경고', "IP를 올바르게 입력해주세요", QMessageBox.Yes)
            return

        if ip_s1 > ip_e1 or ip_s2 > ip_e2 or ip_s3 > ip_e3 or ip_s4 > ip_e4 or \
                ip_s1 > 255 or ip_s2 > 255 or ip_s3 > 255 or ip_s4 > 255 or ip_e1 > 255 or ip_e2 > 255 or \
                ip_e3 > 255 or ip_e4 > 255:
            QMessageBox.question(self, '경고', "IP를 올바르게 입력해주세요", QMessageBox.Yes)
            return

        self.setEnabled(False)
        self.camera_thread_admin.stop = True

        fw = find_window(self, ip_s1, ip_s2, ip_s3, ip_s4, ip_e1, ip_e2, ip_e3, ip_e4, time_out)



    def ip_and_mac_btn_toggle(self,e,line_edit):

        if not get_memory("edit_dev_id"):
            return

        QLineEdit.keyPressEvent(line_edit, e)

        ip_value = self.input_admin_ip.text()
        mac_value = self.input_admin_mac.text()
        edit_ip = get_memory("edit_dev_ip")
        edit_mac = get_memory("edit_dev_mac")

        # if len(ip_value) != 0 and len(mac_value) != 0 and ip_value != edit_ip and mac_value != edit_mac \
        #         and check_ip_exp(ip_value) and check_mac_exp(mac_value):
        #     self.ip_and_mac_btn.show()
        # else:
        #     self.ip_and_mac_btn.hide()



    def id_and_name_btn_toggle(self,e,line_edit):

        if not get_memory("edit_dev_id"):
            return

        QLineEdit.keyPressEvent(line_edit, e)

        # id_value = self.input_dev_id.text()
        # name_value = self.input_dev_name.text()
        # edit_did = get_memory("edit_dev_id")
        # edit_name = get_memory("edit_dev_nm")
        #
        # if len(id_value) != 0 and len(name_value) != 0 and (id_value != edit_did or name_value != edit_name):
        #     self.id_and_name_btn.show()
        # else:
        #     self.id_and_name_btn.hide()


    def ip_and_mac_chage(self):


        ip_value = self.input_admin_ip.text()
        mac_value = self.input_admin_mac.text()
        d_id = get_memory("edit_id")
        edit_ip = get_memory("edit_dev_ip")
        edit_mac = get_memory("edit_dev_mac")
        edit_dev_id = get_memory("edit_dev_id")

        if not d_id:
            QMessageBox.warning(self, '경고', "먼저 기기를 선택해주세요", QMessageBox.Yes)
            return

        if  ip_value == edit_ip or mac_value == edit_mac:
            QMessageBox.warning(self, '경고', "IP 와 MAC을 모두 변경해주세요.", QMessageBox.Yes)
            return

        if not(len(ip_value) != 0 and len(mac_value) != 0 and check_ip_exp(ip_value) and check_mac_exp(mac_value)):
            QMessageBox.warning(self, '경고', "IP 와 MAC을 올바르게 입력해주세요.", QMessageBox.Yes)
            return

        device_list = Database().get_admin_device_list()
        for device in device_list:
            if device['ip_addr'] == ip_value or device['mac_addr'] == mac_value:
                QMessageBox.warning(self, '경고', "사용중인 IP주소 또는 MAC주소가 있습니다", QMessageBox.Yes)
                return

        self.camera_thread_admin.stop = True

        answer = QMessageBox.question(self, '물음', '아이피 주소와 MAC 주소를 변경하시겠습니까?',QMessageBox.Yes | QMessageBox.No)

        if answer == QMessageBox.Yes:
            self.area_admin_net.setEnabled(False)
            self.area_admin_info.setEnabled(False)
            self.area_admin_device.setEnabled(False)
            self.area_ip_range.setEnabled(False)
            self.area_admin_param_setting.setEnabled(False)
            self.btn_find.setEnabled(False)
            self.area_admin_description.show()
            self.admin_gif.start()

            ip_value = self.input_admin_ip.text()
            mac_value = self.input_admin_mac.text()
            old_ip_value = get_memory("edit_dev_ip")
            self.change_thread = thread_chage(self, old_ip_value, 50000, ip_value, mac_value)
            self.change_thread.signal.connect(self.ip_mac_change_result)
            self.change_thread.start()

    @pyqtSlot(bool, dict)
    def ip_mac_change_result(self, is_success, result_dict):
        self.area_admin_net.setEnabled(True)
        self.area_admin_info.setEnabled(True)
        self.area_admin_device.setEnabled(True)
        self.area_ip_range.setEnabled(True)
        self.area_admin_param_setting.setEnabled(True)
        self.btn_find.setEnabled(True)
        self.area_admin_description.hide()
        self.admin_gif.stop()
        if is_success:
            result = Database().update_ip_and_mac(get_memory("edit_id"), result_dict['new_ip'], result_dict['new_mac'])
            if result:
                set_memory("edit_dev_ip", result_dict['new_ip'])
                set_memory("edit_dev_mac", result_dict['new_mac'])
                self.load_admin_device()
                delete_memory("edit_id")
                QMessageBox.information(self, '알림', result_dict['msg']) #성공

            else:
                QMessageBox.information(self, '오류', "IP 와 MAC을 변경하였으나 DB에 값을 반영하지 못했습니다")
        else:
            QMessageBox.information(self, '알림', result_dict['msg']) #실패



    def id_and_name_chage(self):
        edit_id = get_memory("edit_id")
        edit_did = self.input_dev_id.text()
        edit_name = self.input_dev_name.text()

        if not edit_id:
            QMessageBox.warning(self, '경고', "먼저 기기를 선택해주세요", QMessageBox.Yes)
            return


        result = Database().update_id_and_name(edit_id, edit_did, edit_name)

        if result:
            set_memory("edit_dev_id",edit_did)
            set_memory("edit_dev_nm",edit_name)
            self.load_admin_device()
            QMessageBox.information(self, '알림', '수정 성공')
        else:
            QMessageBox.warning(self, '경고', "수정 실패.", QMessageBox.Yes)



    def focus_in_ip_mac(self, e, line_edit):
        # self.labael_admin_description.setText("IP 와 MAC 을 모두 변경 후 IP/MAC 수정버튼을 눌러주세요")
        # self.labael_admin_description.show()
        QLineEdit.focusInEvent(line_edit, e)

    def focus_out_ip_mac(self, e, line_edit):
        self.labael_admin_description.hide()
        QLineEdit.focusOutEvent(line_edit, e)

    def delete_device(self):

        d_id = get_memory("edit_id")
        device_id = get_memory("edit_dev_id")
        have_parent = get_memory("edit_have_parent")
        if not d_id:
            QMessageBox.warning(self, '경고', "먼저 기기를 선택해주세요", QMessageBox.Yes)
            return

        self.camera_thread_admin.stop = True

        answer = QMessageBox.question(self, '물음', '정말 제외하시겠습니까?', QMessageBox.Yes | QMessageBox.No)
        if answer != QMessageBox.Yes:
            return

        db = Database()
        result = db.delete_device(d_id)

        if have_parent:
            result2 = db.delete_node_device(device_id)
        else:
            result2 = True

        if result and result2:
            delete_memory("edit_id")
            texts = []
            texts += self.area_admin_net.findChildren(QLineEdit)
            texts += self.area_admin_info.findChildren(QLineEdit)
            texts += self.area_admin_device.findChildren(QLineEdit)
            for text in texts:
                text.setText("")

            self.user_devices_data = db.get_user_device_list(self.user_data['id'], self.user_data['role'])
            self.msg_bar_starter()

            if not self.user_devices_data and self.response_thread.isRunning():
                self.count_table.item(1, 1).setText("0")
                self.count_table.item(1, 2).setText("0")
                self.btn_red.setText("0")
                self.btn_orange.setText("0")
                self.btn_yellow.setText("0")
                self.response_thread.stop = True
                self.blink_animation.stop()
                #self.scheduler.stop()

            self.load_admin_device()
            QMessageBox.information(self, '알림', '제외 하였습니다')
        else:
            QMessageBox.warning(self, '경고', "실패.", QMessageBox.Yes)


    def firm_update(self):

        d_id = get_memory("edit_id")
        ip = get_memory("edit_dev_ip")
        if not d_id:
            QMessageBox.warning(self, '경고', "먼저 기기를 선택해주세요", QMessageBox.Yes)
            return

        self.camera_thread_admin.stop = True

        try:
            fname = QFileDialog.getOpenFileName(self, 'Open file', './')



            if fname[0]:
                extension_name = fname[0][fname[0].rindex(".") + 1:]
                if extension_name != "bin":
                    QMessageBox.warning(self, '경고', "bin 파일만 선택 가능 합니다.", QMessageBox.Yes)
                    return
                f = open(fname[0], 'rb')
                bin_data = None
                with f:
                    bin_data = f.read()

                if type(bin_data) != bytes:
                    QMessageBox.warning(self, '경고', "파일을 확인해주세요", QMessageBox.Yes)
                    return

                answer = QMessageBox.question(self, '물음', 'IP : {} \n파일 : {} \n펌웨어를 업데이트 하시겠습니까?'.format(ip, fname[0]),
                                              QMessageBox.Yes | QMessageBox.No)
                if answer != QMessageBox.Yes:
                    return

                self.area_admin_net.setEnabled(False)
                self.area_admin_info.setEnabled(False)
                self.area_admin_device.setEnabled(False)
                self.area_admin_param_setting.setEnabled(False)
                self.area_ip_range.setEnabled(False)
                self.btn_find.setEnabled(False)
                self.area_admin_description2.show()
                self.admin_gif2.start()

                self.firm_update_thread.ip = ip
                self.firm_update_thread.bin_data = bin_data
                self.firm_update_thread.start()


        except:
            QMessageBox.warning(self, '경고', "에러 발생", QMessageBox.Yes)



    @pyqtSlot(bool, str)
    def firm_update_end_event(self, result, msg):

        self.area_admin_net.setEnabled(True)
        self.area_admin_info.setEnabled(True)
        self.area_admin_device.setEnabled(True)
        self.area_ip_range.setEnabled(True)
        self.area_admin_param_setting.setEnabled(True)
        self.btn_find.setEnabled(True)
        self.area_admin_description2.hide()
        self.admin_gif2.stop()

        if result:
            texts = self.area_admin_device.findChildren(QLineEdit)
            for text in texts:
                text.setText("")
            self.area_admin_device.setEnabled(False)
            delete_memory("edit_id")
            QMessageBox.information(self, '알림', '펌웨어 업데이트 성공\n잠시 후에 카메라에 접속해 주세요')

        else:
            QMessageBox.warning(self, '경고', "펌웨어 업데이트 실패. \n" + msg, QMessageBox.Yes)



    def param_update(self):


        d_id = get_memory("edit_id")
        edit_ip = get_memory("edit_dev_ip")
        edit_mac = get_memory("edit_dev_mac")
        edit_dev_id = get_memory("edit_dev_id")

        if not d_id:
            QMessageBox.warning(self, '경고', "먼저 기기를 선택해주세요", QMessageBox.Yes)
            return

        self.camera_thread_admin.stop = True

        texts = []
        texts += self.area_admin_param_setting.findChildren(QLineEdit)

        try:
            for line_edit in texts:
                if line_edit.whatsThis():
                    if line_edit.whatsThis() == 'param_r' and int(line_edit.text()) > 4294967295:
                        QMessageBox.warning(self, '경고', "R 값을 4294967295 이하로 값을 입력해주세요", QMessageBox.Yes)
                        return
                    if line_edit.whatsThis() == 'param_tbkg' and int(line_edit.text()) > 4294967295:
                        QMessageBox.warning(self, '경고', "TBkg 값을 4294967295 이하로 값을 입력해주세요", QMessageBox.Yes)
                        return
                else:
                    if int(line_edit.text()) > 65535:
                        QMessageBox.warning(self, '경고', "값들을 65535 이하로 입력해주세요\n(R,TBkg 제외)", QMessageBox.Yes)
                        return

        except:
            QMessageBox.warning(self, '에러', "에러 발생", QMessageBox.Yes)

        paramdict = get_memory("edit_param_dict")

        change_values = {}
        is_change = False
        if paramdict['R'] != self.param_r.text():
            is_change = True
            change_values['R'] = "%s -> %s" % (paramdict['R'], self.param_r.text())
        if paramdict['B'] != self.param_b.text():
            is_change = True
            change_values['B'] = "%s -> %s" % (paramdict['B'], self.param_b.text())
        if paramdict['F'] != self.param_f.text():
            is_change = True
            change_values['F'] = "%s -> %s" % (paramdict['F'], self.param_f.text())
        if paramdict['O'] != self.param_o.text():
            is_change = True
            change_values['O'] = "%s -> %s" % (paramdict['O'], self.param_o.text())
        if paramdict['T'] != self.param_t.text():
            is_change = True
            change_values['ε'] = "%s -> %s" % (paramdict['T'], self.param_t.text())
        if paramdict['atm'] != self.param_atm.text():
            is_change = True
            change_values['τatm'] = "%s -> %s" % (paramdict['atm'], self.param_atm.text())
        if paramdict['O2'] != self.param_o2.text():
            is_change = True
            change_values['O(2)'] = "%s -> %s" % (paramdict['O2'], self.param_o2.text())
        if paramdict['S'] != self.param_s.text():
            is_change = True
            change_values['S'] = "%s -> %s" % (paramdict['S'], self.param_s.text())
        if paramdict['max_value'] != self.param_max.text():
            is_change = True
            change_values['IR_Max'] = "%s -> %s" % (paramdict['max_value'], self.param_max.text())
        if paramdict['min_value'] != self.param_min.text():
            is_change = True
            change_values['IR_Min'] = "%s -> %s" % (paramdict['min_value'], self.param_min.text())
        if paramdict['auto1'] != self.param_auto1.text():
            is_change = True
            change_values['AutoCAL1'] = "%s -> %s" % (paramdict['auto1'], self.param_auto1.text())
        if paramdict['auto2'] != self.param_auto2.text():
            is_change = True
            change_values['AutoCAL2'] = "%s -> %s" % (paramdict['auto2'], self.param_auto2.text())
        if paramdict['auto3'] != self.param_auto3.text():
            is_change = True
            change_values['AutoCAL3'] = "%s -> %s" % (paramdict['auto3'], self.param_auto3.text())
        if paramdict['tbkg'] != self.param_tbkg.text():
            is_change = True
            change_values['TBkg'] = "%s -> %s" % (paramdict['tbkg'], self.param_tbkg.text())

        change_text = ""

        if not is_change:
            self.camera_thread_admin.stop = False
            self.camera_thread_admin.start()

            QMessageBox.warning(self, '경고', "변경 사항이 없습니다.", QMessageBox.Yes)
            return

        change_text +="====================================\n"
        for key, value in change_values.items():
            change_text += "%s : %s \n" % (key, value)
        change_text += "====================================\n"

        answer = QMessageBox.question(self, '물음', change_text + '파라미터를 업데이트 하시겠습니까?',
                                      QMessageBox.Yes | QMessageBox.No)
        if answer != QMessageBox.Yes:
            self.camera_thread_admin.stop = False
            self.camera_thread_admin.start()
            return

        self.area_admin_net.setEnabled(False)
        self.area_admin_info.setEnabled(False)
        self.area_admin_device.setEnabled(False)
        self.area_admin_param_setting.setEnabled(False)
        self.area_ip_range.setEnabled(False)
        self.btn_find.setEnabled(False)
        self.area_admin_description3.show()
        self.admin_gif3.start()

        sSendData = "020603"
        R = int(self.param_r.text()).to_bytes(4,byteorder='little').hex()
        B = int(self.param_b.text()).to_bytes(2,byteorder='little').hex()
        F = int(self.param_f.text()).to_bytes(2,byteorder='little').hex()
        O = int(self.param_o.text()).to_bytes(2,byteorder='little').hex()
        T = int(self.param_t.text()).to_bytes(2,byteorder='little').hex()
        atm = int(self.param_atm.text()).to_bytes(2,byteorder='little').hex()
        O2 = int(self.param_o2.text()).to_bytes(2,byteorder='little').hex()
        S = int(self.param_s.text()).to_bytes(2,byteorder='little').hex()
        max_value = int(self.param_max.text()).to_bytes(2,byteorder='little').hex()
        min_value = int(self.param_min.text()).to_bytes(2,byteorder='little').hex()
        auto1 = int(self.param_auto1.text()).to_bytes(2,byteorder='little').hex()
        auto2 = int(self.param_auto2.text()).to_bytes(2,byteorder='little').hex()
        auto3 = int(self.param_auto3.text()).to_bytes(2,byteorder='little').hex()
        tbkg = int(self.param_tbkg.text()).to_bytes(4,byteorder='little').hex()
        sConfig = R+B+F+O+T+atm+O2+S+max_value+min_value+auto1+auto2+auto3+"0000"+tbkg
        sSendData += sConfig
        sRecvPacket = camera_data(edit_ip).hex()
        sSendData += sRecvPacket[74:]

        # print("parameter")
        # print(sSendData.replace(",",'').replace(" ",''))


        self.param_thread.ip = edit_ip
        self.param_thread.send_data = sSendData.replace(",",'').replace(" ",'')
        self.param_thread.nRecvByte = 114
        self.param_thread.start()

    @pyqtSlot()
    def param_end_event(self):
        self.area_admin_net.setEnabled(True)
        self.area_admin_info.setEnabled(True)
        self.area_admin_device.setEnabled(True)
        self.area_ip_range.setEnabled(True)
        self.area_admin_param_setting.setEnabled(True)
        self.btn_find.setEnabled(True)
        self.area_admin_description3.hide()
        self.admin_gif3.stop()
        #delete_memory("edit_id")
        self.camera_thread_admin.stop = False
        self.camera_thread_admin.start()
        QMessageBox.information(self, '알림', '파라미터 업데이트 완료')


    def click_fd_btn(self, btn_name, num):

        d_id = get_memory("edit_id")
        edit_ip = get_memory("edit_dev_ip")
        edit_mac = get_memory("edit_dev_mac")
        edit_dev_id = get_memory("edit_dev_id")

        if not d_id:
            QMessageBox.warning(self, '경고', "먼저 기기를 선택해주세요", QMessageBox.Yes)
            return

        self.camera_thread_admin.stop = True

        if btn_name == "calibration" or btn_name == "initialization" or btn_name == "soft_reset":
            msg_dict = {"calibration": "불꽃 감지기 상태 보정을 ", "initialization":"불꽃 동작상태를 초기화를", "soft_reset":"카메라 재 부팅을" }
            answer = QMessageBox.question(self, '물음',  '%s 하시겠습니까?' %(msg_dict[btn_name]) ,QMessageBox.Yes | QMessageBox.No)
            if answer != QMessageBox.Yes:
                return
        elif btn_name == "hard_reset":
            c_text = "지금 초기화"
            text, ok = QInputDialog.getText(self, '경고', "카메라의 모든 설정이 초기화 됩니다 (IP 포함)\n 초기화 하시려면 '" + c_text + "' 를 입력해주세요")
            if text != c_text or not ok:
                return
        else:
            return

        sSendData = "0206"
        sRecvPacket = camera_data(edit_ip).hex()

        sSendData += sRecvPacket[4:62]
        sSendData += str(num)
        sSendData+=sRecvPacket[66:]


        #print("FD")
        #print(sSendData.replace(",",'').replace(" ",''))

        result = runReceiveOnce(edit_ip, 50000, sSendData.replace(",",'').replace(" ",'') , 114, "parameter2")





    ###</admin tool>

    ###<history>
    def load_tree_his(self):
        self.tree_his.clear()
        completed_dict = {}

        node_list = self.user_tree_data

        tree_data_list = Database().get_tree_data(node_list)

        if not tree_data_list:
            return

        level_0 = QTreeWidgetItem(self.tree_his)
        level_0.setText(0, tree_data_list[0]['name'])
        completed_dict[tree_data_list[0]['id']] = level_0

        self.btn_list = []

        for i, row in enumerate(tree_data_list[1:]):
            data = QTreeWidgetItem()
            data.setText(0, row['name'])
            data.setForeground(0, QtGui.QBrush(QtGui.QColor(Qt.black)))
            completed_dict[row['id']] = data
            for id in completed_dict.keys():
                if id == row['parent_id']:
                    completed_dict[id].addChild(data)
            if row['level'] == 4:
                device_name_dict[row['device_id']] = row['name']
                btn = QPushButton("{0}".format(row['name']))
                btn.setFont(QFont('돋움', 11))
                btn.setIcon(qta.icon("fa5s.camera", color="blue"))
                btn.setStyleSheet(style_dict['section_tree_btn'].replace("$color", "blue"))
                btn.setCursor(QCursor(Qt.PointingHandCursor))
                btn.clicked.connect(lambda state, x=row['device_id'], y=row['id'], z=btn, name= row['name']: self.select_camera_his(x, y, z, name))
                self.btn_list.append(btn)
                self.tree_his.setItemWidget(data, 0, btn)

        self.tree_his.expandAll()


    def select_camera_his(self, dev_id, id, c_btn, name):

        set_memory("edit_did", dev_id)

        # if self.camera_thread_conf.isRunning():
        #     self.camera_thread_conf.stop = True

        # 트리에서 클릭 한 버튼을 배경색을 바꿔줌
        for btn in self.btn_list:
            btn.setStyleSheet(style_dict['section_tree_btn'].replace("$color", "blue"))
        c_btn.setStyleSheet("Text-align:left;border:none;background-color:#FFFFCC;color:blue;")

        # 트리 정보를 띄움
        db = Database()
        f_data = db.select_famaily_data(id, 4)[0]
        f_ids = [value for key, value in f_data.items()]
        f_names = db.get_family_names(f_ids)
        if len(f_names) < 5:  # 족보가 5보다 작으면 미완성 트리
            return
        f_text = f_names_to_str(f_names, name)
        self.label_cam_fam_info2.setText(f_text)


        # 영상 쓰레드 스타트
        # set_conf_play_id(dev_id)
        # self.camera_thread_conf.stop = False
        # self.camera_thread_conf.start()



    def search_his(self):

        dev_id = get_memory("edit_did")

        if not dev_id:
            QMessageBox.warning(self, '경고', "먼저 기기를 선택해주세요", QMessageBox.Yes)
            return

        self.d1 : QDateTimeEdit
        v1 = self.d1.dateTime().toPyDateTime()
        v2 = self.d2.dateTime().toPyDateTime()

        if v1 > v2:
            QMessageBox.warning(self, '경고', "날짜 시간을 올바르게 입력해주세요", QMessageBox.Yes)
            return

        self.video.stop()
        self.video.video_path = None
        self.list_his.clearContents()
        self.list_his.setEnabled(False)
        self.outer_serach.setEnabled(False)
        self.area_file_name.hide()
        self.history_search.dev_id = dev_id
        self.history_search.s_date = v1
        self.history_search.e_date = v2
        self.history_search.start()

        self.btn_his_search.setText("")
        self.his_gif.start()
        self.label_loading.show()


    def play_his(self):

        if self.new_video_path == None:
            return

        self.screen_his_blinder.hide()

        self.video.set_video_path(self.new_video_path)

        if self.video_name:
            self.label_file_name.setText("파일명 : "+self.video_name)
            self.area_file_name.show()
        self.video.play()


    def pause_his(self):
        self.video.pause()

    def stop_his(self):
        self.video.stop()


    def move_pos(self, pos):
        self.video.move(pos)


    def move_pos_btn(self, value):
        self.video.move_by_btn(value)


    @pyqtSlot(int)
    def update_prog_bar(self, duration):
        self.prog_bar.setRange(0, duration)
        self.prog_bar.setSingleStep(int(duration / 100))
        self.prog_bar.setPageStep(int(duration / 100))
        self.prog_bar.setTickInterval(int(duration / 100))
        hours, remainder = divmod(duration, 100000)
        minutes, seconds = divmod(remainder, 60000)
        self.label_fpt.setText('{:02}:{:02}:{:02}'.format(int(hours), int(minutes), int(seconds/1000)))

    @pyqtSlot(str)
    def update_state(self ,msg):
        if msg == 'Playing':
            self.btn_his_play.hide()
            self.btn_his_play_b.hide()
            self.btn_his_pause.show()
            self.btn_his_pause_b.show()
        else:
            self.btn_his_play.show()
            self.btn_his_play_b.show()
            self.btn_his_pause.hide()
            self.btn_his_pause_b.hide()


    @pyqtSlot(int)
    def update_pos(self, pos):
        self.prog_bar.setValue(pos)
        td = datetime.timedelta(milliseconds=pos)
        stime = str(td)
        hours, remainder = divmod(pos, 100000)
        minutes, seconds = divmod(remainder, 60000)
        self.label_pt.setText('{:02}:{:02}:{:02}'.format(int(hours), int(minutes), int(seconds/1000)))

    @pyqtSlot(list)
    def history_list_receive(self, data_list):
        self.list_his.setEnabled(True)
        self.outer_serach.setEnabled(True)
        self.btn_his_search.setText("Search")
        self.his_gif.stop()
        self.label_loading.hide()
        if data_list:
            self.list_his.setRowCount(len(data_list))
            for row, data in enumerate(data_list):
                status_str = ""
                b_color = "gray"
                color = "white"
                if data['alarm_status'] == 'FL':
                    b_color = status_b_color[4]
                    color = status_color[4]
                    status_str = "불꽃감지"
                elif data['alarm_status'] == 'ER':
                    b_color = status_b_color[3]
                    color = status_color[3]
                    status_str = "알람"
                elif data['alarm_status'] == 'WR2':
                    b_color = status_b_color[2]
                    color = status_color[2]
                    status_str = "주의"
                elif data['alarm_status'] == 'WR1':
                    b_color = status_b_color[1]
                    color = status_color[1]
                    status_str = "경고"
                elif data['alarm_status'] == 'OK':
                    b_color = status_b_color[0]
                    color = status_color[0]
                    status_str = "정상"

                btn = QPushButton(status_str)
                btn.setStyleSheet("background-color:{};color:{};border:none;border-radius:5;".format(b_color, color))
                btn.clicked.connect(lambda state, x=row,y= data['video_path']: self.select_his(x, y))
                btn.mouseDoubleClickEvent = lambda e: self.btn_his_play.click()

                self.list_his.setCellWidget(row, 0, btn)
                # self.list_his.item(row, 0).setTextAlignment(Qt.AlignCenter)
                # self.list_his.item(row, 0).setText(status_str)
                self.list_his.setItem(row, 1,QTableWidgetItem(""))
                self.list_his.item(row, 1).setText(data['video_time'])
                self.list_his.item(row, 1).setBackground(QBrush(Qt.white))
                self.list_his.item(row, 1).setWhatsThis(str(data['video_path']))
            self.list_his.scrollToTop()

        else:
            self.list_his.setRowCount(0)
            QMessageBox.information(self, '알림', '검색 된 내역이 없습니다.')

    def click_his(self):
        row = self.list_his.currentIndex().row()
        header = self.list_his.item(row, 1)
        if not header:
            return
        video_path = header.whatsThis()
        if not video_path:
            return

        self.select_his(row, video_path)

    def select_his(self, row, video_path):

        self.list_his : QTableWidget
        for i in range(self.list_his.rowCount()):
            self.list_his.item(i, 1).setForeground(QBrush(Qt.black))
            self.list_his.item(i, 1).setBackground(QBrush(Qt.white))

        self.list_his.item(row, 1).setForeground(QBrush(Qt.black))
        self.list_his.item(row, 1).setBackground(QtGui.QColor("#FFFFCC"))
        video_name = video_path[video_path.rindex("/")+1:]
        self.video_name = video_name
        self.new_video_path = video_path

    ###</history>


    ###<report>
    def load_tree_report(self):
        self.tree_report.clear()
        completed_dict = {}

        node_list = self.user_tree_data

        tree_data_list = Database().get_tree_data(node_list)

        if not tree_data_list:
            return

        level_0 = QTreeWidgetItem(self.tree_report)
        level_0.setText(0, tree_data_list[0]['name'])
        completed_dict[tree_data_list[0]['id']] = level_0

        self.btn_list = []

        for i, row in enumerate(tree_data_list[1:]):
            data = QTreeWidgetItem()
            data.setText(0, row['name'])
            data.setForeground(0, QtGui.QBrush(QtGui.QColor(Qt.black)))
            completed_dict[row['id']] = data
            for id in completed_dict.keys():
                if id == row['parent_id']:
                    completed_dict[id].addChild(data)
            if row['level'] == 4:
                device_name_dict[row['device_id']] = row['name']
                btn = QPushButton("{0}".format(row['name']))
                btn.setFont(QFont('돋움', 11))
                btn.setIcon(qta.icon("fa5s.camera", color="blue"))
                btn.setStyleSheet(style_dict['section_tree_btn'].replace("$color", "blue"))
                btn.setCursor(QCursor(Qt.PointingHandCursor))
                btn.clicked.connect(lambda state, x=row['device_id'], y=row['id'], z=btn, name= row['name']: self.select_camera_report(x, y, z, name))
                self.btn_list.append(btn)
                self.tree_report.setItemWidget(data, 0, btn)

        self.tree_report.expandAll()

    def select_camera_report(self, dev_id, id, c_btn, name):
        set_memory("edit_did", dev_id)

        # if self.camera_thread_conf.isRunning():
        #     self.camera_thread_conf.stop = True

        # 트리에서 클릭 한 버튼을 배경색을 바꿔줌
        for btn in self.btn_list:
            btn.setStyleSheet(style_dict['section_tree_btn'].replace("$color", "blue"))
        c_btn.setStyleSheet("Text-align:left;border:none;background-color:#FFFFCC;color:blue;")
        # 트리 정보를 띄움
        db = Database()
        f_data = db.select_famaily_data(id, 4)[0]
        f_ids = [value for key, value in f_data.items()]
        f_names = db.get_family_names(f_ids)
        if len(f_names) >= 5:  # 족보가 5보다 작으면 미완성 트리
            f_text = f_names_to_str(f_names, name)
            self.label_cam_fam_info3.setText(f_text)



    def init_graph_area(self):

        hb_layout = QHBoxLayout(self.area_graph)
        hb_layout.setSpacing(0)
        hb_layout.setContentsMargins(0, 0, 0, 0)
        q_scroll = QScrollArea(self.area_graph)
        q_scroll.setWidgetResizable(True)
        q_scroll_area = QWidget()
        self.graph_layout = QGridLayout(q_scroll_area)
        self.graph_layout.setSpacing(0)
        self.graph_layout.setContentsMargins(0, 0, 0, 0)
        q_scroll.setWidget(q_scroll_area)
        hb_layout.addWidget(q_scroll)
        self.fig = plt.Figure(figsize=(4, 3), dpi=80)
        self.graph_canvas = FigureCanvas(self.fig)
        self.graph_layout.addWidget(self.graph_canvas)

    def search_rep(self):

        dev_id = get_memory("edit_did")

        if not dev_id:
            QMessageBox.warning(self, '경고', "먼저 기기를 선택해주세요", QMessageBox.Yes)
            return

        self.d1: QDateTimeEdit
        v1 = self.d3.dateTime().toPyDateTime()
        v2 = self.d4.dateTime().toPyDateTime()

        if v1 > v2:
            QMessageBox.warning(self, '경고', "날짜 시간을 올바르게 입력해주세요", QMessageBox.Yes)
            return


        add_query = ""
        if self.cb_report.currentIndex() == 1:
            add_query = "AND alarm_status = 'OK' "
        elif self.cb_report.currentIndex() == 2:
            add_query = "AND alarm_status = 'WR1' "
        elif self.cb_report.currentIndex() == 3:
            add_query = "AND alarm_status = 'WR2' "
        elif self.cb_report.currentIndex() == 4:
            add_query = "AND alarm_status = 'ER' "
        elif self.cb_report.currentIndex() == 5:
            add_query = "AND alarm_status = 'FL' "



        self.list_rep.clearContents()
        self.list_rep.setEnabled(False)
        self.outer_serach2.setEnabled(False)
        self.report_search.dev_id = dev_id
        self.report_search.s_date = v1
        self.report_search.e_date = v2
        self.report_search.add_query = add_query


        self.report_search.start()

        self.btn_rep_search.setText("")
        self.rep_gif.start()
        self.label_loading2.show()



    @pyqtSlot(list)
    def report_list_receive(self, data_list):
        self.list_rep.setEnabled(True)
        self.outer_serach2.setEnabled(True)
        self.btn_rep_search.setText("Search")
        self.rep_gif.stop()
        self.label_loading2.hide()
        self.x_time_list.clear()
        self.fig.clear()
        plt.close(self.fig)
        plt.close("all")
        plt.clf()

        for cursor in self.cursor_list:
            cursor.remove()
        self.cursor_list.clear()

        if data_list:
            self.list_rep.setRowCount(len(data_list))

            spot1_list = []
            spot2_list = []
            spot3_list = []
            area1_list = []
            area2_list = []
            area3_list = []
            area4_list = []
            area5_list = []
            area6_list = []
            time_list = []


            for row, data in enumerate(data_list):

                self.list_rep.setItem(row, 0, QTableWidgetItem(""))
                self.list_rep.item(row, 0).setBackground(QBrush(Qt.white))
                self.list_rep.item(row, 0).setTextAlignment(Qt.AlignCenter)
                self.list_rep.item(row, 0).setText(data['report_time'])

                status_str = ""
                b_color = "gray"
                color = "white"
                if data['alarm_status'] == 'FL':
                    b_color = status_b_color[4]
                    color = status_color[4]
                    status_str = "불꽃감지"
                elif data['alarm_status'] == 'ER':
                    b_color = status_b_color[3]
                    color = status_color[3]
                    status_str = "알람"
                elif data['alarm_status'] == 'WR2':
                    b_color = status_b_color[2]
                    color = status_color[2]
                    status_str = "경고"
                elif data['alarm_status'] == 'WR1':
                    b_color = status_b_color[1]
                    color = status_color[1]
                    status_str = "주의"
                elif data['alarm_status'] == 'OK':
                    b_color = status_b_color[0]
                    color = status_color[0]
                    status_str = "정상"

                btn = QPushButton(status_str)
                btn.setStyleSheet("background-color:{};color:{};border:none;border-radius:5;".format(b_color, color))
                self.list_rep.setCellWidget(row, 1, btn)

                for i in range(9):
                    self.list_rep.setItem(row, i+2,QTableWidgetItem(""))
                    self.list_rep.item(row, i+2).setBackground(QBrush(Qt.white))
                    self.list_rep.item(row, i+2).setTextAlignment(Qt.AlignCenter)

                self.list_rep.item(row, 0).setWhatsThis(str(data['report_time']))

                self.list_rep.item(row, 2).setText(str(data['spot1_temp']))
                self.list_rep.item(row, 3).setText(str(data['spot2_temp']))
                self.list_rep.item(row, 4).setText(str(data['spot3_temp']))
                self.list_rep.item(row, 5).setText(str(data['area1']))
                self.list_rep.item(row, 6).setText(str(data['area2']))
                self.list_rep.item(row, 7).setText(str(data['area3']))
                self.list_rep.item(row, 8).setText(str(data['area4']))
                self.list_rep.item(row, 9).setText(str(data['area5']))
                self.list_rep.item(row, 10).setText(str(data['area6']))

                spot1_list.insert(0,data['spot1_temp'])
                spot2_list.insert(0,data['spot2_temp'])
                spot3_list.insert(0,data['spot3_temp'])
                area1_list.insert(0,data['area1'])
                area2_list.insert(0,data['area2'])
                area3_list.insert(0,data['area3'])
                area4_list.insert(0,data['area4'])
                area5_list.insert(0,data['area5'])
                area6_list.insert(0,data['area6'])

                self.x_time_list.insert(0, str(data['report_time']))
                if row ==0 or row == len(data_list) - 1:
                    time_list.insert(0,str(data['report_time']))
                else:
                    time_list.insert(0, "\n"*row)




            self.fig.set_facecolor("#1B2138")
            self.fig.set_tight_layout(True)
            index = 0

            ax = self.fig.add_subplot(111, facecolor="#262D49")

            s1 = ax.plot(time_list, spot1_list,'-', label="SPOT1", color=spot_color[index])
            cursor = mplcursors.cursor(s1, hover=True)
            cursor.connect('add', lambda e = s1, name = "SPOT1", i = index:self.show_annotation(e, name, i))
            self.cursor_list.append(cursor)
            index += 1

            s2 = ax.plot(time_list, spot2_list,linestyle='-', label="SPOT2", color=spot_color[1])
            cursor = mplcursors.cursor(s2, hover=True)
            cursor.connect('add', lambda e = s2, name = "SPOT2", i = index:self.show_annotation(e, name, i))
            self.cursor_list.append(cursor)
            index += 1

            s3 = ax.plot(time_list, spot3_list,'-', label="SPOT3", color=spot_color[2] )
            cursor = mplcursors.cursor(s3, hover=True)
            cursor.connect('add', lambda e = s3, name = "SPOT3", i = index:self.show_annotation(e, name, i))
            self.cursor_list.append(cursor)
            index += 1


            if sum(area1_list) != 0:
                a1 = ax.plot(time_list, area1_list,'-', label="AREA1", color=self.areas_colors[0])
                cursor = mplcursors.cursor(a1, hover=True)
                cursor.connect('add', lambda e=a1, name="AREA1", i = index: self.show_annotation(e, name, i))
                self.cursor_list.append(cursor)
                index += 1
            if sum(area2_list) != 0:
                a2 = ax.plot(time_list, area2_list, '-', label="AREA2", color=self.areas_colors[1])
                cursor = mplcursors.cursor(a2, hover=True)
                cursor.connect('add', lambda e=a2, name="AREA2", i = index: self.show_annotation(e, name, i))
                self.cursor_list.append(cursor)
                index += 1
            if sum(area3_list) != 0:
                a3 = ax.plot(time_list, area3_list, '-', label="AREA3", color=self.areas_colors[2])
                cursor = mplcursors.cursor(a3, hover=True)
                cursor.connect('add', lambda e=a3, name="AREA3", i = index: self.show_annotation(e, name, i))
                self.cursor_list.append(cursor)
                index += 1

            if sum(area4_list) != 0:
                a4 = ax.plot(time_list, area4_list, '-', label="AREA4", color=self.areas_colors[3])
                cursor = mplcursors.cursor(a4, hover=True)
                cursor.connect('add', lambda e=a4, name="AREA4", i = index: self.show_annotation(e, name, i))
                self.cursor_list.append(cursor)
                index += 1
            if sum(area5_list) != 0:
                a5 = ax.plot(time_list, area5_list, '-', label="AREA5", color=self.areas_colors[4])
                cursor = mplcursors.cursor(a5, hover=True)
                cursor.connect('add', lambda e=a5, name="AREA5", i = index: self.show_annotation(e, name, i))
                self.cursor_list.append(cursor)
                index += 1
            if sum(area6_list) != 0:
                a6 = ax.plot(time_list, area6_list, '-', label="AREA6", color=self.areas_colors[5])
                cursor = mplcursors.cursor(a6, hover=True)
                cursor.connect('add', lambda e=a6, name="AREA6", i = index: self.show_annotation(e, name, i))
                self.cursor_list.append(cursor)


            ax.tick_params(colors='white', which='both')
            ax.grid(color='lightgray', alpha=0.5, linestyle='dashed', linewidth=0.5)



            ax.legend()
            self.graph_canvas.draw()
            self.graph_canvas.parent().show()
            self.list_rep.scrollToTop()

            self.btn_graph_save.show()
            self.btn_data_save.show()
        else:
            self.list_rep.setRowCount(0)
            self.graph_canvas.parent().hide()
            self.btn_graph_save.hide()
            self.btn_data_save.hide()
            QMessageBox.information(self, '알림', '검색 된 내역이 없습니다.')



    def show_annotation(self, sel, name, index):

        #print(type(sel.index))

        for i,cursor in enumerate(self.cursor_list):
            if i != index:
                for s in cursor.selections:
                    cursor.remove_selection(s)
        xi, yi = sel.target
        sel.annotation.set_text("%s\n%s\n%.1f°C" % (name, self.x_time_list[int(sel.index)], yi))



    def save_graph(self):

        dev_id = get_memory("edit_did")

        if not dev_id:
            QMessageBox.warning(self, '경고', "먼저 기기를 선택해주세요", QMessageBox.Yes)
            return
        name = self.user_devices_data[dev_id]['dev_nm']
        fname = QFileDialog.getSaveFileName(self, '이미지 저장', '%s%s' % (name, ".png"), "PNG Files (*.png);")

        if not fname or fname[0] == '':
            return
        try:
            self.fig.savefig(fname[0])
        except:
            pass


    def save_exel_data(self):
        dev_id = get_memory("edit_did")

        if not dev_id:
            QMessageBox.warning(self, '경고', "먼저 기기를 선택해주세요", QMessageBox.Yes)
            return
        name = self.user_devices_data[dev_id]['dev_nm']
        fname = QFileDialog.getSaveFileName(self, '데이터 저장', '%s%s' % (name, ".xls"), "exel Files (*.xls);")

        if not fname or fname[0] == '':
            return

        wbk = xlwt.Workbook()
        self.exel_sheet = wbk.add_sheet("sheet", cell_overwrite_ok=True)
        self.exel_data_add()
        try:
            wbk.save(fname[0])
        except:
            pass

    def exel_data_add(self):

        self.exel_sheet.write(0, 0, '측정시간')
        self.exel_sheet.write(0, 1, '측정결과')
        self.exel_sheet.write(0, 2, 'SPOT1')
        self.exel_sheet.write(0, 3, 'SPOT2')
        self.exel_sheet.write(0, 4, 'SPOT3')
        self.exel_sheet.write(0, 5, 'AREA1')
        self.exel_sheet.write(0, 6, 'AREA2')
        self.exel_sheet.write(0, 7, 'AREA2')
        self.exel_sheet.write(0, 8, 'AREA3')
        self.exel_sheet.write(0, 9, 'AREA4')
        self.exel_sheet.write(0, 10, 'AREA6')

        for row in range(self.list_rep.rowCount()):
            for col in range(self.list_rep.columnCount()):
                try:
                    if col != 1:
                        teext = str(self.list_rep.item(row, col).text())
                    else:
                        teext = str(self.list_rep.cellWidget(row, col).text())
                    self.exel_sheet.write(row+1, col, teext)
                except AttributeError:
                    print()


            ###</report>








# <로그인 창>
logon_form = uic.loadUiType(resource_path('login.ui'))[0]
class login_window(QDialog, logon_form):
    def __init__(self):
        super().__init__()
        if VIDEO_FORDER_PATH == None:
            QMessageBox.warning(self, '경고', "설정 정보를 불러오지 못했습니다", QMessageBox.Yes)
            self.close()
            sys.exit()
        self.setupUi(self)
        self.login_title1.setText("%s" % (login_title1))
        self.login_title2.setText("%s" % (login_title2))
        self.setWindowIcon(QIcon(resource_path('icon.png')))
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.setAttribute(Qt.WA_TranslucentBackground, True)
        self.btn_login.clicked.connect(self.login)
        self.btn_login_cancel.clicked.connect(self.cancle)
        self.input_login_pw.setEchoMode(QLineEdit.Password)

        self.input_login_id.focusInEvent = lambda e, this= self.input_login_id : self.focus_in(e, this)
        self.input_login_pw.focusInEvent = lambda e, this= self.input_login_pw : self.focus_in(e, this)
        self.input_login_id.focusOutEvent = lambda e, this= self.input_login_id: self.focus_out(e, this)
        self.input_login_pw.focusOutEvent = lambda e, this= self.input_login_pw: self.focus_out(e, this)


        self.outer_login.setStyleSheet(style_dict['login'].replace("$url", resource_path("login_background.png").replace("\\","/")))


        shadow = QGraphicsDropShadowEffect()
        shadow.setBlurRadius(50)
        shadow.setXOffset(10)
        shadow.setYOffset(15)
        shadow.setColor(QtGui.QColor("black"))
        self.outer_login2.setGraphicsEffect(shadow)

        # oImage = QImage(resource_path('login_background.png'))
        # sImage = oImage.scaled(QSize(700, 700))  # resize Image to widgets size
        # palette = QPalette()
        # palette.setBrush(QPalette.Window, QBrush(sImage))
        # self.outer_login.setPalette(palette)

        self.btn_login_cancel : QPushButton
        self.btn_login_cancel.setIcon(qta.icon("mdi.close-circle", color="black"))
        self.btn_login_cancel.setIconSize(QtCore.QSize(32, 32))



    def login(self):
        user_id = self.input_login_id.text()
        user_pw = self.input_login_pw.text()
        db = Database()
        user_data = db.login(user_id, user_pw)

        if user_data:
            self.login_process(user_data)
        else:
            QMessageBox.warning(self, '경고', "로그인 실패.", QMessageBox.Yes)

    def login_process(self, user_data):

        db = Database()

        user_menu_auth = db.get_user_menu_auth_list(user_data['id'], user_data['role'])
        user_devices = db.get_user_device_list(user_data['id'], user_data['role'])
        user_tree_auth = db.get_user_tree_auth_list(user_data['id'], user_data['role'])
        user_tree_auth.append(1) #고객이름 id 1 인 트리


        mainWindow.label_title1.setText(top_title1)
        mainWindow.label_title2.setText(top_title2)
        mainWindow.user_data = user_data
        mainWindow.user_devices_data = user_devices
        mainWindow.user_tree_data = user_tree_auth


        mainWindow.menu_box.clear()

        if 's' in user_menu_auth: #menu권한이 있으면
            mainWindow.menu_box.addItem("Section 관리")
        if 'u' in user_menu_auth:  # menu권한이 있으면
            mainWindow.menu_box.addItem("사용자 관리")


        #mainWindow.menu_box.addItem("로그아웃")
        mainWindow.menu_box.addItem("S/W 버전 정보")
        mainWindow.menu_box.addItem("프로그램 종료")

        if not 'c' in user_menu_auth:
            mainWindow.btn_conf.setVisible(False)
        else:
            mainWindow.btn_conf.setVisible(True)

        if not 'h' in user_menu_auth:
            mainWindow.btn_history.setVisible(False)
        else:
            mainWindow.btn_history.setVisible(True)

        if not 'r' in user_menu_auth:
            mainWindow.btn_report.setVisible(False)
        else:
            mainWindow.btn_report.setVisible(True)

        if not 'tool' in user_data['role']:
            mainWindow.btn_admin.setVisible(False)
        else:
            mainWindow.btn_admin.setVisible(True)


        mainWindow.load_tree_dashboard()

        self.input_login_id.setText("")
        self.input_login_pw.setText("")


        self.input_login_id.setFocus()

        self.close()

        if "admin" in user_data['role'] or "user" in user_data['role']:
            mainWindow.delay_executor.stop = False
            mainWindow.delay_executor.start()
        else:
            mainWindow.tabWidget.setCurrentIndex(4)
            mainWindow.btn_dashboard.setVisible(False)
            mainWindow.load_admin_device()
        mainWindow.show()


    def cancle(self):
        self.close()
        sys.exit()
    def keyPressEvent(self, e:QKeyEvent):
        if e.key() == Qt.Key_Return or e.key() == Qt.Key_Enter:
            self.login()

    def focus_in(self, e, line_edit):

        shadow = QGraphicsDropShadowEffect()
        shadow.setBlurRadius(10)
        shadow.setXOffset(0)
        shadow.setYOffset(0)
        shadow.setColor(QtGui.QColor("#62C15B"))
        line_edit.setGraphicsEffect(shadow)
        QLineEdit.focusInEvent(line_edit, e)


    def focus_out(self, e, line_edit):

        line_edit.setGraphicsEffect(None)
        QLineEdit.focusOutEvent(line_edit, e)


app = QApplication(sys.argv)

mainWindow = WindowClass()
loginWindow = login_window()
def pop_login():
    loginWindow.show()


pop_login()


app.exec_()
