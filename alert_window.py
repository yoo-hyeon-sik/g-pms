from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtGui import QBrush, QCursor
from PyQt5.QtWidgets import QDialog, QTableWidget, QTableWidgetItem, QPushButton
from global_object import resource_path
from PyQt5 import uic, QtGui

alert_form = uic.loadUiType(resource_path('alert_window.ui'))[0]
class alert_window(QDialog, alert_form):
    signal = pyqtSignal(int)
    def __init__(self, parent):
        super().__init__()
        self.setupUi(self)
        self.parent = parent
        self.alert_list = []

    def spread_alert_list(self):
        if self.alert_list:
            self.table_alert.clearContents()
            self.table_alert.setRowCount(len(self.alert_list))
            for i, data in enumerate(self.alert_list):
                self.table_alert.setItem(i, 0, QTableWidgetItem(data['parent_name']))
                self.table_alert.item(i, 0).setTextAlignment(Qt.AlignCenter)
                self.table_alert.item(i, 0).setFlags(Qt.ItemIsEnabled)
                self.table_alert.item(i, 0).setForeground(QBrush(QtGui.QColor("green")))

                self.table_alert.setItem(i, 1, QTableWidgetItem(data['name']))
                self.table_alert.item(i, 1).setTextAlignment(Qt.AlignCenter)
                self.table_alert.item(i, 1).setFlags(Qt.ItemIsEnabled)
                self.table_alert.item(i, 1).setForeground(QBrush(QtGui.QColor("blue")))
                btn = QPushButton("보기")
                btn.setCursor(QCursor(Qt.PointingHandCursor))
                btn.setStyleSheet("border:black;background-color:#BDBDBD;color:black;border-radius:5")
                self.table_alert.setCellWidget(i, 2, btn)
                btn.clicked.connect(lambda state, parent_id = int(data['parent_id']):self.signal.emit(parent_id))