from global_object import *
from PyQt5 import uic



find_form = uic.loadUiType(resource_path('find.ui'))[0]
class find_window(QDialog, find_form):
    def __init__(self, parent, ip_s1, ip_s2, ip_s3, ip_s4, ip_e1, ip_e2, ip_e3, ip_e4, time_out):
        try:
            super().__init__()
            #self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
            self.setupUi(self)
            self.parent = parent
            self.ip_s1 = ip_s1
            self.ip_s2 = ip_s2
            self.ip_s3 = ip_s3
            self.ip_s4 = ip_s4
            self.ip_e1 = ip_e1
            self.ip_e2 = ip_e2
            self.ip_e3 = ip_e3
            self.ip_e4 = ip_e4
            self.time_out = time_out


            self.btn_stop.clicked.connect(self.stop_find)
            self.btn_close.clicked.connect(self.close)

            self.find_thread = thread_findDevice(self)
            self.find_thread.signal.connect(self.end_event)


            self.show()
            time.sleep(0.1)
            self.find_thread.start()
        except:
            print("여기서 발생1")


    def stop_find(self):
        self.find_thread.stoper = True

    @pyqtSlot(list)
    def end_event(self, temp_device_list):  # find
        self.find_thread.stoper = True
        if len(temp_device_list) > 0:
            result = QMessageBox.information(self, '알림', '검색 된 기기들을 추가 하시겠습니까?', QMessageBox.Yes | QMessageBox.No)
            if result == QMessageBox.Yes:
                db = Database()
                log_text = ""
                log_time = QDateTime.currentDateTime().toString('yyyy-MM-dd hh:mm:ss')
                is_fail = False
                for device in temp_device_list:
                    ran_num = ''.join(random.choice("0123456789") for i in range(10))
                    d_id = "G-IR-A" + ran_num
                    d_name = "새기기 "+device['ip']
                    result = db.insert_device(device, d_id, d_name)
                    if not result:
                        is_fail = True

                if is_fail:
                    QMessageBox.warning(self, '경고', '도중에 오류가 발생 했습니다', QMessageBox.Yes)
                else:
                    self.parent.load_admin_device()

        else:
            QMessageBox.information(self, '알림', '검색 된 기기가 없습니다.')

    def keyPressEvent(self, event):
        key = event.key()
        if key == QtCore.Qt.Key_Escape:
            self.close()

    def closeEvent(self, event):
        if self.find_thread.stoper == False:
            event.ignore()
            QMessageBox.warning(self, '경고', '중지 후 닫아주세요', QMessageBox.Yes)
            return

        self.parent.setDisabled(False)