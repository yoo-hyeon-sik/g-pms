# -*- mode: python ; coding: utf-8 -*-


block_cipher = None

add_files = [ ('files/main_window.ui', '.'),
              ('files/login.ui', '.'),
              ('files/find.ui', '.'),
              ('files/alert_window.ui', '.'),
              ('files/icon.png', '.'),
              ('files/loading.gif', '.'),
              ('files/pcon.png', '.'),
              ('files/Menu_Btn_Icon_Admin.png', '.'),
              ('files/Menu_Btn_Icon_Configure.png', '.'),
              ('files/Menu_Btn_Icon_Dashboard.png', '.'),
              ('files/Menu_Btn_Icon_History.png', '.'),
              ('files/Menu_Btn_Icon_Menu.png', '.'),
              ('files/Menu_Btn_Icon_Report.png', '.'),
              ('files/login_background.png', '.')
            ]


a = Analysis(['main.py', 'global_object.py','db_module.py', 'find_window.py', 'alert_window.py','Video.py'],
             pathex=[''],
             binaries=[],
             datas=add_files,
             hiddenimports=[],
             hookspath=[],
             hooksconfig={},
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='G-PMS(IR).exe',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=False,
          disable_windowed_traceback=False,
          target_arch=None,
          codesign_identity=None,
          entitlements_file=None , icon='icon.ico')
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='pms_client')
