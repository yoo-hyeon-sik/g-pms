import json
import os
import socket
import subprocess
import sys
import re
import traceback

import numpy as np
from PyQt5 import QtGui, QtCore, uic
from PyQt5.QtCore import QThread, pyqtSignal
from PyQt5.QtWidgets import QGraphicsScene

import base64
from PIL import Image
from io import BytesIO
import datetime
import math
import qtawesome as qta
from PyQt5.QtCore import Qt, QEvent, pyqtSlot, QTimer, QModelIndex, QDate, QSize
from PyQt5.QtGui import QCursor, QFont, QDropEvent, \
    QBrush, QKeyEvent, QPixmap, QPen, QStaticText, QFocusEvent, QDoubleValidator, QIntValidator, QMovie, QIcon
from PyQt5.QtWidgets import *
import random
from PyQt5.QtCore import pyqtSlot, QDateTime
from PyQt5.QtWidgets import QDialog, QMessageBox


# camaera default parameter
# camera parameter:R= 395653 B= 1428 F= 1 O= 412
# emissivity= 100 atmospheric= 100 bkgTemp= 0 atmTemp= 1000
# point1= 70 point2= 3 point3= 0 point4= 0 point5= 0 point6= 0
# ffcPeriod= 25

#######프로그램 시작하면서 설정파일을 불러옴
opt_path = "C:\g-pms-ir\config"
VIDEO_FORDER_PATH = None
server_path = "C:\g-pms-ir\pms_server"
top_title1 = "Monitoring System"
top_title2 = ""
sw_ver = None
sw_date = None

DATABASE_URL = None
DATABASE_PORT = None
DATABASE_DBNAME = None
DATABASE_USER = None
DATABASE_PASSWORD = None

try:
    with open(opt_path + "/config_sensor-pms.json", "r") as json_file:
        opt_data = json.load(json_file)
        sw_ver = opt_data['CLIENT_PROGRAM']['sw_ver']
        sw_date = opt_data['CLIENT_PROGRAM']['sw_date']
        top_title1 = opt_data['CLIENT_PROGRAM']['top_title1']
        top_title2 = opt_data['CLIENT_PROGRAM']['top_title2']
        login_title1 = opt_data['CLIENT_PROGRAM']['login_title1']
        login_title2 = opt_data['CLIENT_PROGRAM']['login_title2']
        DATABASE_URL = opt_data['DEFAULT']['CFI_DATABASE_URL']
        DATABASE_PORT = opt_data['DEFAULT']['CFI_DATABASE_PORT']
        DATABASE_DBNAME = opt_data['DEFAULT']['CFI_DATABASE_DBNAME']
        DATABASE_USER = opt_data['DEFAULT']['CFI_DATABASE_USER']
        DATABASE_PASSWORD = opt_data['DEFAULT']['CFI_DATABASE_PASSWORD']
        VIDEO_FORDER_PATH = opt_data['DEFAULT']['IR_MP4_IMG_PATH']
        MAIN_LOGO1 = opt_data['CLIENT_PROGRAM']['main_logo1']
except:
    pass


from db_module import *
######################################## 변수 ########################################
#background-color:#353944
login_user_data = {}
PACKET_INTERVAL = 0.2
SOCKET_WAIT = 0.5
PACKET_SIZE = 38556
screen_width = 32077
screen_height = 240
screen_ratio = 1
screen_big_mod = False
sReq1data = [0x02, 0x01, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0b, 0x03]
camera_stoper = False
memory = {}
device_name_dict = {}
play_device_list = []
all_device_list = []
play_conf_id = ""
temp_device_list = []
is_working = False
status_b_color = ['#0177BD','#FFE400','orange','#DC3545', '#DC3545'] #blue , yellow, orange, red, red
status_color = ['white','black','black','white', 'white']
level_color = ["black","black", "black", "black","blue"]
spot_color = ["red", "orange", "yellow"]
status_str_list = ["[정상]","[주의]", "[경고]", "[알람]","[불꽃감지]"]
color_list = ["qlineargradient(spread:pad, x1:0 y1:0, x2:0 y2:1, stop:0 #8282DF, stop:1 #2D2DAA )"]
style_dict = {
    "default_outer":
    """
    background-color:"#353944";
    color:'white';
    border:none;
    """
    ,
    "default_outer2":
    """
    border-radius:3;
    background-color:#25253A;
    """
    ,
    "section_btn_normal":
    """
        QPushButton{
            background-color:qlineargradient(spread:pad, x1:0 y1:0, x2:0 y2:1, stop:0 #8282DF, stop:1 #2D2DAA );
            color:white;
            border:none;
        }
        QPushButton:pressed{
            background-color:black;
        }
        QPushButton:disabled{
            background-color:gray;
        }
    """
    ,
    "section_btn_node_delete":
    """
        QPushButton{
            background-color:qlineargradient(spread:pad, x1:0 y1:0, x2:0 y2:1, stop:0 #FF0000, stop:1 #CC3D3D );
            color:black;
        }
        QPushButton:pressed{
            background-color:orange;
        }
        QPushButton:disabled{
            background-color:gray;
        }
    """
    ,
    "section_tree_btn":
    """
        QPushButton:hover:!pressed{
            background-color:#CCE8FF
        }
        QPushButton{
            Text-align:left;
            border:none;
            color:$color;
        }
    """
    ,
    "section_tree_btn_selected":
    """
        QPushButton{
            Text-align:left;
            background-color:#FFFFCC;
            color:$color;
        }
    """
    ,
    "info_area_style":
    """
        border:2 solid white;
        color:$color;
        background-color:$bcolor;
    """
    ,
    "info_area_style2":
        """
            border:1 solid black;
            color:$color;
            background-color:$bcolor;
        """
    ,
    "info_area_style3":
        """
            border:none;
            color:$color;
            background-color:$bcolor;
        """
    ,
    "info_area_style4":
        """
            border:3 solid #47C83E;
            color:$color;
            background-color:$bcolor;
        """
    ,
    "table_style1":
        """
        QHeaderView::section { background-color:black;color:white}
        QTableWidget::item{color:black;}
        """
    ,
    "table_style2":
        """
        QHeaderView::section { background-color:#373737;color:white;border:none;}
        QTableWidget{alternate-background-color: #CBCBCB;background-color: #E7E7E7;}
        QTableWidget::item{color:black;}
        
        """
    ,
    "table_style3":
        """
        QHeaderView::section { background-color:#373737;color:white;border:none;}
        QTableWidget{background-color: #585858;}
        QTableWidget::item{color:white;}
    
        """
    ,
    "btn_area_style":
    """
        QPushButton{
            background-color:$bcolor;
            border:none;
            color:black;
        }
        QPushButton:pressed{
            background-color:black;
            color:white;
        }
    
    """
    ,
    "prog_bar_style":
    """
    QSlider::groove:horizontal {
        border: 1px solid #262626;
        height: 5px;
        background: #393939;
        margin: 0 12px;
    }
    
    QSlider::handle:horizontal {
        background: none;
        border: none;
        width: 23px;
        height: 100px;
        margin: -24px -12px;
        image: url($url);
    }
    """
    ,
    "play_btn":
    """
        QPushButton:hover:!pressed{
            background-color:#CCE8FF
        }
        QPushButton{
            Text-align:left;
            border:none;
            color:green;
        }
    """
    ,
    "cam_btn":
        """
            QPushButton:hover:!pressed{
                background-color:#CCE8FF
            }
            QPushButton{
                Text-align:left;
                border:none;
                color:blue;
            }
        """

    ,
    "new_tree_btn":
    """
    QPushButton{
        padding:7;
        padding-left:10;
        margin:2;
        Text-align:left;
        border:none;
        border-top-left-radius: 3;
        border-bottom-left-radius: 3;
        color:#666666;
        background-color:qlineargradient( x1: 0, y1: 0, x2: 0.05, y2: 0, stop: 0 $bcolor, stop: .5 $bcolor,
         stop: .51 white, stop: 1 white);
    }  
    """
    ,
    "new_tree_btn_selected":
        """
        QPushButton{
            padding:7;
            padding-left:10;
            margin:2;
            Text-align:left;
            border:none;
            border-top-left-radius: 3;
            border-bottom-left-radius: 3;
            color:black;
            background-color:$bcolor;
        }  
        """
    ,
    "new_tree_btn_selected2":
    """
    QPushButton{
        padding:7;
        padding-left:10;
        margin:2;
        Text-align:left;
        border:none;
        border-top-left-radius: 3;
        border-bottom-left-radius: 3;
        color:$color;
        background-color:$bcolor;
    }      
    """
    ,
    "login":
    """
        QWidget#outer_login{
        background-image: url($url);
        border-radius:10;
        }
    """
    ,
    "login2":
    """
        QWidget#outer_login{
        color:#606060;
        }
    """


}
default_cmd = {'relativecolor': 0, 'absolute_min': 0, 'absolute_max': 0, 'flip': 0, 'colortable': 0,
               'gainmode': 0, 'area1_onoff': 0, 'area1_x_start': 0, 'area1_y_start': 0, 'area1_x_end': 0, 'area1_y_end': 0,
               'area1_warning1_onoff': 0, 'area1_warning1_temp': 0.0, 'area1_warning2_onoff': 0, 'area1_warning2_temp': 0.0,
               'area1_alarm_onoff': 0, 'area1_alarm_temp': 0.0, 'area1_distance': 0, 'area1_emission_rate': 0, 'area2_onoff': 0,
               'area2_x_start': 0, 'area2_y_start': 0, 'area2_x_end': 0, 'area2_y_end': 0, 'area2_warning1_onoff': 0,
               'area2_warning1_temp': 0.0, 'area2_warning2_onoff': 0, 'area2_warning2_temp': 0.0, 'area2_alarm_onoff': 0,
               'area2_alarm_temp': 0, 'area2_distance': 0, 'area2_emission_rate': 0, 'area3_onoff': 0, 'area3_x_start': 0,
               'area3_y_start': 0, 'area3_x_end': 0, 'area3_y_end': 0, 'area3_warning1_onoff': 0, 'area3_warning1_temp': 0.0,
               'area3_warning2_onoff': 0, 'area3_warning2_temp': 0.0, 'area3_alarm_onoff': 0, 'area3_alarm_temp': 0.0,
               'area3_distance': 0, 'area3_emission_rate': 0, 'area4_onoff': 0, 'area4_x_start': 0, 'area4_y_start': 0,
               'area4_x_end': 0, 'area4_y_end': 0, 'area4_warning1_onoff': 0, 'area4_warning1_temp': 0.0, 'area4_warning2_onoff': 0,
               'area4_warning2_temp': 0.0, 'area4_alarm_onoff': 0, 'area4_alarm_temp': 0, 'area4_distance': 0,
               'area4_emission_rate': 0, 'area5_onoff': 0, 'area5_x_start': 0, 'area5_y_start': 0, 'area5_x_end': 0,
               'area5_y_end': 0, 'area5_warning1_onoff': 0, 'area5_warning1_temp': 0, 'area5_warning2_onoff': 0,
               'area5_warning2_temp': 0.0, 'area5_alarm_onoff': 0, 'area5_alarm_temp': 0.0, 'area5_distance': 0,
               'area5_emission_rate': 0, 'area6_onoff': 0, 'area6_x_start': 0, 'area6_y_start': 0, 'area6_x_end': 0,
               'area6_y_end': 0, 'area6_warning1_onoff': 0, 'area6_warning1_temp': 0.0, 'area6_warning2_onoff': 0,
               'area6_warning2_temp': 0.0, 'area6_alarm_onoff': 0, 'area6_alarm_temp': 0, 'area6_distance': 0,
               'area6_emission_rate': 0, 'warning1_onoff': 0, 'warning1_temp': 0.0, 'warning2_onoff': 0, 'warning2_temp': 0.0,
               'alarm_onoff': 0, 'alarm_temp': 0.0, 'tot_distance': 0, 'tot_emission_rate': 0}

#######################################################################################




######################################## 함수 ########################################


def receivePacket(sock, nPacketSize):
    bReceiveByte = bytearray(b'')
    nSizeReceived = 0
    while nSizeReceived < nPacketSize:
        data = sock.recv(nPacketSize)
        nSizeReceived += len(data)

        bReceiveByte += data
    if bReceiveByte[0] != 0x02 or bReceiveByte[-1] != 0x03:
        return None
    return bReceiveByte

def parser(packetData):


    bImageData = packetData[8:38408]

    # ------------- parse RGB data ---------------------------------------
    im = np.frombuffer(bImageData, dtype=np.uint16).reshape((120, 160))
    MASK5 = 0b011111
    MASK6 = 0b111111
    # 2 byte RGB565 --> 3byte RGB888
    b_RGB = (im & MASK5) << 3
    g_RGB = ((im >> 5) & MASK6) << 2
    r_RGB = ((im >> (5 + 6)) & MASK5) << 3
    # Compose into one 3-dimensional matrix of 8-bit integers
    #rgbData = np.dstack((b_RGB, g_RGB, r_RGB)).astype(np.uint8)
    rgbData = np.dstack((r_RGB, g_RGB,b_RGB)).astype(np.uint8)

    return rgbData


def parser2(db_data):
    png_value = base64.b64decode(db_data)
    img_before = Image.open(BytesIO(png_value))
    img = np.array(img_before)
    return img



def runReceiveDeviceInfo(Targethost, Targetport, sSendData, nRecvByte, time_out):
    client_socket = None
    bRecvData = None
    global is_working
    is_working = True

    try:
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.settimeout(time_out / 1000)
        client_socket.connect((Targethost, Targetport))
        bArray = bytes.fromhex(sSendData)
        client_socket.sendall(bArray)

        bRecvData = receivePacket(client_socket, nRecvByte)

        if bRecvData == None:
            client_socket.close()
            return None
        client_socket.close()
    except socket.timeout:
        print("...sock timeout:", Targethost, Targetport)
        client_socket.close()
        return None
    except Exception as err:
        return None
    finally:
        client_socket.close()
        is_working = False
        return bRecvData

def runReceiveOnce(Targethost, Targetport, sSendData, nRecvByte, sCommand):

    client_socket = None
    bRecvData = None
    global is_working
    is_working = True
    try:
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.settimeout(1.0)
        client_socket.connect((Targethost, Targetport))

        bArray = bytes.fromhex(sSendData)
        client_socket.sendall(bArray)
        if sCommand != "ip-mac" or sCommand != "parameter":
            bRecvData = receivePacket(client_socket, nRecvByte)
        else:
            time.sleep(1)
        if bRecvData == None:
            client_socket.close()
            return None
        client_socket.close()
    except socket.timeout:
        client_socket.close()
        return
    except Exception as err:
        traceback.print_exc()
        client_socket.close()
        print(err)
    finally:
        client_socket.close()
        is_working = False
        return bRecvData







def set_camera_stoper(flag):
    global camera_stoper
    camera_stoper = flag


def get_play_list_index():
    try:
        return play_device_list.index(None)
    except:
        return None

def set_memory(key,value):
    memory[key] = value


def get_memory(key):
    if memory.get(key) != None:
        return memory.get(key)
    else:
        return False
def delete_memory(key):
    if memory.get(key) != None:
        memory.pop(key)
        return True
    else:
        return False

def get_memory_all():
    return memory

def delete_memory_all():
    global memory
    memory = {}

def set_ratio(input_value):
    global screen_ratio
    screen_ratio = input_value
def get_ratio():
    global screen_ratio
    return screen_ratio


def is_screen_big_mod():
    global screen_big_mod
    return screen_big_mod

def set_big_mod(flag):
    global screen_big_mod
    screen_big_mod = flag

def set_screen_size(width,height):
    global screen_width, screen_height
    screen_width= width
    screen_height = height
def get_screen_size():
    global screen_width, screen_height
    return screen_width, screen_height

def set_conf_play_id(id):
    global play_conf_id
    play_conf_id = id

def set_is_working(flag):
    global is_working
    is_working = flag

def working():
    return is_working


def res_data_to_msg_data(data):

    #flame
    if data['flame_onoff'] == 256:
        return 4, data['spot1_temp']

    #ar area
    elif data['area1_alarm_onoff'] == 1:
        return 3, data['area1_top']
    elif data['area2_alarm_onoff'] == 1:
        return 3, data['area2_top']
    elif data['area3_alarm_onoff'] == 1:
        return 3, data['area3_top']
    elif data['area4_alarm_onoff'] == 1:
        return 3, data['area4_top']
    elif data['area5_alarm_onoff'] == 1:
        return 3, data['area5_top']
    elif data['area6_alarm_onoff'] == 1:
        return 3, data['area6_top']

    #ar global
    elif data['alarm_onoff'] == 1:
        return 3, data["spot1_temp"]


    #warn2 area
    elif data['area1_warning2_onoff'] == 1 :
        return 2, data['area1_top']
    elif data['area2_warning2_onoff'] == 1 :
        return 2, data['area2_top']
    elif data['area3_warning2_onoff'] == 1 :
        return 2, data['area3_top']
    elif data['area4_warning2_onoff'] == 1 :
        return 2, data['area4_top']
    elif data['area5_warning2_onoff'] == 1 :
        return 2, data['area5_top']
    elif data['area6_warning2_onoff'] == 1 :
        return 2, data['area6_top']

    #warn2 global
    elif data['warning2_onoff'] == 1 :
        return 2, data["spot1_temp"]


    #warn1 area
    elif data['area1_warning1_onoff'] == 1 :
        return 1, data['area1_top']
    elif data['area2_warning1_onoff'] == 1 :
        return 1, data['area2_top']
    elif data['area3_warning1_onoff'] == 1 :
        return 1, data['area3_top']
    elif data['area4_warning1_onoff'] == 1 :
        return 1, data['area4_top']
    elif data['area5_warning1_onoff'] == 1 :
        return 1, data['area5_top']
    elif data['area6_warning1_onoff'] == 1 :
        return 1, data['area6_top']

    #warn1 global
    elif data['warning1_onoff'] == 1 :
        return 1, data["spot1_temp"]


    else:
        return 0 ,data['spot1_temp']


def f_names_to_str(f_names, name):
    if len(f_names) == 5:
        f_text = "<font color=black>Level 0 : [ </font>"
        f_text +="<font color=blue>"+f_names[0]+"</font>"
        f_text += "<font color=black> ] Level1 : [ </font>"
        f_text += "<font color=blue>" + f_names[1] + "</font>"
        f_text += "<font color=black> ] Level2 : [ </font>"
        f_text += "<font color=blue>" + f_names[2] + "</font>"
        f_text += "<font color=black> ] Level3 : [ </font>"
        f_text += "<font color=blue>" + f_names[3] + "</font>"
        f_text += "<font color=black> ]</font>"
        f_text += "<font color=black> 카메라 명 : [ </font>"
        f_text += "<font color=blue>" + name + "</font>"
        f_text += "<font color=black> ]</font>"
        return f_text
    else:
        return ""



ip_reg = re.compile('^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$')
def check_ip_exp(ip_str):
    return ip_reg.match(ip_str)


mac_reg = re.compile('^([0-9a-fA-F][0-9a-fA-F]:){5}([0-9a-fA-F][0-9a-fA-F])$')
def check_mac_exp(mac_str):
    return mac_reg.match(mac_str)



def camera_data(ip):
    global is_working
    is_working = True
    try:

        bRecvData = runReceiveOnce(ip, 50000,
                                   "02,04,03,00,0007,03".replace(",", '').replace(" ", ''), 114, "getConfig")
        if bRecvData:
            return bRecvData
        else:
            return False

    except Exception as e:
        return False
    finally:
        is_working = False



# def debugSavepacket(filePath,buffer):
#     dirPath="d:/temp/bootdata/"
#     fw = open(dirPath+filePath, "wb");
#     fw.write(buffer);
#     fw.close()
#     return



#######################################################################################







######################################## 쓰레드 ########################################
#대시보드 현재시각 표시 쓰레드
class thread_time(QThread):
    signal = pyqtSignal()
    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.stop = False
    def run(self):
        while not self.stop:
            time.sleep(1.0)
            self.signal.emit()








#대시보드 카메라 영상 쓰레드
class thread_camera(QThread):
    signal = pyqtSignal(list)
    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.stop = True
        self.width = 320
        self.height = 240

    def run(self):
        db = Database()
        while not self.stop:
            time.sleep(0.2)
            try:
                img_data_list = db.get_image_data(play_device_list)
            except Exception:
                img_data_list = []
            self.signal.emit(img_data_list)
        return


#대시보드 카메라 마우스 좌표 쓰레드
class thread_dot_temp(QThread):
    signal = pyqtSignal(list)
    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.stop = True

    def run(self):
        db = Database()
        while not self.stop:
            try:
                dot_data_list = db.get_mouse_data2(play_device_list)
            except Exception:
                dot_data_list = []

            self.signal.emit(dot_data_list)

            time.sleep(0.5)
        return



#대시보드 상태표시 쓰레드
class thread_response(QThread):
    signal = pyqtSignal(list)
    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.stop = True

    def run(self):
        db = Database()
        while not self.stop:
            try:
                response_data_list = db.get_response_data(all_device_list)
            except Exception:
                response_data_list = []

            msg_data_list = []

            if response_data_list:
                for row in response_data_list:
                    level, temp = res_data_to_msg_data(row)
                    msg_data_list.append({"dev_id": row['dev_id'], "level": level, "temp": temp, "time": row['time']})

            self.signal.emit(msg_data_list)
            time.sleep(3.0)
        return


#configuration에서 보는 카메라(DB값)
class thread_camera2(QThread):
    signal = pyqtSignal(list)
    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.stop = True
        self.db = Database()

    def run(self):

        while not self.stop:
            time.sleep(0.2)
            try:
                img_data_list = self.db.get_image_data([play_conf_id])
            except Exception:
                img_data_list = []
            self.signal.emit(img_data_list)
        return


#configuration에서 보는 spot123 + mouse temperature
class thread_dot_temp_conf(QThread):
    signal = pyqtSignal(list)
    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.stop = True
        self.db = Database()

    def run(self):

        while not self.stop:
            time.sleep(0.2)
            try:
                img_data_list = self.db.get_mouse_data2([play_conf_id])
            except Exception:
                img_data_list = []
            self.signal.emit(img_data_list)
        return



#admin tool에서 보는 카메라(소켓통신)
class thread_camera3(QThread):
    signal = pyqtSignal(bytearray)
    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.stop = True
        self.Targethost = ""

    def run(self):

        Targetport = 50000
        client_socket = None
        global is_working
        is_working = True

        try:
            client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client_socket.settimeout(2.0)
            client_socket.connect((self.Targethost, Targetport))
            while not self.stop:

                time.sleep(0.2)
                client_socket.sendall(bytes(sReq1data))
                bRecvData = receivePacket(client_socket, PACKET_SIZE)
                self.signal.emit(bRecvData)


        except Exception:
            client_socket.close()
        finally:
            client_socket.close()
            is_working = False
            return


#기기 검색 쓰레드
class thread_findDevice(QThread):
    signal = pyqtSignal(list)

    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.stoper = False

    def run(self):
        time.sleep(0.9)

        ip_s1 = int(self.parent.ip_s1)
        ip_s2 = int(self.parent.ip_s2)
        ip_s3 = int(self.parent.ip_s3)
        ip_s4 = int(self.parent.ip_s4)
        ip_e1 = int(self.parent.ip_e1)
        ip_e2 = int(self.parent.ip_e2)
        ip_e3 = int(self.parent.ip_e3)
        ip_e4 = int(self.parent.ip_e4)

        time_out = int(self.parent.time_out)

        global temp_device_list

        device_list = Database().get_admin_device_list()
        devcie_ip_list = list(map(lambda x: x['ip_addr'], device_list))

        ip_list = []
        for i1 in range(ip_s1, ip_e1 + 1):
            for i2 in range(ip_s2, ip_e2 + 1):
                for i3 in range(ip_s3, ip_e3 + 1):
                    for i4 in range(ip_s4, ip_e4 + 1):
                        ip_list.append(str(i1) + "." + str(i2) + "." + str(i3) + "." + str(i4))

        for idx, ip in enumerate(ip_list):
            if self.stoper: break
            self.parent.msg_bar.setText(ip + " 검색중")

            msg = "접속 성공"

            try:
                startupinfo = subprocess.STARTUPINFO()
                startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
                response = subprocess.Popen(
                    ['ping', '-n', '1', '-w', str(time_out), ip],
                    stdout=subprocess.PIPE, shell=True
                )
                response.wait(time_out)
            except subprocess.CalledProcessError:
                response = None
                self.parent.result_list.addItem(str(ip) + " : " + "접속 실패")
                self.parent.result_list.scrollToBottom()
                continue

            if not response.poll():
                bRecvData = runReceiveDeviceInfo(ip, 50000, "02,04,03,00,0007,03".replace(",", '').replace(" ", ''),
                                                 114, time_out)
                if bRecvData is not None:
                    data = {}
                    data['camera_parameter'] = bRecvData[:41].hex()
                    data['tcp_port'] = int.from_bytes(bRecvData[39:41], "little", signed=False)
                    data['mac'] = "%02x:%02x:%02x:%02x:%02x:%02x" % (
                        bRecvData[41], bRecvData[42], bRecvData[43], bRecvData[44], bRecvData[45], bRecvData[46])
                    data['ip'] = "%d.%d.%d.%d" % (bRecvData[47], bRecvData[48], bRecvData[49], bRecvData[50])
                    data['etc'] = bRecvData[51:].hex()
                    if not ip in devcie_ip_list:
                        temp_device_list.append({"ip": data['ip'], "mac": data['mac']})

                else:
                    msg = "접속 실패"

            else:
                msg = "접속 실패"

            if ip in devcie_ip_list:
                msg += " (등록된 기기)"


            self.parent.result_list.addItem(str(ip) + " : " + msg)
            time.sleep(0.05)
            self.parent.result_list.scrollToBottom()

        self.parent.result_list.addItem("=====완료=====")
        time.sleep(0.05)
        self.parent.result_list.scrollToBottom()
        self.parent.msg_bar.setText("완료")
        self.parent.btn_stop.setDisabled(True)
        self.stoper = True
        self.signal.emit(temp_device_list)
        temp_device_list = []
        self.quit()
        return



#IP, MAC 변경 쓰레드
class thread_chage(QThread):
    signal = pyqtSignal(bool, dict)
    def __init__(self, parent, ip, port, new_ip, new_mac):
        super().__init__(parent)
        self.parent = parent
        self.ip = ip
        self.port = port
        self.new_ip = new_ip
        self.new_mac = new_mac
        self.stop = False


    def run(self):
        client_socket = None
        ip = self.ip
        port = self.port
        new_ip = self.new_ip
        new_mac = self.new_mac

        try:
            # 기기정보를 받기 위한 소켓통신
            client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client_socket.settimeout(1.0)
            client_socket.connect((ip, port))
            sSendData_1 = "02,04,03,00,0007,03".replace(",", '').replace(" ", '')
            nRecvBytes_1 = 114
            bArray = bytes.fromhex(sSendData_1)
            client_socket.sendall(bArray)
            bRecvData_1 = receivePacket(client_socket, nRecvBytes_1)
            client_socket.close()
        except Exception:
            client_socket.close()
            self.signal.emit(False, {"msg": "없는 기기 입니다"})
            return
        finally:
            client_socket.close()

        time.sleep(1.0)
        try:

            sRecvPacket = bRecvData_1.hex()  # 기기 정보 패킷
            sSendData_2 = "0206"
            sSendData_2 += sRecvPacket[4:82]
            new_mac_value = new_mac.replace(":", "")  # 맥 체인지
            sSendData_2 += new_mac_value  # 맥 체인지
            lstIp = str(new_ip).split(".")
            sIpHex = ""
            for sIp in lstIp:  sIpHex += "%02x" % int(sIp)
            sSendData_2 += sIpHex
            sSendData_2 += sRecvPacket[102:]
            # 기기정보를 변경하기 위한 소켓 통신
            client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client_socket.settimeout(3.0)
            client_socket.connect((ip, port))
            sdata = sSendData_2.replace(",", '').replace(" ", '')
            bArray2 = bytes.fromhex(sdata)
            client_socket.sendall(bArray2)
            time.sleep(5.0)

        except Exception:
            self.signal.emit(False, {"msg": "기기 변경 명령어 입력 실패"})
            return
        self.signal.emit(True, {"msg": "완료\n잠시 후에 카메라에 접속해 주세요", "new_ip": new_ip, "new_mac": new_mac})
        return




#파라미터 변경 쓰레드
class thread_param(QThread):
    signal = pyqtSignal()
    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.ip = None
        self.port = None
        self.send_data = None
        self.nRecvByte = None
        self.stop = False


    def run(self):
        runReceiveOnce(self.ip, 50000, self.send_data, self.nRecvByte, "parameter")
        time.sleep(3.0)
        self.signal.emit()






#history 검색 쓰레드( DB 검색)
class thread_history(QThread):
    signal = pyqtSignal(list)
    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.dev_id = None
        self.s_date = None
        self.e_date = None
        #self.db = Database()

    def run(self):

        try:
            db = Database()
            history_data_list = db.select_video_list(self.dev_id, self.s_date, self.e_date)
        except Exception:
            history_data_list = []
        self.signal.emit(history_data_list)

        return


#report 검색 쓰레드( DB 검색)
class thread_report(QThread):
    signal = pyqtSignal(list)
    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.dev_id = None
        self.s_date = None
        self.e_date = None
        self.add_query = ""
        #self.db = Database()

    def run(self):

        try:
            db = Database()
            report_data_list = db.select_report_list(self.dev_id, self.s_date, self.e_date, self.add_query)
        except Exception:
            report_data_list = []
        self.signal.emit(report_data_list)

        return


class thread_firm_update(QThread):
    signal = pyqtSignal(bool, str)
    def __init__(self, parent):
        super().__init__(parent)
        self.ip = None
        self.bin_data = None

    def run(self):

        lstReqPre = ["02,81,03", "02,83,03", "02,82,03"]  # start-bit,function,product-model,boot-data-size
        lstReqPost = ["010a,03", "010a,03", "010a,03"]  # packet-size,end-bit

        client_socket = None
        bRecvData = None
        try:

            Targethost = self.ip
            Targetport = 50000
            bBootData = self.bin_data
            nFileSize = len(bBootData)
            #print("....file-size:", nFileSize)
            client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            #print(Targethost, Targetport)
            client_socket.connect((Targethost, Targetport))
            #print("connected....")

            # .............. send Boot-Start
            bPre = bytes.fromhex(lstReqPre[0].replace(",", '')) + (nFileSize).to_bytes(4, byteorder='little')
            #print(bPre)
            bPost = (266).to_bytes(2, byteorder='little') + b'\x03'
            #print(bPost)
            # bSend = bPre+bBootData[0:256]+bPost
            bSend = bPre + bytes(bytearray(256)) + bPost
            #debugSavepacket("boot_start.bin", bSend)
            client_socket.sendall(bSend)  # send first packet
            bRecvData = receivePacket(client_socket, 4)
            #print('..recv-start:', bRecvData)
            if bRecvData[0] != 0x02 or bRecvData[1] != 0x81:
                self.signal.emit(False, "실패(1)")
                return
            # .............. send Boot-Data
            # send bootdata(256)
            for idx in range(0, int(nFileSize / 256)):
                bPre = bytes.fromhex(lstReqPre[1].replace(",", '')) + (256).to_bytes(4, byteorder='little')
                bSend = bPre + bBootData[idx * 256:(idx + 1) * 256] + bPost
                #debugSavepacket("boot_data_" + str(idx) + ".bin", bSend)
                client_socket.sendall(bSend)  # send Body packet
                bRecvData = receivePacket(client_socket, 4)
                #print('..recv-body:', idx, bRecvData)
                if bRecvData[0] != 0x02 or bRecvData[1] != 0x83:
                    self.signal.emit(False, "실패(2)")
            # send bootdata(last)
            realDataSize = nFileSize % 256
            bPre = bytes.fromhex(lstReqPre[1].replace(",", '')) + (256).to_bytes(4, byteorder='little')
            bPost = (266).to_bytes(2, byteorder='little') + b'\x03'
            bSend = bPre + bBootData[int(nFileSize / 256) * 256:] + bytes(bytearray(256 - realDataSize)) + bPost
            #debugSavepacket("boot_data_last.bin", bSend)
            client_socket.sendall(bSend)  # send final Body packet(less 256)
            bRecvData = receivePacket(client_socket, 4)
            #print('..recv-body(f):', bRecvData)

            """
            if bRecvData[0] != 0x02 or bRecvData[1] != 0x83:
                print("....res error:body")
                return
            """
            # .............. send Boot-Finish
            bPre = bytes.fromhex(lstReqPre[2].replace(",", '')) + (256).to_bytes(4, byteorder='little')
            bPost = (266).to_bytes(2, byteorder='little') + b'\x03'
            bSend = bPre + bytes(bytearray(256)) + bPost
            #debugSavepacket("boot_finish.bin", bSend)
            client_socket.sendall(bSend)  # send End packet
            bRecvData = receivePacket(client_socket, 4)
            # bRecvData = receivePacket(client_socket,4)
            #print('..recv-end:', bRecvData)

            client_socket.close()
        except socket.timeout:
            client_socket.close()
            self.signal.emit(False, "소켓 통신 타임 아웃")
            return
        except Exception as err:
            traceback.print_exc()
            self.signal.emit(False, "실패:"+str(err))
            return
        finally:
            client_socket.close()
        time.sleep(3.0)
        self.signal.emit(True, "성공")




#프로그램 실행 후 msg_bar_starter를 실행 시켜줌

class thread_delay_execute(QThread):
    signal = pyqtSignal()
    def __init__(self, parent, delay):
        super().__init__(parent)
        self.parent = parent
        self.delay = delay
        self.stop = False
    def run(self):
        time.sleep(self.delay)
        if not self.stop:
            self.signal.emit()
        return









#######################################################################################








# exe 파일 만들때 ui파일들을 포함시키기 위한 코드
def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    base_path = getattr(sys, '_MEIPASS', os.path.dirname(os.path.abspath(__file__))+"\\files")
    return os.path.join(base_path, relative_path)